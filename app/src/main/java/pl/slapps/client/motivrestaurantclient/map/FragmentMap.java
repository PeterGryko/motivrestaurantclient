package pl.slapps.client.motivrestaurantclient.map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 24.03.15.
 */
public class FragmentMap extends SmartPitFragment {


    private GoogleMap map;
    private ProgressBar bar;
    private String TAG = FragmentMap.class.getName();
    private LatLng initialPoint;
    private MapHelper mapHelper;
    // private MapRouteHelper mapRouteHelper;


    private boolean radar = false;

    private SmartPitSelectorView btn_radar;

    private boolean IS_DEFAULT_CONTEXT = false;

    //private ArrayList<JSONObject> restaurants;


    public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_map, parent, false);


        // bar = (ProgressBar) v.findViewById(R.id.bar);
        parseInitialArgument();


        mapHelper = new MapHelper((MainActivity) this.getActivity());

        SupportMapFragment fragment = SupportMapFragment.newInstance();

        this.getActivity().getSupportFragmentManager().beginTransaction().add(R.id.map, fragment).commitAllowingStateLoss();

        int padding = (int)this.getResources().getDimension(R.dimen.splash_btn_padding);
        btn_radar = (SmartPitSelectorView) v.findViewById(R.id.btn_radar);
        btn_radar.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.radar);
        btn_radar.getImageView().setPadding(padding, padding, padding, padding);

        btn_radar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (radar) {
                    radar = false;

                    refreashMap(MyApplication.getCurrentPlaces());
                    btn_radar.getImageView().setImageDrawable(FragmentMap.this.getResources().getDrawable(R.drawable.radar));
                } else {
                    radar = true;

                    refreashMap(MyApplication.getCurrentPlaces());
                    btn_radar.getImageView().setImageDrawable(FragmentMap.this.getResources().getDrawable(R.drawable.home));

                }

            }
        });
        //fragment.getMapAsync(this);
        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {


                map = googleMap;
                map.setMyLocationEnabled(true);
                //map.setOnMyLocationChangeListener(this);
                //mapHelper.getRestaurantsHelper().refreashSelf(map);
                mapHelper.setInfoWindowClickListener(map);
                mapHelper.setLocationListener(map);
                mapHelper.relocateLocationButton(FragmentMap.this.getView());
                mapHelper.setInfoWindowAdapter(map);
                //mapHelper.getPlacesHelper().refreashPlaces(MyApplication.getPlaces(), map);
                //mapHelper.getMarkerManager().setUpClusters(map);
                //mapHelper.getMarkerManager().refreashPoints(MyApplication.getPlaces(), map);
                refreashMap(MyApplication.getPlaces());
                mapHelper.setRouteClickListener(map);
                if (initialPoint != null) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(initialPoint, 12));
                    MyApplication.currentLocation = initialPoint;
                }

                map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {

                        // showCurrentLocationContext();
                        ((MainActivity) FragmentMap.this.getActivity()).setCurrentLocationContext();

                        return false;
                    }
                });

                ((MainActivity) FragmentMap.this.getActivity()).initDrawer();


            }
        });


        return v;
    }


    public void refreashMap(ArrayList<Place> places) {


        if (!radar) {
            ArrayList<Place> array = new ArrayList<Place>();
            for (int i = 0; i < (places.size() < 20 ? places.size() : 20); i++) {
                array.add(places.get(i));
            }
            mapHelper.getMarkerManager().refreashPoints(array, map);
        } else
            mapHelper.getMarkerManager().refreashPoints(places, map);

        IS_DEFAULT_CONTEXT = false;

    }

    public boolean isRouteActive() {
        return mapHelper.getRouteHelper().isActive();
    }

    public void resetMapRoute() {

        mapHelper.getRouteHelper().reset();
        //mapRouteHelper.reset();
        refreashMap(MyApplication.getCurrentPlaces());


    }

    public void resetMap() {
        if (IS_DEFAULT_CONTEXT)
            return;
        mapHelper.getRouteHelper().reset();
        //mapRouteHelper.reset();
        mapHelper.getMarkerManager().resetPoints(map);
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(MyApplication.currentLocation, 12));
        IS_DEFAULT_CONTEXT = true;
        radar = true;
        btn_radar.getImageView().setImageDrawable(FragmentMap.this.getResources().getDrawable(R.drawable.home));


    }

    public void toggleRoute(final Place end) {
        mapHelper.getRouteHelper().toggleRoute(end, map);


    }


    private void parseInitialArgument() {
        Log.d(TAG, "parse initial argument ");
        Bundle initialArg = this.getArguments();
        if (initialArg != null) {
            // double lat = initialArg.getDouble("lat");
            //double lon = initialArg.getDouble("lon");

            String geo = initialArg.getString("geocoder");
            JSONObject object = null;
            try {
                object = new JSONObject(geo);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }


    @Override
    public String getLabel() {
        return null;
    }


    public void onDestroyView() {
        super.onDestroyView();

        try {
            Fragment fragment = (this.getActivity()
                    .getSupportFragmentManager().findFragmentById(R.id.map));
            FragmentTransaction ft = getActivity().getSupportFragmentManager()
                    .beginTransaction();
            ft.remove(fragment);
            ft.commitAllowingStateLoss();
        } catch (Throwable t) {
            Log.d(TAG, t.toString());
        }
        //  if(this.listener!=null&& lm!=null)
        //     lm.removeUpdates(this);
        map = null;

    }


    public void centerOnMarker(double latitude, double longitude) {
        Log.d(TAG, "center on map invoked");
        if (map == null)
            return;


        Log.d(TAG, "centering map " + latitude + " " + longitude);

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
        //changeState(false);
        IS_DEFAULT_CONTEXT = false;

    }


}
