package pl.slapps.client.motivrestaurantclient.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;

import com.google.android.gms.maps.model.Marker;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitBaseFragment;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentPlaceDetails;
import pl.slapps.client.motivrestaurantclient.widget.RatingWidget;

/**
 * Created by piotr on 05.04.15.
 */
public class MapPlacesHelper {

    private String TAG = MapPlacesHelper.class.getName();

    private MainActivity context;

    private MapMarkerManager markerManager;

    public MapPlacesHelper(MainActivity context, MapMarkerManager markerManager) {
        this.context = context;
        this.markerManager = markerManager;
    }


    public Place getPlaceById(String id) {
        Place returnData = null;
        for (int i = 0; i < markerManager.getPlaces().size(); i++) {
            if (markerManager.getPlaces().get(i).getGPlaceId().equals(id)) {
                returnData = markerManager.getPlaces().get(i);
                break;
            }
        }
        return returnData;
    }


    public View getPlaceInfoWindow(String id, final Marker marker) {
        View v = LayoutInflater.from(context).inflate(R.layout.place_window_layout, null);
        ImageView qm = (ImageView)v.findViewById(R.id.qm);
        LinearLayout baseLayout = (LinearLayout)v.findViewById(R.id.base);
        Place model = getPlaceById(id);
        if (model == null)
            return v;

        if (model.getGName().trim().equals("")) {
            //onMarkerClick(id);
            //marker.hideInfoWindow();
            //v.setVisibility(View.GONE);
            baseLayout.setVisibility(View.GONE);
            return v;
        }
        qm.setVisibility(View.GONE);
        //TextView tvRate = (TextView) v.findViewById(R.id.tv_rate);
        TextView tvName = (TextView) v.findViewById(R.id.tv_name);
        TextView tvAddress = (TextView) v.findViewById(R.id.tv_address);
        final SmartImageView image = (SmartImageView) v.findViewById(R.id.iv_logo);
        image.setMode(SmartImageView.Mode.NORMAL.ordinal());
        RatingWidget rating = (RatingWidget) v.findViewById(R.id.rating);

        //if (model.getGRate() != null && !model.getGRate().equals("")) {
            rating.setScore((int) model.getGRate());
            rating.setEnabled(false);
        //} else {
        //    rating.setVisibility(View.GONE);
       // }
        tvName.setText(model.getGName());
        tvAddress.setText(model.getGAddress());

        if (!model.getRLogo().equals("")) {
            SmartPitImageLoader.SmartImagesListener listener = new SmartPitImageLoader.SmartImagesListener() {
                @Override
                public void onResponse(SmartPitImageLoader.SmartImageContainer container, boolean flag) {
                    if (image != null && container.getBitmap() != null) {
                        image.setImageBitmap(container.getBitmap());
                        //marker.hideInfoWindow();
                        if (marker != null && marker.isInfoWindowShown()) {
                            marker.hideInfoWindow();
                            marker.showInfoWindow();
                        }
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            };


            SmartPitActivity.getImageLoader()
                    .get(model.getRLogo(), listener, 0, 0);
        } else
            image.setVisibility(View.GONE);


        return v;
    }

    public boolean onMarkerClick(String id) {

        if (((SmartPitBaseFragment) context.getCurrentDrawerFragment()).getCurrentFragment() instanceof FragmentPlaceDetails) {
            ((FragmentPlaceDetails) ((SmartPitBaseFragment) context.getCurrentDrawerFragment()).getCurrentFragment()).refreashData(id);
        } else {

            Bundle arg = new Bundle();
            arg.putString("place_id", id);
            FragmentPlaceDetails fd = new FragmentPlaceDetails();
            fd.setArguments(arg);
            context.switchDrawerFragment(fd);
        }
        context.showMenu();

        return false;
    }

}
