package pl.slapps.client.motivrestaurantclient.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 02.04.15.
 */
public class FragmentHistoryOrders extends SmartPitFragment {
    private ListView lv;
    private ArrayList<JSONObject> list;
    private SmartImageView logo;
    private TextView tvTitle;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_history_orders, parent, false);
        lv = (ListView) v.findViewById(R.id.list);

        ArrayList<String> mocks = new ArrayList<String>();
        mocks.add("kanpaka");
        mocks.add("z kutasa");
        lv.setAdapter(new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, mocks));


        tvTitle = (TextView) v.findViewById(R.id.tv_title);
        logo = (SmartImageView) v.findViewById(R.id.logo);

        parseInitialArgs();
        return v;
    }

    public void parseInitialArgs() {
        if (this.getArguments() != null) {
            JSONObject city = null;
            try {
                city = new JSONObject(this.getArguments().getString("restaurant"));

                String title = city.has("title") ? city.getString("title") : "";
                String url = city.has("logo") ? city.getString("logo") : "";

                tvTitle.setText(title);
                SmartPitImageLoader.setImage(FragmentHistoryOrders.this.getActivity(), logo, url, 0, 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
