package pl.slapps.client.motivrestaurantclient.menu;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitGoogleAddressesAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 26.03.15.
 */
public class FragmentConfirmBuy extends SmartPitFragment implements RestaurantInterface {

    private String TAG = FragmentConfirmBuy.class.getName();

    private TextView tvCount;
    private TextView tvPrice;
    private ListView lv;
    private ArrayList<JSONObject> list;
    private LinearLayout btnBuy;
    private LinearLayout btnInfo;
    private LinearLayout btnAddress;

    //private Button btnLogin;

    private EditText et_username;
    private EditText et_email;
    private EditText et_phone;
    private AutoCompleteTextView et_address;
    private EditText et_house;
    private EditText et_flat;
    private EditText et_city;
    private EditText et_note;
    private double lat;
    private double lon;
    private SmartPitGoogleAddressesAdapter addressesAdapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_confirm_order, parent, false);

        tvCount = (TextView) v.findViewById(R.id.tv_count);
        tvPrice = (TextView) v.findViewById(R.id.tv_price);


        et_username = (EditText) v.findViewById(R.id.et_username);
        et_email = (EditText) v.findViewById(R.id.et_email);
        et_phone = (EditText) v.findViewById(R.id.et_phone);
        et_address = (AutoCompleteTextView) v.findViewById(R.id.et_address);
        et_house = (EditText) v.findViewById(R.id.et_house);
        et_flat = (EditText) v.findViewById(R.id.et_flat);
        et_city = (EditText) v.findViewById(R.id.et_city);

        et_note = (EditText) v.findViewById(R.id.et_note);
        addressesAdapter = new SmartPitGoogleAddressesAdapter(this.getActivity(), android.R.layout.simple_list_item_1);
        et_address.setAdapter(addressesAdapter);
        et_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lat = addressesAdapter.getPoint(i).getLat();
                lon = addressesAdapter.getPoint(i).getLon();
            }
        });


        if (this.getArguments() != null) {
            String price = this.getArguments().getString("price");
            int count = this.getArguments().getInt("count");
            tvPrice.setText(price);
            tvCount.setText(Integer.toString(count));
        }

        /*
        btnLogin = (Button) v.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle arg = new Bundle();
                FragmentProfileBase fb = new FragmentProfileBase();
                fb.setArguments(arg);
                ((MainActivity) FragmentConfirmBuy.this.getActivity()).switchDrawerTitleFragment(fb);

                ((MainActivity) FragmentConfirmBuy.this.getActivity()).showMasterMenu();
            }
        });
        btnLogin.setVisibility(View.GONE);
*/
        btnBuy = (LinearLayout) v.findViewById(R.id.btn_buy);
        btnInfo = (LinearLayout) v.findViewById(R.id.btn_info);
        btnAddress = (LinearLayout) v.findViewById(R.id.btn_choose_address);

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentConfirmBuy.this.getActivity()).showSlaveMenu();
            }
        });
        btnAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "eheh");
            }
        });

        //btnBuy.configure(R.drawable.circle_bkg,R.drawable.circle_profile_sel,R.drawable.money);
        //btnBuy.getImageView().setPadding(10,10,10,10);
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (MyApplication.getCurrentUser() == null) {
                    Toast.makeText(FragmentConfirmBuy.this.getActivity(), "Musisz się zalogować", Toast.LENGTH_LONG).show();
                    return;
                }

                JSONObject data = new JSONObject();
                try {
                    Log.d(TAG, MyApplication.getCart().toString());
                    JSONArray json_cart = new JSONArray();
                    for (int i = 0; i < MyApplication.getCart().size(); i++) {
                        json_cart.put(MyApplication.getCart().get(i));
                    }

                    String client = et_username.getText().toString();
                    String phone = et_phone.getText().toString();
                    String street = et_address.getText().toString();
                    String flat = et_flat.getText().toString();
                    String house = et_house.getText().toString();
                    String note = et_note.getText().toString();

                    if (lat == 0 || lon == 0) {
                        Toast.makeText(FragmentConfirmBuy.this.getActivity(), "Musisz wybrać adres", Toast.LENGTH_LONG).show();
                        return;
                    } else if (client.trim().equals("")
                            || phone.trim().equals("")
                            || street.trim().equals("")
                            || flat.trim().equals("")
                            || house.trim().equals("")) {
                        Toast.makeText(FragmentConfirmBuy.this.getActivity(), "Musisz wypełnić wszystkie pola", Toast.LENGTH_LONG).show();
                        return;
                    }


                    JSONObject details = new JSONObject();
                    details.put("on_place", false);
                    details.put("client", client);
                    details.put("phone", phone);
                    details.put("street", street);
                    details.put("flat", flat);
                    details.put("house", house);
                    details.put("note", note);
                    details.put("lat", lat);
                    details.put("lon", lon);
                    details.put("price", tvPrice.getText().toString());


                    data.put("deliveryData", details);


                    data.put("cart", json_cart);

                    data.put("status", "new");
                    data.put("owner", MyApplication.getCurrentRestaurant().isRestaurant());


                    DAO.createOrder(data, new Response.Listener() {
                        @Override
                        public void onResponse(Object o) {
                            Log.d(TAG, o.toString());
                            JSONObject d = null;
                            try {
                                d = new JSONObject(o.toString());

                                JSONObject api = d.has("api") ? d.getJSONObject("api") : new JSONObject();
                                JSONObject doc = api.has("doc") ? api.getJSONObject("doc") : new JSONObject();
                                String id = doc.has("_id") ? doc.getString("_id") : "";
                                ((MainActivity) FragmentConfirmBuy.this.getActivity()).getService().initSocket(id);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            Toast.makeText(FragmentConfirmBuy.this.getActivity(), "Zamówienie zostało złożone", Toast.LENGTH_LONG).show();
                            MyApplication.getCart().removeAll(MyApplication.getCart());
                            ((MainActivity) FragmentConfirmBuy.this.getActivity()).refreashCart();
                            ((MainActivity) FragmentConfirmBuy.this.getActivity()).onBackPressed();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.d(TAG, volleyError.toString());
                            Log.d(TAG, AppHelper.getErrorMessage(volleyError));
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        if (MyApplication.getCurrentUser() != null)
            parseProfileData(MyApplication.getCurrentUser());
        // } else
        //     btnLogin.setVisibility(View.VISIBLE);

        return v;
    }

    public void refreashUser() {
        /*
        btnLogin.setVisibility(View.GONE);
        DAO.getProfile(new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                try {
                    parseProfileData(new JSONObject(o.toString()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                btnLogin.setVisibility(View.VISIBLE);
            }
        });
*/
    }

    public void parseProfileData(JSONObject p) {

        try {

            JSONObject data = p.has("data") ? p.getJSONObject("data") : new JSONObject();

            String username = data.has("username") ? data.getString("username") : "";
            String email = data.has("email") ? data.getString("email") : "";


            String address = data.has("address") ? data.getString("address") : "";
            String house = data.has("house") ? data.getString("house") : "";
            String flat = data.has("flat") ? data.getString("flat") : "";
            String phone = data.has("phone") ? data.getString("phone") : "";

            JSONObject avatar = p.has("avatar") ? p.getJSONObject("avatar") : new JSONObject();
            String path = avatar.has("path") ? avatar.getString("path") : "";
            lat = data.has("lat") ? data.getDouble("lat") : 0;
            lon = data.has("lng") ? data.getDouble("lng") : 0;

            // MyApplication.setCurrentProfile(p);

            // SmartPitAppHelper.getInstance(context).setImage(ivLogo, DAO.API + path, 0, 0);


            et_username.setText(username);
            et_address.setText(address);
            et_phone.setText(phone);
            et_email.setText(email);
            et_house.setText(house);
            et_flat.setText(flat);
            // et_house.setText(house);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
