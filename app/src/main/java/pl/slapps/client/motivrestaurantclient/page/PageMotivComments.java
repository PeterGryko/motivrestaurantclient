package pl.slapps.client.motivrestaurantclient.page;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.adapter.AdapterComments;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentNewComment;

/**
 * Created by piotr on 19.07.15.
 */
public class PageMotivComments {

    private String TAG = PageMotivComments.class.getName();
    private LinearLayout layout_info;
    private ListView lv;
    private SmartPitSelectorView btn_comment;

    public View createView(LayoutInflater inflater, final Context context, final Place model) {
        View v = inflater.inflate(R.layout.page_motiv_comments, null);

        layout_info = (LinearLayout) v.findViewById(R.id.info_layout);
        lv = (ListView) v.findViewById(R.id.lv);
        btn_comment = (SmartPitSelectorView) v.findViewById(R.id.btn_comment);

        btn_comment.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.comment);
        btn_comment.getImageView().setPadding(15, 15, 15, 15);

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyApplication.getCurrentUser() == null) {
                    Toast.makeText(context, "Musisz się zalogować", Toast.LENGTH_LONG).show();
                    return;
                }

                FragmentNewComment fc = new FragmentNewComment();
                Bundle arg = new Bundle();
                arg.putString("data", model.toString());
                fc.setArguments(arg);
                ((MainActivity)context).switchDrawerFragment(fc);
            }
        });


        DAO.getPlaceComments(model.getRId(), "1", "20", new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
                try {
                    JSONObject object = new JSONObject(o.toString());
                    object = object.has("api") ? object.getJSONObject("api") : object;
                    JSONArray array = object.getJSONArray("results");

                    ArrayList<JSONObject> data = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        data.add(array.getJSONObject(i));
                    }

                    if (data.size() > 0) {
                        layout_info.setVisibility(View.GONE);
                        lv.setAdapter(new AdapterComments(context, data));
                        //  SmartPitAppHelper.getInstance(context).setListViewHeightBasedOnChildren(lv);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
            }
        });

        return v;
    }
}
