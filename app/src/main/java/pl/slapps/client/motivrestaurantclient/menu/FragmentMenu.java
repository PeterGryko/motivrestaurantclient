package pl.slapps.client.motivrestaurantclient.menu;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitViewPagerAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.page.PageMenu;

import pl.slapps.client.motivrestaurantclient.widget.AppHelper;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 24.03.15.
 */
public class FragmentMenu extends SmartPitPagerFragment implements RestaurantInterface{


    private ArrayList<View> list;


    private ArrayList<PageMenu> pages;
    private Place restaurant;

    private String TAG = FragmentMenu.this.getClass().getName();
    private float top_view_height;
    private View v;

    private ArrayList<View> categries;


    private ViewPager pager_categories;

    private ArrayList<JSONObject> backstack;

    private boolean selection_coop_flag;

    private int[] input_color;
    private int[] output_color;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        Log.d(TAG, "state " + savedInstanceState);
        v = inflater.inflate(R.layout.fragment_menu, parent, false);
        int input = this.getResources().getColor(R.color.six);
        int output = this.getResources().getColor(R.color.four);


        input_color = new int[]{Color.red(input), Color.green(input), Color.blue(input)};
        output_color = new int[]{Color.red(output), Color.green(output), Color.blue(output)};


        pager_categories = (ViewPager) v.findViewById(R.id.pager_categories);
        pager_categories.setPageMargin(-(SmartPitAppHelper.getScreenWidth(this.getActivity()) * 5) / 7);
        pager_categories.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                Log.d(TAG, "position " + position);

                int[] out = AppHelper.generateRainbowTour(input_color, output_color, 1 - Math.abs(position));

                ((GradientDrawable) ((ViewGroup) page).getChildAt(0).getBackground()).setColor(
                        Color.rgb(out[0], out[1], out[2]));


                ViewCompat.setScaleX(page, 1 - Math.abs(position));
                ViewCompat.setScaleY(page, 1 - Math.abs(position));

            }
        });

        pager_categories.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float movement, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

                Log.d(TAG, "on page selected categories " + position);
                if (!selection_coop_flag) {
                    selection_coop_flag = true;
                    FragmentMenu.this.getPager().setCurrentItem(position);


                } else
                    selection_coop_flag = false;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        categries = new ArrayList<>();
        top_view_height = this.getActivity().getResources().getDimension(R.dimen.top_height);


        list = new ArrayList<View>();
        backstack = new ArrayList<>();

        pages = new ArrayList<>();


        parseInitialArgs(v, this.getArguments().getBundle("state"));


        this.getPager().getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity());

        //((MainActivity) FragmentMenu.this.getActivity()).getAbImpl().getSearchbarBase().setVisibility(View.GONE);
        //((MainActivity) FragmentMenu.this.getActivity()).getAbImpl().getRestaurantBase().setVisibility(View.VISIBLE);


        return v;
    }

    public void onDestroyView() {
        super.onDestroyView();


        Bundle out = new Bundle();
        ArrayList<Bundle> arrayList = new ArrayList<>();
        for (int i = 0; i < pages.size(); i++) {
            arrayList.add(pages.get(i).onSaveInstanceState());
        }
        out.putParcelableArrayList("pages", arrayList);

        this.getArguments().putBundle("state", out);


    }


    public void restoreMenu(ArrayList<Bundle> saved_pages) {
        list.removeAll(list);
        pages.removeAll(pages);
        categries.removeAll(categries);
        for (int i = 0; i < saved_pages.size(); i++)

        {
            try {
                Place restaurant = new Place(saved_pages.get(i).getString("restaurant"));

                JSONObject parent_category = new JSONObject(saved_pages.get(i).getString("parent_category"));
                String label = parent_category.has("label") ? parent_category.getString("label") : "Wszystkie";

                int page = saved_pages.get(i).getInt("page");

                PageMenu pageMenu = new PageMenu();
                pages.add(pageMenu);
                list.add(pageMenu.createView((MainActivity) this.getActivity(), restaurant, parent_category, this, page, null));
                categries.add(generateCategoryTab(label));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setViewsPager(v, R.id.pager, list);

        this.getPager().setOffscreenPageLimit(list.size());
        pager_categories.setOffscreenPageLimit(categries.size());
        this.getHost().getTabWidget().setEnabled(false);


        pager_categories.setAdapter(new SmartPitViewPagerAdapter(this.getActivity(), categries));
        //this.getPager().setCurrentItem(list.size()-1,true);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                pager_categories.setCurrentItem(categries.size());
            }
        });

        Log.d(TAG, "list size " + list.size() + " current item " + getPager().getCurrentItem());
    }


    public void parseMenu(Place restaurant, JSONObject parent_category, JSONObject current_parent, int page) {

        Log.d(TAG, "parse menu " + page);

        for (int i = list.size() - 1; i > page; i--) {
            list.remove(i);
            categries.remove(i);
            pages.remove(i);

        }

        try {
            String label = parent_category.has("label") ? parent_category.getString("label") : "Wszystkie";
            categries.add(generateCategoryTab(label));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        PageMenu pageMenu = new PageMenu();
        pages.add(pageMenu);
        list.add(pageMenu.createView((MainActivity) this.getActivity(), restaurant, parent_category, this, list.size(), null));


        if (this.getPager() != null && this.getPager().getAdapter() != null) {
            this.getPager().getAdapter().notifyDataSetChanged();
        } else {

            this.setViewsPager(v, R.id.pager, list);
        }


        if (pager_categories.getAdapter() == null) {
            pager_categories.setAdapter(new SmartPitViewPagerAdapter(this.getActivity(), categries));
        } else {
            pager_categories.getAdapter().notifyDataSetChanged();
        }
        this.getHost().getTabWidget().setEnabled(false);
        this.getPager().setOffscreenPageLimit(list.size());
        pager_categories.setOffscreenPageLimit(categries.size());
        pager_categories.setCurrentItem(categries.size() - 1, true);


    }

    public void onAdapterSetted() {
        Log.d(TAG, "on adapter setted " + list.size() + " " + getPager().getCurrentItem());
    }

    private View generateCategoryTab(String tab) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.tab_category, null);
        TextView tv_label = (TextView) v.findViewById(R.id.tv_label);
        tv_label.setText(tab);

        return v;
    }


    private void parseInitialArgs(View v, Bundle state) {


        if (state != null && state.getParcelableArrayList("pages") != null) {
            ArrayList<Bundle> pages = state.getParcelableArrayList("pages");
            restoreMenu(pages);
            Log.d(TAG, "fragment menu restore instantce state! " + pages.size());

        } else if (this.getArguments() != null) {
            try {
                restaurant = new Place(this.getArguments().getString("data"));


                parseMenu(restaurant, new JSONObject(), null, 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    public View createTabIndicator(Context context, int index) {
        View v = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        return v;

    }

    @Override
    public String getLabel() {
        return null;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.d(TAG, "on page scrolled " + positionOffset);

    }

    @Override
    public void onPageSelected(int position) {
        if (!selection_coop_flag) {
            selection_coop_flag = true;
            pager_categories.setCurrentItem(position);

            Log.d(TAG, "on page selected " + position);

        } else
            selection_coop_flag = false;

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
