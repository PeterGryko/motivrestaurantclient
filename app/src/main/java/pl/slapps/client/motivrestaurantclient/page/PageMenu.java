package pl.slapps.client.motivrestaurantclient.page;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitViewPagerAdapter;
import pl.gryko.smartpitlib.widget.SmartPitTabHostIndicator;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.menu.FragmentMenu;
import pl.slapps.client.motivrestaurantclient.menu.PageCategories;
import pl.slapps.client.motivrestaurantclient.menu.PageProducts;

/**
 * Created by piotr on 12.08.15.
 */
public class PageMenu {

    class TabContent implements TabHost.TabContentFactory {
        private Context context;

        public TabContent(Context context) {
            this.context = context;
        }

        @Override
        public View createTabContent(String tag) {
            return new View(context);

        }
    }

    private ViewPager viewPager;
    private ArrayList<View> viewsList;
    private TabHost host;
    private MainActivity context;
    private SmartPitTabHostIndicator smart_indicator;

    private PageProducts pageProducts;
    private PageCategories pageCategories;

    private Bundle outState;

    public Bundle onSaveInstanceState() {

        outState.putBundle("products", pageProducts.onSaveInstanceState());
        outState.putBundle("categories", pageCategories.onSaveInstanceState());
        return outState;
    }


    public View createView(MainActivity context, Place restaurant, JSONObject parent_category, final FragmentMenu menu, int page, Bundle savedInstanceState) {



        outState = new Bundle();
        outState.putString("restaurant", restaurant.toString());
        outState.putString("parent_category", parent_category.toString());
        outState.putInt("page",page);
        ArrayList<View> list = new ArrayList<>();
        View v = LayoutInflater.from(context).inflate(R.layout.page_menu, null);
        Bundle cat_state = null;
        Bundle pro_state = null;

        if (savedInstanceState != null) {
            cat_state = savedInstanceState.getBundle("categories");
            pro_state = savedInstanceState.getBundle("products");


        }
        pageCategories = new PageCategories();
        pageProducts = new PageProducts();

        list.add(pageCategories.init(context, menu, restaurant, parent_category, page, cat_state));
        list.add(pageProducts.init(context, null, restaurant, parent_category, pro_state));

        setViewsPager(context, v, R.id.pager, list);

        smart_indicator = (SmartPitTabHostIndicator) v.findViewById(R.id.smart_indicator);


        smart_indicator.initView(getHost(), LayoutInflater.from(context).inflate(R.layout.profile_indicator_view, null), false);


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                smart_indicator.updateChildren(position, positionOffset);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }

    public TabHost getHost() {
        return host;
    }

    public View createTabIndicator(Context context, int index) {
        TextView v = (TextView) LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        switch (index) {
            case 0:
                v.setText("Kategorie");
                break;
            case 1:
                v.setText("Produkty");
                break;
        }

        return v;

    }

    public void setViewsPager(MainActivity context, View v, int pagerId, ArrayList<View> list) {

        viewsList = list;
        viewPager = (ViewPager) v.findViewById(pagerId);

        host = (TabHost) v.findViewById(android.R.id.tabhost);
        host.setup();
        host.getTabWidget().removeAllViews();

        TabHost.TabSpec spec = null;
        for (int i = 0; i < viewsList.size(); i++) {
            spec = this
                    .getHost()
                    .newTabSpec(Integer.toString(i))
                    .setIndicator(
                            createTabIndicator(context,
                                    i)
                    )
                    .setContent(new TabContent(context));
            this.getHost().addTab(spec);
        }
        viewPager.setAdapter(new SmartPitViewPagerAdapter(context, viewsList));
        this.getHost().getTabWidget().setEnabled(false);




    }
}
