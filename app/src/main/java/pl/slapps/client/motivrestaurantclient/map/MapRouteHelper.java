package pl.slapps.client.motivrestaurantclient.map;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.route.SmartMapRouteManager;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 13.07.15.
 */
public class MapRouteHelper {
    private String TAG = MapRouteHelper.class.getName();
    private ArrayList<SmartMapRouteManager.Leg> roads;
    private Context context;
    private MapHelper mapHelper;
    private Marker current_info;
    private SmartMapRouteManager.Step current_step;

    public MapRouteHelper(Context context, MapHelper mapHelper) {
        this.context = context;
        this.mapHelper = mapHelper;
    }


    public View getRouteInfoWindow(String data, final Marker marker) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_route_window, null);
        TextView tv_distance = (TextView) v.findViewById(R.id.tv_distance);
        TextView tv_duration = (TextView) v.findViewById(R.id.tv_duration);
        TextView tv_instructions = (TextView) v.findViewById(R.id.tv_instructions);

        try {
            JSONObject object = new JSONObject(data);
            String duration = object.has("duration") ? object.getString("duration") : "";
            String distacne = object.has("distance") ? object.getString("distance") : "";
            String instructions = object.has("instructions") ? object.getString("instructions") : "";
            tv_distance.setText(distacne);
            tv_duration.setText(duration);
            tv_instructions.setText(Html.fromHtml(instructions));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return v;
    }

    public void setOnClickListener(final GoogleMap map) {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (roads == null)
                    return;

                for (int i = 0; i < roads.size(); i++) {

                    for (int j = 0; j < roads.get(i).steps.size(); j++) {
                        SmartMapRouteManager.Step step = roads.get(i).steps.get(j);
                        Log.d(TAG, "step " + step.distance);
                        if (PolyUtil.isLocationOnPath(latLng, step.polylineOptions.getPoints(), true, 100)) {
                            if (current_info != null) {
                                current_info.remove();
                                current_info = null;

                            }
                            if (current_step != null) {
                                current_step.polyline.remove();

                                current_step.polylineOptions.color(Color.BLACK);
                                current_step.drawLine(map);
                                current_step = null;
                            }
                            JSONObject base_data = new JSONObject();
                            try {
                                base_data.put("instructions", step.instructions);

                                base_data.put("distance", step.distance);
                                base_data.put("duration", step.duration);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            step.polyline.remove();
                            step.polylineOptions.color(Color.BLUE);
                            step.drawLine(map);
                            current_info = map.addMarker(new MarkerOptions().position(latLng).title(MapHelper.ROUTE + MapHelper.SEPARATOR + base_data.toString()));
                            current_info.setAlpha(0);

                            current_info.showInfoWindow();
                            map.animateCamera(CameraUpdateFactory.newLatLng(current_info.getPosition()));

                            current_step = step;
                            return;
                        }

                    }


                }

                if (current_step != null) {
                    current_step.polyline.remove();

                    current_step.polylineOptions.color(Color.BLACK);
                    current_step.drawLine(map);
                    current_step = null;
                }
            }
        });
    }

    public boolean isActive() {
        return roads !=null;
    }

    public void reset() {
        roads = null;
    }

    public void toggleRoute(final Place end, final GoogleMap map) {
        if (MyApplication.currentLocation == null) {
            Toast.makeText(context, "Brak aktualnej lokalizacji", Toast.LENGTH_LONG).show();
            return;
        }

        new SmartMapRouteManager().showRoute(MyApplication.currentLocation, end.getPosition(), "AIzaSyCXuTyeq9UBftKWdILyb6-E4Q7_Kp1Y16Q", new SmartMapRouteManager.RouteListener() {
            @Override
            public void success(ArrayList<SmartMapRouteManager.Leg> list) {

                Log.d(TAG, "success");
                roads = list;

                ArrayList<LatLng> points = new ArrayList<LatLng>();
                mapHelper.getMarkerManager().clearMap(map);
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j < list.get(i).steps.size(); j++) {
                        SmartMapRouteManager.Step step = list.get(i).steps.get(j);
                        step.drawLine(map);
                        //map.addPolyline(step.polylineOptions);
                        points.addAll(step.polylineOptions.getPoints());
                    }


                }

                //fragmentMap.changeState(false);

                String title = MapHelper.PLACE + MapHelper.SEPARATOR + end.getGPlaceId();
                Log.d(TAG, "marker logo " + end.getRLogo());
                if (end.getRLogo().trim().equals("")) {
                    map.addMarker(new MarkerOptions().position(end.getPosition()).title(title).icon(BitmapDescriptorFactory.fromBitmap(CustomClusterRenderer.iconBitmap)));

                } else {
                    try {
                        new RemoteIconMapMarker(context).init(end.getPosition(), end.getRLogo(), title, map, true);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                ((MainActivity) context).showMenu();
                mapHelper.getMarkerManager().animateMap(points, map);

            }

            @Override
            public void failure() {

            }
        });


    }
}
