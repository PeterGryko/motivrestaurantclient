package pl.slapps.client.motivrestaurantclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;


import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.menu.PageCategories;

/**
 * Created by piotr on 30.03.15.
 */

public class CustomScrollView extends ObservableScrollView {

    private int border;
    boolean allaw = true;
    private String TAG = CustomScrollView.class.getName();
    private boolean isRevining = false;
    private PageCategories.RevindListener revindListener;

    private boolean intercept = false;


    public CustomScrollView(Context context) {
        super(context);

    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public void setInterceptFlag(boolean flag) {
        this.intercept = flag;
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        Log.d(TAG, "on inercept touch event "+intercept);
        //return intercept;
        return super.onInterceptTouchEvent(event);
    }


}
