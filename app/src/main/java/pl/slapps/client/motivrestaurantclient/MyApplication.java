package pl.slapps.client.motivrestaurantclient;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import pl.slapps.client.motivrestaurantclient.model.Place;

/**
 * Created by piotr on 26.03.15.
 */
public class MyApplication extends Application {

    private static ArrayList<JSONObject> cart;


    private static ArrayList<Place> places;
    private static ArrayList<Place> current_places;


    private static JSONObject currentUser;
    private static Place currentRestaurant;


    public static LatLng currentLocation;
    public static String token;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
    }

    public static void setCurrentRestaurant(Place r) {
        currentRestaurant = r;
    }

    public static Place getCurrentRestaurant() {
        return currentRestaurant;
    }

    public static ArrayList<JSONObject> getCart() {
        if (cart == null)
            cart = new ArrayList<JSONObject>();
        return cart;
    }


    public static ArrayList<Place> getPlaces() {
        if (places == null)
            places = new ArrayList<Place>();
        return places;
    }

    public static ArrayList<Place> getCurrentPlaces() {
        if (current_places == null)
            current_places = new ArrayList<Place>();
        return current_places;
    }


    public static void setPlaces(ArrayList<Place> r) {
        places = r;
    }

    public static void setCurrentPlaces(ArrayList<Place> r) {
        current_places = r;
    }


    public static void setCurrentProfile(JSONObject p) {
        currentUser = p;
    }

    public static JSONObject getCurrentUser() {
        return currentUser;
    }

    public static String getCurrentProfileId() {
        String id = "";
        if (currentUser != null) {
            try {
                id = currentUser.has("_id") ? currentUser.getString("_id") : "";
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return id;
    }
}
