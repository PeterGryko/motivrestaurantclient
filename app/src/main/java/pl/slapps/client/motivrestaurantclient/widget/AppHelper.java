package pl.slapps.client.motivrestaurantclient.widget;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.bitmaps.SmartPitImagesListener;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.model.Order;
import pl.slapps.client.motivrestaurantclient.service.BackgroundService;

/**
 * Created by piotr on 30.03.15.
 */
public class AppHelper {

    private static String TAG = AppHelper.class.getName();

    public static int getScreenHeight(Activity context) {
        WindowManager w = context.getWindowManager();
        Display d = w.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        d.getMetrics(metrics);
        int heightPixels = metrics.heightPixels;
        Log.d(TAG, "default height " + heightPixels);
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
            try {
                heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
                Log.d(TAG, "height >14 <17 " + heightPixels);
            } catch (Exception ignored) {
                Log.d(TAG, ignored.toString());
            }
        if (Build.VERSION.SDK_INT >= 17)
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
                heightPixels = realSize.y;
                Log.d(TAG, "height >=17 " + heightPixels);

            } catch (Exception ignored) {
                Log.d(TAG, ignored.toString());
            }
        Log.d(TAG, "return height " + heightPixels);

        Rect rectangle = new Rect();
        Window window = context.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarTop = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int statusbarHeight = Math.abs(statusBarTop - contentViewTop);
        return heightPixels - statusbarHeight;
    }


    public static String getErrorMessage(VolleyError volleyError) {
        String out = "nie dizala";
        try {
            if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {


                String json = new String(new String(volleyError.networkResponse.data));

                Log.d(TAG, json);

                JSONObject input = new JSONObject(json);

                out = input.has("message") ? input.getString("message") : "";

                //Additional cases
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, out);
        return out;
    }


    public static int[] generateRainbowTour(int[] rgbIn, int[] rgbOut, float movement) {

        int[] rgbFinal = new int[4];

        for (int i = 0; i < rgbIn.length; i++) {

            rgbFinal[i] = rgbIn[i] + (int) ((rgbOut[i] - rgbIn[i]) * movement);
        }

        return rgbFinal;
    }

    public static int[] generateAlphaRainbowTour(int[] rgbIn, int[] rgbOut, float movement) {

        int[] rgbFinal = new int[4];

        for (int i = 0; i < rgbIn.length; i++) {
            if (i == 0)
                rgbFinal[i] = 50;
            else
                rgbFinal[i] = rgbIn[i] + (int) ((rgbOut[i] - rgbIn[i]) * movement);
        }

        return rgbFinal;
    }

    public static void showStatusChangeDialog(JSONObject data, Context context) {


        Order order = new Order(data);

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_status_change, null);
        TextView tvStatus = (TextView) v.findViewById(R.id.tv_status);
        TextView tvCount = (TextView) v.findViewById(R.id.tv_count);
        TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);

        tvStatus.setText(order.status);
        tvPrice.setText(order.price);
        tvCount.setText(Integer.toString(order.cart.length()));
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_bkg));
        dialog.setContentView(v);
        dialog.show();


    }

    public static void showMessageDialog(JSONObject data, final Context context, final BackgroundService backgroundService) {


        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_message, null);
        final TextView tvMessage = (TextView) v.findViewById(R.id.tv_message);
        final EditText etMessage = (EditText) v.findViewById(R.id.et_msg);
        SmartPitSelectorView btnSend = (SmartPitSelectorView) v.findViewById(R.id.btn_send);
        int pad = (int) context.getResources().getDimension(R.dimen.btn_padding_large);
        btnSend.getImageView().setPadding(pad, pad, pad, pad);
        btnSend.configure(R.drawable.circle_sel, R.drawable.circle_profile_sel, R.drawable.send_message);

        String body = null;
        try {
            final String from = data.has("to") ? data.getString("to") : "";
            final String to = data.has("from") ? data.getString("from") : "";

            body = data.has("body") ? data.getString("body") : "";

            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String msg = etMessage.getText().toString();
                    if (msg.trim().equals("")) {
                        Toast.makeText(context, "Podaj teść wiadomosći", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        backgroundService.emitMessage(msg, from,to);
                        tvMessage.append("\n" + msg);
                    }
                }
            });

            tvMessage.setText(body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_bkg));
        dialog.setContentView(v);
        dialog.show();


    }


    public static void setImage(final Context context, final SmartImageView image_view, String url, int width, int height, final Object position) {

        image_view.getImageView().setTag(position);

        Log.d(TAG, "set image " + image_view.getImageView().getTag().toString() + " #");

        final SmartPitImageLoader.SmartImagesListener li = new SmartPitImagesListener(context, url, image_view) {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                // super.onErrorResponse(arg0);
                image_view.setVisibility(View.GONE);
                image_view.showErrorImage();
                SmartPitAppHelper.showViewWithAnimation(image_view, 300);
            }

            @Override
            public void onResponse(SmartPitImageLoader.SmartImageContainer arg0, boolean arg1) {
                // super.onResponse(arg0, arg1);
                Log.d(TAG, "response received " + image_view.getImageView().getTag().toString() + " ## " + position.toString());

                if (arg0.getBitmap() != null) {
                    if (image_view.getImageView().getTag() == position) {
                        image_view.setImageBitmap(arg0.getBitmap());
                        image_view.setVisibility(View.GONE);
                        SmartPitAppHelper.showViewWithAnimation(image_view, 300);
                    }
                }
            }
        };

        SmartPitActivity.getImageLoader()
                .get(url, li, width, height);
    }

}
