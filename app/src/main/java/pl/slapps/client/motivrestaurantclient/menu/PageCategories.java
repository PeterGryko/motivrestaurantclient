package pl.slapps.client.motivrestaurantclient.menu;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;


/**
 * Created by piotr on 16.03.15.
 */
public class PageCategories {

    public interface RevindListener {
        public void onFinished();
    }

    private String TAG = PageCategories.class.getName();

    private ObservableListView listView;
    private Context context;
    //private SmartPitFragmentsInterface listener;
    private ArrayList<JSONObject> data;

    private boolean reviningList = false;
    private RevindListener revindListener;

    private ProgressBar bar;
    private Bundle outState;

    public Bundle onSaveInstanceState() {

        return outState;
    }

    private void restoreInstanceState(final Bundle savedInstanceState, final FragmentMenu menu) {

        Log.d(TAG, "page categories restore instance ");
        try {
            final Place restaurant = new Place(savedInstanceState.getString("restaurant"));
            final JSONObject parent_category = new JSONObject(savedInstanceState.getString("parent_category"));
            final int page = savedInstanceState.getInt("page");

            outState.putInt("page", page);

            outState.putString("restaurant", restaurant.toString());
            outState.putString("parent_category", parent_category.toString());

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    menu.parseMenu(restaurant, data.get(position), parent_category, page);
                }
            });
            listView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
                @Override
                public void onScrollChanged(int i, boolean b, boolean b2) {
                    Log.d(TAG, "scroll chagned " + i);

                    if (!reviningList) {
                        // menu.setListPosition(i);
                    } else {
                        if (i == 0) {
                            revindListener.onFinished();
                            reviningList = false;
                        }

                    }

                }

                @Override
                public void onDownMotionEvent() {

                }

                @Override
                public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                    // Log.d(TAG, "scroll state  " + scrollState.name());

                }
            });


            parseHttpResponse(savedInstanceState.getString("data"));
            bar.setVisibility(View.GONE);
        } catch (Throwable t) {
            t.printStackTrace();
            Log.d(TAG, "page categories restore error  " + t.toString());

        }


    }

    private void createInstance(final FragmentMenu menu, final Place restaurant, final JSONObject parent_category, final int page) {

        outState.putInt("page", page);

        outState.putString("restaurant", restaurant.toString());
        outState.putString("parent_category", parent_category.toString());

        Log.d(TAG, "page categories restuarant " + restaurant.toString());
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                menu.parseMenu(restaurant, data.get(position), parent_category, page);
            }
        });
        listView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int i, boolean b, boolean b2) {

                if (!reviningList) {
                    // menu.setListPosition(i);
                } else {
                    if (i == 0) {
                        revindListener.onFinished();
                        reviningList = false;
                    }

                }

            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                // Log.d(TAG, "scroll state  " + scrollState.name());

            }
        });


        Log.d(TAG, "parent category " + parent_category.toString());


        String parent_id = null;
        try {
            parent_id = parent_category.has("_id") ? parent_category.getString("_id") : null;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String id = restaurant.isRestaurant();


        DAO.getRestaurantCategories(id, new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                Log.d(TAG, o.toString());
                try {
                    outState.putString("data", o.toString());
                    parseHttpResponse(o.toString());
                    bar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                bar.setVisibility(View.GONE);

                try {
                    parseHttpResponse(volleyError.toString());
                    outState.putString("data", "");

                    bar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, parent_id);
    }

    public View init(final Context context, final FragmentMenu menu, final Place restaurant, final JSONObject parent_category, final int page, final Bundle savedInstanceState) {

        this.context = context;
        data = new ArrayList<>();
        outState = new Bundle();
        final Animation animation = AnimationUtils.loadAnimation(context, R.anim.down_from_top);


        View v = LayoutInflater.from(context).inflate(R.layout.fragment_list, null);
        listView = (ObservableListView) v.findViewById(R.id.lv);
        bar = (ProgressBar) v.findViewById(R.id.bar);


        if (data.size() > 0)
            bar.setVisibility(View.GONE);

        if (savedInstanceState == null)
            createInstance(menu, restaurant, parent_category, page);
        else
            restoreInstanceState(savedInstanceState, menu);


        // SmartPitAppHelper.getInstance(context).setListViewHeightBasedOnChildren(listView);
        return v;
    }

    private void parseHttpResponse(String o) throws JSONException {


        Log.d(TAG, "parse http response " + o);

        JSONObject result = new JSONObject(o.toString());
        result = result.has("api")?result.getJSONObject("api"):result;

        JSONArray array = result.has("results")?result.getJSONArray("results"):new JSONArray();

        // JSONArray array = new JSONArray(o.toString());

        // for (int i = 0; i < array.length(); i++) {
        //     data.add(array.getJSONObject(i));
        // }
        for (int i = 0; i < array.length(); i++) {
            Log.d(TAG, "adding element ");
            JSONObject object = new JSONObject();

            data.add(array.getJSONObject(i));
        }

        listView.setAdapter(new AdapterCategories(context, data));


    }

    public ListView getListView() {
        return listView;
    }

    public void revindList(RevindListener listener) {

        if (listView.getCurrentScrollY() == 0) {

            listener.onFinished();
        } else {
            revindListener = listener;
            reviningList = true;
            listView.setSelection(0);
        }
    }


}
