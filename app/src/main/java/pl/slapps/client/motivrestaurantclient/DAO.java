package pl.slapps.client.motivrestaurantclient;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartMultipartRequest;

/**
 * Created by piotr on 26.03.15.
 */
public class DAO {

    private static String TAG = DAO.class.getName();
    public final static String API = "http://188.166.101.11:6999/";

    //public final static String API = "http://192.168.1.2:8000/";
    public final static String API_KEY = "AIzaSyA3e8Tx1lM1Z0olVKSMYilfTo6PoOKaLNA";
    public final static String API_PLACE_KEY = "AIzaSyA3e8Tx1lM1Z0olVKSMYilfTo6PoOKaLNA";

    public final static String API_V1_STATIC = "http://188.166.101.11:7990/";

    public final static String API_V1 = "http://188.166.101.11:7990/v1/";

    /*

    Places requestes
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     */

    public static void geocodeIp(
            Response.Listener successListener,
            Response.ErrorListener errorListener) {


        String request = "http://188.166.101.11:7990/v1/geocoder?satelize";
        //String request = API + "restaurant/comment/?id=" + id;

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }



    public static void getPlacesByTag(String tag,
                                      Response.Listener successListener,
                                      Response.ErrorListener errorListener) {

        String request = API_V1 + "place?find.tags.$in=" + tag + ",";
        //String request = API + "restaurant/comment/?id=" + id;
        if (MyApplication.currentLocation != null) {
            LatLng position = MyApplication.currentLocation;
            request += "&location=" + Double.toString(position.latitude) + "," + Double.toString(position.longitude);
        }

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void getPlaces(
            Response.Listener successListener,
            Response.ErrorListener errorListener, String query, LatLng position, String placeId) {
        String request = API_V1;

        if (placeId != null)
            request += "place?place_id=" + placeId;
        else if (query == null && position != null)
            request += "place?location=" + Double.toString(position.latitude) + "," + Double.toString(position.longitude) + "&geocode";
        else if (query != null && position != null)
            request += "place/search?input=" + query + "&location=" + Double.toString(position.latitude) + "," + Double.toString(position.longitude);



        Log.d(TAG, request);
        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener) {

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);


        rq.start();

    }

    /*
    public static void getPlaceDetails(
            Response.Listener successListener,
            Response.ErrorListener errorListener, String id) {


        String request = API + "place/" + id;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }
*/

    /*

    User requestes
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
     */


    public static void updateUserAvatar(final File file,
                                        Response.Listener successListener,
                                        Response.ErrorListener errorListener) {


        String request = API + "profile/set/avatar";

        Log.d(TAG, request);

        SmartMultipartRequest jr = new SmartMultipartRequest(Request.Method.PUT, request,
                errorListener, successListener, file, "file", null) {
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


    public static void loginUser(final JSONObject data,
                                 Response.Listener successListener,
                                 Response.ErrorListener errorListener) {


        //String request = API + "api-token-auth/";

        String request = API_V1 + "User/auth";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.POST, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void registerUser(final JSONObject data,
                                    Response.Listener successListener,
                                    Response.ErrorListener errorListener) {


        String request = API_V1 + "User/auth";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.PUT, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void getProfile(
            Response.Listener successListener,
            Response.ErrorListener errorListener) {


        String request = API_V1 + "profile";
        //String request = API + "user";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener) {
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                //headers.put("Authorization","Token "+ MyApplication.token);
                return headers;
            }

        };
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void updateProfil(
            Response.Listener successListener,
            Response.ErrorListener errorListener, final JSONObject data) {


        String request = API_V1 + "User";
        //String request = API + "user";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.PUT, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);

                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    /*

    Commnets requestes
    /////////////////////////////////////////////////////////////////////////////////

     */

    public static void getPlaceComments(String id, String page, String count,
                                        Response.Listener successListener,
                                        Response.ErrorListener errorListener) {


        String request = API_V1 + "comment?find.owner=" + id + "&populate.media&page=" + page + "&perPage=" + count;
        //String request = API + "restaurant/comment/?id=" + id;

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void getUserComments(
            Response.Listener successListener,
            Response.ErrorListener errorListener,String id) {


        String request = API_V1 + "comment?find.profile="+id+"&populate.owner";
        //String request = API + "restaurant/comment/?id=" + id;

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener) {
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                //headers.put("Authorization","Token "+ MyApplication.token);
                return headers;
            }

        };


        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


    public static void updateImageToComment(final File file,
                                            Response.Listener successListener,
                                            Response.ErrorListener errorListener) {


        String request = API_V1 + "files";

        Log.d(TAG, request);

        SmartMultipartRequest jr = new SmartMultipartRequest(Request.Method.PUT, request,
                errorListener, successListener, file, "file", null) {
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


    public static void addCommentToPlace(final JSONObject data,
                                         Response.Listener successListener,
                                         Response.ErrorListener errorListener) {


        String request = API_V1 + "comment";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.POST, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);

                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void getProductComment(String id, String page,
                                         Response.Listener successListener,
                                         Response.ErrorListener errorListener) {


        //String request = API + "restaurant/" + id + "/comments/" + page;
        String request = "http://192.168.1.2:8000/" + "product/comment/?id=" + id;

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void getWikipediaDescription(String query,
                                               Response.Listener successListener,
                                               Response.ErrorListener errorListener) {


        //String request = API + "restaurant/" + id + "/comments/" + page;
        String request = "https://pl.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=" + query;

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


    /*
    Activityes
    /////////////////////////////////////////////////////////////////////////////////

     */

    public static void getUserActivities(
            Response.Listener successListener,
            Response.ErrorListener errorListener) {


        String request = API_V1 + "activity?populate.Comment";
        //String request = API + "restaurant/comment/?id=" + id;

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener) {
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                //headers.put("Authorization","Token "+ MyApplication.token);
                return headers;
            }

        };


        jr.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


    public static void getAutocomplete(RequestQueue rq,
                                       Response.Listener successListener,
                                       Response.ErrorListener errorListener, String query) {
        String request = API;


        request += "place/autocomplete/" + query;

        final JSONObject data = new JSONObject();
        try {


            data.put("autocomplete", true);


            if (MyApplication.currentLocation != null) {
                JSONArray location = new JSONArray();
                location.put(MyApplication.currentLocation.latitude);
                location.put(MyApplication.currentLocation.longitude);
                data.put("location", location);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.PUT, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        rq.cancelAll("autocomplete");
        jr.setTag("autocomplete");
        rq.add(jr);
        rq.start();


    }

    public static void getProducts(Response.Listener listener, Response.ErrorListener errorListener, String restaurant_id, String category_id) {

        String request = "";
        if (category_id == null)
            request = API_V1 + "product?find.owner=" + restaurant_id + "&method=getRoots";
        else
            request = API_V1 + "product?find.category=" + category_id;


        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);

        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getRestaurantCategories(String id, Response.Listener listener, Response.ErrorListener errorListener, String parentId) {
        String request = "";
        if (parentId == null)
            request = API_V1 + "category?find.owner=" + id + "&method=getRoots";
        else
            request = API_V1 + "category?_id=" + parentId + "&method=getChildren";


        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);

        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    public static void createOrder(final JSONObject data,
                                   Response.Listener successListener,
                                   Response.ErrorListener errorListener) {


        String request = API_V1 + "order";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.POST, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("token", MyApplication.token);

                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


}
