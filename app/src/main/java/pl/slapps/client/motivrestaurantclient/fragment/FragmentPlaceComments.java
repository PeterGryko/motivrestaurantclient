package pl.slapps.client.motivrestaurantclient.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.page.PageGoogleComments;
import pl.slapps.client.motivrestaurantclient.page.PageMotivComments;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 18.07.15.
 */
public class FragmentPlaceComments extends SmartPitPagerFragment  implements RestaurantInterface {
    private String TAG = FragmentNewComment.class.getName();
    private LinearLayout layout_comment;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_place_comments, parent, false);

        v.setLayoutParams(new LinearLayout.LayoutParams(SmartPitAppHelper.getScreenWidth(this.getActivity()), ViewGroup.LayoutParams.MATCH_PARENT));

        if (this.getArguments() != null) {
            String data = this.getArguments().getString("data");
            Place model = null;
            try {
                model = new Place(data);

                ArrayList<View> list = new ArrayList<>();
                list.add(new PageMotivComments().createView(inflater, this.getActivity(), model));

                list.add(new PageGoogleComments().createView(inflater, this.getActivity(), model));

                this.setViewsPager(v, R.id.pager, list);

                this.initMovingIndicator(v, R.id.smart_indicator, inflater.inflate(R.layout.profile_indicator_view, null), false);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return v;
    }

    @Override
    public View createTabIndicator(Context context, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.pager_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_label);
        switch (i) {
            case 0:
                tv.setText("jemjem");
                break;
            case 1:
                tv.setText("google");
                break;
        }

        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }


}
