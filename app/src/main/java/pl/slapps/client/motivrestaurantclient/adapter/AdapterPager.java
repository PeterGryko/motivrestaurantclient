package pl.slapps.client.motivrestaurantclient.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;



import java.util.ArrayList;

/**
 * Created by piotr on 24.03.15.
 */
public class AdapterPager extends PagerAdapter {

    private String TAG = AdapterPager.class.getName();
    private ArrayList<View> list;
    private Context context;


    public AdapterPager(Context context,
                        ArrayList<View> list) {

        this.context = context;
        this.list = list;


    }

    public void addView(View v)
    {
        list.add(v);
        notifyDataSetChanged();
    }
    public void removeView(View v)
    {
        list.remove(v);
        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);

        //SmartPitAppHelper.getInstance(context).stripViewGroup((SmartImageView) object, true);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        View row = list.get(position);

        ((ViewPager) container).addView(row);


        return row;

    }
}