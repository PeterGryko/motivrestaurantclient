package pl.slapps.client.motivrestaurantclient.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


import org.json.JSONArray;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitImagesFlipperAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 31.03.15.
 */
public class FragmentInfo extends SmartPitFragment {
    private ViewPager pager;
    private SmartPitImagesFlipperAdapter adapter;
    private TextView tvName;
    private TextView tvAddress;
    private TextView tvDesc;

    private ImageButton btnComment;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_info, parent, false);
        pager = (ViewPager) v.findViewById(R.id.pager);

        tvName = (TextView) v.findViewById(R.id.tv_name);
        tvAddress = (TextView) v.findViewById(R.id.tv_address);
        tvDesc = (TextView) v.findViewById(R.id.tv_desc);

        btnComment = (ImageButton) v.findViewById(R.id.btn_comment);

        final Place model = (Place)MyApplication.getCurrentRestaurant();
        //final MapRestaurantModel model = MapRestaurantModel.valueOfLocal(MyApplication.getCurrentRestaurant());

        tvName.setText(model.getGName());
        tvAddress.setText(model.getRAddress());
        tvDesc.setText(model.getRDesc());

        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) FragmentInfo.this.getActivity()).switchDrawerFragment(new FragmentNewComment());
                ((MainActivity) FragmentInfo.this.getActivity()).showMasterMenu();
            }
        });
        if (model.getRMedia() != null) {
            ArrayList<String> images = model.getRMedia();


            adapter = new SmartPitImagesFlipperAdapter(this.getActivity(), images, 0, 0);
            adapter.setOnelementClickListener(new SmartPitImagesFlipperAdapter.OnElementClickedListener() {
                @Override
                public void onClick(int position) {

                    JSONArray array = new JSONArray();
                    for (int i = 0; i < model.getRMedia().size(); i++) {
                        array.put(model.getRMedia().get(i));
                    }
                    Bundle arg = new Bundle();
                    arg.putString("data", array.toString());
                    FragmentGallery fg = new FragmentGallery();
                    fg.setArguments(arg);
                    ((MainActivity) FragmentInfo.this.getActivity()).switchDrawerFragment(fg);
                    ((MainActivity) FragmentInfo.this.getActivity()).showMasterMenu();
                }
            });
            pager.setAdapter(adapter);
        }

        return v;
    }


    @Override
    public String getLabel() {
        return null;
    }
}
