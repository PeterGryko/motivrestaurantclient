package pl.slapps.client.motivrestaurantclient.page;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.AdapterView;


import com.github.ksoichiro.android.observablescrollview.ObservableListView;

import org.json.JSONException;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitBaseFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.adapter.AdapterNearby;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentPlaceDetails;

/**
 * Created by piotr on 27.05.15.
 */
public class PageNearest {

    private ObservableListView lv;
    private ArrayList<Place> list;
    //private Context context;
    private MainActivity context;
    //private LinearLayout bottom;
    //private Context context;

    private int last_position;

    //private Animation show_bottom_animation;
    //private Animation hide_bottom_animation;

    private Animation show_rewind_animation;
    private Animation hide_rewind_animation;

    private SmartPitSelectorView btn_rewind;

    private String TAG = PageNearest.class.getName();

    private void setLastPosition(int position) {
     //   if (last_position > position && bottom.getVisibility() == View.GONE && !(show_bottom_animation.hasStarted() && !show_bottom_animation.hasEnded())) {
     //       bottom.startAnimation(show_bottom_animation);
     //   } else if (last_position < position && bottom.getVisibility() == View.VISIBLE && !(hide_bottom_animation.hasStarted() && !hide_bottom_animation.hasEnded())) {
     //       bottom.startAnimation(hide_bottom_animation);
     //   }
        last_position = position;

        if (position > 4 && btn_rewind.getVisibility() == View.GONE) {
            btn_rewind.startAnimation(show_rewind_animation);
        } else if (position < 4 && btn_rewind.getVisibility() == View.VISIBLE) {
            btn_rewind.startAnimation(hide_rewind_animation);
        }

    }

    /*
    private void initBottomAnimations(View v) {
        show_bottom_animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ViewCompat.setTranslationY(bottom, bottom.getHeight() - (bottom.getHeight() * interpolatedTime));
            }
        };
        show_bottom_animation.setDuration(400);
        show_bottom_animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                bottom.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        hide_bottom_animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ViewCompat.setTranslationY(bottom, (bottom.getHeight() * interpolatedTime));
            }
        };
        hide_bottom_animation.setDuration(400);

        hide_bottom_animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bottom.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
*/

    private void initRewindAnimations(View v) {
        show_rewind_animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ViewCompat.setScaleX(btn_rewind, interpolatedTime);
                ViewCompat.setScaleY(btn_rewind, interpolatedTime);

            }
        };
        show_rewind_animation.setDuration(300);
        show_rewind_animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btn_rewind.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        hide_rewind_animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                ViewCompat.setScaleX(btn_rewind, 1 - interpolatedTime);
                ViewCompat.setScaleY(btn_rewind, 1 - interpolatedTime);
            }
        };
        hide_rewind_animation.setDuration(300);

        hide_rewind_animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btn_rewind.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public View createView(MainActivity context, LayoutInflater inflater) {
        this.context = context;
        View v = inflater.inflate(R.layout.page_nearest, null);

        lv = (ObservableListView) v.findViewById(R.id.lv);
       // bottom = (LinearLayout) v.findViewById(R.id.bottom);
        btn_rewind = (SmartPitSelectorView) v.findViewById(R.id.btn_rewind);

        btn_rewind.configure(R.drawable.circle_sel, R.drawable.circle_profile_sel, R.drawable.rewind);
        btn_rewind.getImageView().setPadding(10, 10, 10, 10);
        btn_rewind.setVisibility(View.GONE);
        btn_rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lv.smoothScrollToPosition(0);
            }
        });
        //initBottomAnimations(v);
        initRewindAnimations(v);


        refreashData(MyApplication.getCurrentPlaces());

        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

                setLastPosition(i);
                Log.d(TAG, "scroll " + i + " ## " + i1 + " ## " + i2);

            }
        });


        return v;
    }

    public void refreashData(ArrayList<Place> l) {


        this.list = new ArrayList<>();

        ArrayList<Place> images_list = new ArrayList<>();
        ArrayList<Place> names_list = new ArrayList<>();
        ArrayList<Place> blank_list = new ArrayList<>();


        for (int i = 0; i < l.size(); i++) {
            if (!l.get(i).getRLogo().trim().equals("")) {
                images_list.add(l.get(i));
            } else if (!l.get(i).getGName().equals("")) {
                names_list.add(l.get(i));
            } else
                blank_list.add(l.get(i));


        }
        list.addAll(images_list);
        list.addAll(names_list);

        try {
            Place divider = new Place();

            divider.put("divider_label", "Pozostałe");
            list.add(divider);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        list.addAll(blank_list);


        lv.setAdapter(new AdapterNearby(context, list));


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                String id = list.get(i).getGPlaceId();
                if (((SmartPitBaseFragment) context.getCurrentDrawerFragment()).getCurrentFragment() instanceof FragmentPlaceDetails) {
                    ((FragmentPlaceDetails) ((SmartPitBaseFragment) context.getCurrentDrawerFragment()).getCurrentFragment()).refreashData(id);
                } else {

                    Bundle arg = new Bundle();
                    arg.putString("place_id", id);
                    FragmentPlaceDetails fd = new FragmentPlaceDetails();
                    fd.setArguments(arg);
                    context.switchDrawerFragment(fd);
                }

            }

        });
    }
}
