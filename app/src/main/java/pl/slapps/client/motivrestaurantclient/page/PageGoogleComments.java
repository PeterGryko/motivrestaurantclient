package pl.slapps.client.motivrestaurantclient.page;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.adapter.AdapterGoogleReviews;

/**
 * Created by piotr on 19.07.15.
 */
public class PageGoogleComments {


    private String TAG = PageMotivComments.class.getName();
    private LinearLayout layout_info;
    private ListView lv;


    public View createView(LayoutInflater inflater, Context context, Place model) {
        View v = inflater.inflate(R.layout.page_google_comments, null);
        layout_info = (LinearLayout) v.findViewById(R.id.info_layout);
        lv = (ListView) v.findViewById(R.id.lv);


        JSONArray array = model.getGReviews();
        ArrayList<JSONObject> data = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {

                data.add(array.getJSONObject(i));

            }
            if (data.size() > 0) {
                lv.setAdapter(new AdapterGoogleReviews(context, data));
                layout_info.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return v;
    }
}
