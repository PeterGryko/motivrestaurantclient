package pl.slapps.client.motivrestaurantclient.adapter;

import android.content.Context;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitGoogleAddressesAdapter;
import pl.gryko.smartpitlib.model.SmartPitGoogleAddress;


/**
 * Created by piotr on 29.03.15.
 */
public class AdapterCities extends SmartPitGoogleAddressesAdapter {
    public AdapterCities(Context context, int res) {
        super(context, res);
    }

    public void populateResult(SmartPitGoogleAddress element, ArrayList<SmartPitGoogleAddress> list) {
        if (element.getCity() != null && !element.getCity().equals("")) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getCity().trim().equals(element.getCity().trim()))
                    return;
            }
            super.populateResult(element, list);

        }
    }


    public String getItem(int index) {
        return this.getPoint(index).getCity();
    }
}
