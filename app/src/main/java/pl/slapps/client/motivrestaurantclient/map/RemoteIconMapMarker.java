package pl.slapps.client.motivrestaurantclient.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import com.android.volley.VolleyError;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.PolyUtil;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 30.03.15.
 */
public class RemoteIconMapMarker {


    private Marker marker;

    private float maxSize = 70;

    private String TAG = RemoteIconMapMarker.class.getName();

    public RemoteIconMapMarker(Context context) {
        maxSize = context.getResources().getDimension(R.dimen.map_marker_size);
    }

    private Bitmap scaleBitmap(Bitmap b, boolean crop) {
        float ratio = 0;


        if (b.getHeight() > b.getWidth())
            ratio = maxSize / b.getHeight();
        else
            ratio = maxSize / b.getWidth();

        float newWidth = b.getWidth() * ratio;
        float newHeight = b.getHeight() * ratio;
        //  BitmapFactory.Options options = new BitmapFactory.Options();
        //  options.inScaled = false;
        //return b;
        Bitmap bm = Bitmap.createScaledBitmap(b, (int) newWidth, (int) newHeight, true);
        if (crop)
            return getCroppedBitmap(bm, (int) maxSize);
        else
            return bm;

    }

    public void initMarker(final Marker marker, String url, final boolean crop) {
        SmartPitActivity.getImageLoader()
                .get(url, new SmartPitImageLoader.SmartImagesListener() {
                    @Override
                    public void onResponse(SmartPitImageLoader.SmartImageContainer container, boolean flag) {

                        if (container.getBitmap() == null)
                            return;
                        Bitmap scaledBitmap = scaleBitmap(container.getBitmap(), crop);
                        try {
                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(scaledBitmap));
                            Log.d(TAG, "image loaded " + scaledBitmap.getHeight() + " " + scaledBitmap.getWidth());

                        } catch (Throwable t) {
                            // t.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, 0, 0);
    }

    public Marker init(LatLng position, String url, String name, GoogleMap map, boolean crop) throws Exception {


        marker = map.addMarker(new MarkerOptions().position(position).title(name));

        initMarker(marker, url, crop);

        return marker;
    }

    public Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
        Bitmap sbmp = null;
        if (bmp.getWidth() != radius || bmp.getHeight() != radius)
            sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
        else
            sbmp = bmp;
        Bitmap output = Bitmap.createBitmap(sbmp.getWidth(),
                sbmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xffa19774;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399"));
        canvas.drawCircle(sbmp.getWidth() / 2 + 0.7f, sbmp.getHeight() / 2 + 0.7f,
                sbmp.getWidth() / 2 + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(sbmp, rect, rect, paint);


        return output;
    }


}
