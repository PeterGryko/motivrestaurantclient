package pl.slapps.client.motivrestaurantclient.page;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 27.05.15.
 */
public class PageNews {
    private String TAG = PageNews.class.getName();
    private ListView lv;
    private LinearLayout info_layout;

    public View createView(LayoutInflater inflater) {
        View v = inflater.inflate(R.layout.page_news, null);
        lv = (ListView) v.findViewById(R.id.lv);
        info_layout = (LinearLayout) v.findViewById(R.id.info_layout);

        DAO.getUserActivities(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
                AppHelper.getErrorMessage(volleyError);
            }
        });
        return v;
    }
}
