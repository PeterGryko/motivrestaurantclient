package pl.slapps.client.motivrestaurantclient.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.client.motivrestaurantclient.DAO;

/**
 * Created by piotr on 02.10.15.
 */
public class Product {


    public String name;
    public String price;
    public String desc;
    public String category;
    public ArrayList<String> urls;
    public JSONObject data;
    public JSONArray ingredients;

    public Product(JSONObject data) {
        try {
            this.data=data;
            name = data.has("name") ? data.getString("name") : "";
            JSONObject d = data.has("data") ? data.getJSONObject("data") : new JSONObject();
            price = data.has("price") ? data.getString("price") : "";

            urls = new ArrayList<>();

            JSONArray photos = data.has("photos") ? data.getJSONArray("photos") : new JSONArray();
            for (int i = 0; i < photos.length(); i++) {
                JSONObject photo = photos.getJSONObject(i);
                String url = photo.has("path") ? photo.getString("path") : "";
                urls.add(DAO.API_V1_STATIC + url);
            }

            JSONObject cat = data.has("category") ? data.getJSONObject("category") : new JSONObject();
            category = cat.has("label") ? cat.getString("label") : "";


            ingredients = data.has("ingredients") ? data.getJSONArray("ingredients") : new JSONArray();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
