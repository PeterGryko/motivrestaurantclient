package pl.slapps.client.motivrestaurantclient.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitImagesFlipperAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 02.04.15.
 */
public class FragmentGallery extends SmartPitFragment implements RestaurantInterface {
    private ViewPager pager;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_gallery, parent, false);
        pager = (ViewPager) v.findViewById(R.id.pager);

        if (this.getArguments() != null) {

            refreashPager(this.getArguments().getString("data"));

        }


        return v;
    }

    public void refreashPager(String data) {
        try {
            JSONArray array = new JSONArray(data);
            ArrayList<String> images = new ArrayList<>();


            for (int i = 0; i < array.length(); i++) {
                images.add(array.getString(i));
            }

            pager.setAdapter(new SmartPitImagesFlipperAdapter(this.getActivity(), images, 0, 0));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
