package pl.slapps.client.motivrestaurantclient.map;

import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.map.MapHelper;

/**
 * Created by piotr on 05.04.15.
 */
public class AdapterMapWindow implements GoogleMap.InfoWindowAdapter {

    private MainActivity context;
    private MapHelper helper;


    public AdapterMapWindow(MainActivity context, MapHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View v = null;


        String entry = marker.getTitle();

        String[] split = entry.split(MapHelper.SEPARATOR);
        if (split[0].equals(MapHelper.PLACE))
            v = helper.getPlacesHelper().getPlaceInfoWindow(split[1], marker);
        else if (split[0].equals(MapHelper.ROUTE))

            v = helper.getRouteHelper().getRouteInfoWindow(split[1], marker);


        return v;
    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;

    }
}
