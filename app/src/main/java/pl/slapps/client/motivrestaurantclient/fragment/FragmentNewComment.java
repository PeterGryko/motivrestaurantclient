package pl.slapps.client.motivrestaurantclient.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;
import pl.slapps.client.motivrestaurantclient.widget.RatingWidget;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 31.03.15.
 */
public class FragmentNewComment extends SmartPitFragment  implements RestaurantInterface {

    private String TAG = FragmentNewComment.class.getName();
    private EditText etBody;

    private String ownerId;

    private SmartPitSelectorView btn_comment;
    private SmartPitSelectorView gallery_pick;
    private SmartPitSelectorView camera_pick;

    //private LinearLayout layout_comment;

    private TextView tv_label;
    private SmartImageView iv_logo;

    private ImageView iv_preview;


    private File picked_file;

    private RatingWidget rating;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_comment, parent, false);

        btn_comment = (SmartPitSelectorView) v.findViewById(R.id.btn_comment);
        camera_pick = (SmartPitSelectorView) v.findViewById(R.id.camera_pick);
        gallery_pick = (SmartPitSelectorView) v.findViewById(R.id.gallery_pick);
        rating = (RatingWidget)v.findViewById(R.id.rating);

        etBody = (EditText) v.findViewById(R.id.et_comment);
        //layout_comment = (LinearLayout) v.findViewById(R.id.btn_comment);

        btn_comment.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.comment);
        btn_comment.getImageView().setPadding(15, 15, 15, 15);

        gallery_pick.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.gallery);
        gallery_pick.getImageView().setPadding(10, 10, 10, 10);

        camera_pick.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.camera);
        camera_pick.getImageView().setPadding(10, 10, 10, 10);

        iv_preview = (ImageView) v.findViewById(R.id.iv_preview);

        gallery_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentNewComment.this.getActivity()).pickImageFromGallery(MainActivity.PICK_TYPE.COMMENT_IMAGE);
            }
        });
        camera_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentNewComment.this.getActivity()).pickImageFromCamera(MainActivity.PICK_TYPE.COMMENT_IMAGE);
            }
        });

        tv_label = (TextView) v.findViewById(R.id.tv_label);
        iv_logo = (SmartImageView) v.findViewById(R.id.iv_logo);
        iv_logo.setErrorImage(this.getResources().getDrawable(R.drawable.icon_restaurant));


        parseInitialArgs();
        return v;
    }

    private void parseInitialArgs() {
        if (this.getArguments() != null) {

            String data = getArguments().getString("data");
            try {
                Place model = new Place(data);

                tv_label.setText(model.getGName());
                SmartPitImageLoader.setImage(FragmentNewComment.this.getActivity(), iv_logo, model.getRLogo(), 50, 50);


                ownerId = model.getRId();
                btn_comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addComment();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void performRequest(JSONObject data) {
        DAO.addCommentToPlace(data, new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
                Toast.makeText(FragmentNewComment.this.getActivity(), "Komentarz został dodany", Toast.LENGTH_LONG).show();
                ((MainActivity) FragmentNewComment.this.getActivity()).onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                Log.d(TAG, AppHelper.getErrorMessage(error));
            }
        });
    }


    private void addComment() {
        final JSONObject data = new JSONObject();
        try {
            JSONObject p = MyApplication.getCurrentUser();

            JSONObject d = p.has("data") ? p.getJSONObject("data") : new JSONObject();
            String username = d.has("username") ? d.getString("username") : "";
            JSONObject avatar = p.has("avatar") && !p.isNull("avatar") ? p.getJSONObject("avatar") : new JSONObject();
            String path = avatar.has("path") ? avatar.getString("path") : "";

            data.put("content", etBody.getText().toString());
            data.put("owner", ownerId);

            JSONObject user = new JSONObject();
            user.put("username", username);
            user.put("avatar", path);
            user.put("rate",rating.getScore());

            data.put("data", user);

            Log.d(TAG, data.toString());

            if (picked_file != null) {
                Log.d(TAG, "file picked");
                DAO.updateImageToComment(picked_file, new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {

                        Log.d(TAG, "response : "+ o.toString());
                        String id = null;
                        try {
                            JSONObject result = new JSONObject(o.toString());

                            id = result.has("_id") ? result.getString("_id") : "";

                            data.put("media", id);


                            performRequest(data);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());
                    }
                });
            } else
                performRequest(data);
            /*


            */
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onImagePick(Bitmap bitmap, File file) {
        picked_file = file;
        if (iv_preview != null)
            iv_preview.setImageBitmap(bitmap);
    }

    @Override
    public String getLabel() {
        return null;
    }
}
