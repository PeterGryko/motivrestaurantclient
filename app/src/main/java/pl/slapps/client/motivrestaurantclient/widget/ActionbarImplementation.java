package pl.slapps.client.motivrestaurantclient.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartActionbarToggler;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.adapter.AdapterPlaces;

/**
 * Created by piotr on 02.04.15.
 */
public class ActionbarImplementation {

    class MyAlphaAnimation {
        private int start_value;
        private int end_value;
        private View view;
        private Animation animation;

        public MyAlphaAnimation(View view) {
            this.view = view;
            animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    setAnimationProgress(interpolatedTime);
                }
            };
        }

        private void setAnimationProgress(float interpolatedTime) {

            int alpha = (int) (start_value + (end_value - start_value) * interpolatedTime);
            actionbarLayout.setBackgroundColor(Color.argb(alpha, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));

            //ViewCompat.setAlpha(view, alpha);
            //setBackgroundAlphaFromDefaultRange(alpha);
        }

        public MyAlphaAnimation setValues(int start, int end) {
            this.start_value = start;
            this.end_value = end;
            return this;
        }

        public void startAnimation(long duration) {
            current_alpha = this.end_value;

            animation.setDuration(duration);
            view.startAnimation(animation);
        }


    }


    private ActionBar ab;

    private String TAG = ActionbarImplementation.class.getName();

    //private FragmentMap fragmentMap;

    private SmartActionbarToggler itemToogle;
    private ImageView itemSearch;
    private ImageView itemCart;

    //private ImageView itemRanking;
    private SmartActionbarToggler itemProfile;

    //private ImageView itemBack;

    private AutoCompleteTextView searchbar;
    private RelativeLayout searchbarBase;
    private RelativeLayout restaurantBase;

    private RelativeLayout actionbarLayout;

    private MainActivity context;
    private AdapterPlaces adapter;
    private InputMethodManager imm;
    private SmoothProgressBar bar;

    //private AnimatedTextView tv_label;

    private MyAlphaAnimation alpha_animation;
    private int current_alpha = 100;
    private int ab_color;


    private TextView restaurantLabel;


    public SmoothProgressBar getProgressBar() {
        return bar;
    }


    //public AnimatedTextView getAnimatedLabel() {
    //    return tv_label;
    // }


    public ActionbarImplementation(final MainActivity context) {
        this.context = context;
        imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        ab = context.getSupportActionBar();
        //ab.setDisplayHomeAsUpEnabled(true);
        ab.setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        ab.setDisplayShowTitleEnabled(false);
        ab.setDisplayShowCustomEnabled(true);


        actionbarLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.actionbar_layout, null);
        ab_color = context.getResources().getColor(R.color.profile_indicator_on);


        actionbarLayout.setBackgroundColor(Color.argb(current_alpha, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));
        searchbar = (AutoCompleteTextView) actionbarLayout.findViewById(R.id.searchbar);
        searchbarBase = (RelativeLayout) actionbarLayout.findViewById(R.id.searchbar_base);
        alpha_animation = new MyAlphaAnimation(actionbarLayout);

        restaurantBase = (RelativeLayout) actionbarLayout.findViewById(R.id.restaurant_base);

        restaurantLabel = (TextView) actionbarLayout.findViewById(R.id.restaurant_label);
        //tv_label = (AnimatedTextView) actionbarLayout.findViewById(R.id.tv_label);
        //tv_label.setPadding(0, 0, SmartPitAppHelper.getScreenWidth(context), 0);

        adapter = new AdapterPlaces(context);
        searchbar.setAdapter(adapter);
        bar = (SmoothProgressBar) actionbarLayout.findViewById(R.id.bar);
        bar.setVisibility(View.INVISIBLE);
        searchbar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Place model = adapter.getPoint(position);


                Log.d(TAG, "place selected");

                context.fetchPlaces(null, model.getGPlaceId());
                imm.hideSoftInputFromWindow(searchbar.getWindowToken(), 0);
/*
                }
*/
            }
        });

        searchbar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    context.fetchPlaces(getSearchtext(), null);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    searchbar.clearFocus();
                    return true;
                }
                return false;
            }
        });

        searchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d(TAG, "after text changed " + editable.toString());
                //  String part = editable.toString();
                // if (part.equals("")) {
                //     context.resetMap();
                //  }
                //else if (!part.trim().equals(""))
                //    adapter.getFilter().filter(part);

            }
        });

        ab.setCustomView(actionbarLayout, new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT));

        Toolbar toolbar = (Toolbar) actionbarLayout.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);

        Toolbar parent = (Toolbar) actionbarLayout.getParent();//first get parent toolbar of current action bar
        parent.setContentInsetsAbsolute(0, 0);
        itemToogle = (SmartActionbarToggler) actionbarLayout.findViewById(R.id.menu_settings);
        itemToogle.setMode(SmartActionbarToggler.ToggleMode.STRIPES);
        itemToogle.setGravity(Gravity.RIGHT);

        itemCart = (ImageView) actionbarLayout.findViewById(R.id.menu_cart);


        itemSearch = (ImageView) actionbarLayout.findViewById(R.id.menu_search);
        //itemRanking = (ImageView) actionbarLayout.findViewById(R.id.menu_stats);
        itemProfile = (SmartActionbarToggler) actionbarLayout.findViewById(R.id.menu_profile);
        itemProfile.setMode(SmartActionbarToggler.ToggleMode.ARROW);
        itemProfile.setAnimatedOffset(context.getResources().getDimension(R.dimen.actionbar_padding));

        //itemProfile.setArrowWieght(3f);
        // itemBack = (ImageView) actionbarLayout.findViewById(R.id.menu_back);

        //setRestauraurantControls();


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.onActionbarItemSelected(v);
            }
        };
        // itemRanking.setOnClickListener(listener);
        itemProfile.setOnClickListener(listener);
        itemSearch.setOnClickListener(listener);
        itemToogle.setOnClickListener(listener);
        itemCart.setOnClickListener(listener);
        restaurantBase.setOnClickListener(listener);
        //   itemBack.setOnClickListener(listener);


    }

    public void setRestaurantLabel(String label) {
        restaurantLabel.setText(label);
    }


    public String getSearchtext() {
        String v = "";
        if (searchbar != null)
            v = searchbar.getText().toString();

        return v;
    }

    //public View getSearchbarBase() {
    //    return searchbarBase;
    // }

    public void showRestaurantBase() {
        //actionbarLayout.setBackgroundColor(Color.argb(255, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));
        alpha_animation.setValues(current_alpha,255).startAnimation(500);
        SmartPitAppHelper.showViewWithAnimation(restaurantBase, 300);
        //SmartPitAppHelper.hideViewWithAnimation(searchbarBase, 300);
        searchbarBase.setVisibility(View.GONE);
        restaurantBase.bringToFront();


    }

    public void showSearchbarBase() {
        alpha_animation.setValues(current_alpha,100).startAnimation(500);

        //actionbarLayout.setBackgroundColor(Color.argb(100, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));
        SmartPitAppHelper.hideViewWithAnimation(restaurantBase, 300);
        SmartPitAppHelper.showViewWithAnimation(searchbarBase, 300);
        searchbarBase.bringToFront();

    }
    public void showProfileBase()
    {
        alpha_animation.setValues(current_alpha,0).startAnimation(500);

    }
    //public RelativeLayout getRestaurantBase() {
    //     return restaurantBase;
    // }

    public void setSearchtext(String text) {
        if (searchbar != null)
            searchbar.setText(text);
    }

    public void resetSerachBar() {
        searchbar.setText("");
    }

    public ActionBar getAb() {
        return ab;
    }

    public void finishCloseAnimation(boolean right) {

        itemProfile.startFinishCloseAnimation();
        itemToogle.startFinishCloseAnimation();
    }

    public void finishOpenAnimation(boolean right) {

        itemProfile.startFinishOpenAnimation();
        itemToogle.startFinishOpenAnimation();
    }

    public void animateActionbar(float offset, boolean right) {


        //int padding = (int) (actionbarAnimatedLayout.getWidth() - actionbarAnimatedLayout.getWidth() * offset);
        int searchPadding = (int) (searchbarBase.getWidth() * offset);
        //int labelPadding = tv_label.getWidth() - (int) (tv_label.getWidth() * offset);

        if (right) {
            itemProfile.setMode(SmartActionbarToggler.ToggleMode.ARROW);
            //actionbarAnimatedLayout.setPadding(padding, 0, 0, 0);
            //searchbarBase.setPadding(0, 0, searchPadding, 0);
            itemProfile.update(offset);


            itemToogle.update(offset);


        } else {

            //setBackgroundAlphaFromDefaultRange(1 - offset);
            //actionbarLayout.setBackgroundColor(Color.argb(out[0], out[1], out[2], out[3]));
            itemProfile.setMode(SmartActionbarToggler.ToggleMode.STRIPES);
            searchbarBase.setPadding(searchPadding, 0, 0, 0);
            //tv_label.setPadding(0, 0, labelPadding, 0);
            itemProfile.update(offset);

        }


    }


    public View getActionbarLayout() {
        return actionbarLayout;
    }

    public void hideBkg() {
        actionbarLayout.setBackgroundColor(Color.TRANSPARENT);
    }

    public void showBkg() {
        actionbarLayout.setBackgroundColor(context.getResources().getColor(R.color.heart));
    }


}
