package pl.slapps.client.motivrestaurantclient.user;

import android.view.LayoutInflater;
import android.view.View;

import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 19.05.15.
 */
public class PageFavs {

    public View createView(LayoutInflater inflater)
    {
        return inflater.inflate(R.layout.page_favs,null);
    }
}
