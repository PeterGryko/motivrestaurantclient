package pl.slapps.client.motivrestaurantclient.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by piotr on 16.03.15.
 */
public class Order {

    public boolean checked;

    public String title;
    public double latitude;
    public double longitude;
    public String address;
    public String description;
    public String price;
    public String status;
    public String phone;
    public String id;
    public boolean on_place;

    public JSONArray cart;
    public JSONObject data;

    public Order() {
    }


    public Order(JSONObject data) {

        try {
            this.data=data;
            id = data.has("_id") ? data.getString("_id") : "";
            status = data.has("status") ? data.getString("status") : "";
            cart = data.has("cart") ? data.getJSONArray("cart") : new JSONArray();
            data = data.has("deliveryData") ? data.getJSONObject("deliveryData") : new JSONObject();
            address = data.has("street") ? data.getString("street") : "";
            price = data.has("price") ? data.getString("price") : "";
            phone = data.has("phone") ? data.getString("phone") : "";

            title = data.has("client") ? data.getString("client") : "";
            on_place = data.has("on_place") ? data.getBoolean("on_place") : false;
            latitude = data.has("lat") ? data.getDouble("lat") : 0;
            longitude = data.has("lon") ? data.getDouble("lon") : 0;




        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    public static JSONArray getMockOrders() {
        JSONArray data = new JSONArray();

        for (int i = 0; i < 16; i++) {
            JSONObject o = new JSONObject();

            try {
                o.put("title", "mock title " + i);

                o.put("latitude", 53.12577 + i);
                o.put("longitude", 23.128257 + i);
                o.put("address", "towarowa mock " + i);
                o.put("description", "mock descritpiton");
                o.put("price", 234);
                if (i < 5)
                    o.put("status", "new");
                else if (i < 10)
                    o.put("status", "production");
                else
                    o.put("status", "to_deliver");

                o.put("id", "sdf3453453534fgdfg");
                data.put(o);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return data;
    }


}
