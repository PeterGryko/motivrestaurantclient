package pl.slapps.client.motivrestaurantclient.service;

/**
 * Created by piotr on 01.05.15.
 */

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import io.socket.client.IO;
import io.socket.emitter.Emitter;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.model.Order;

/**
 * Foreground service. It creates a head view.
 * The pending intent allows to go back to the settings activity.
 */
public class BackgroundService extends Service {

    public interface OnEmitListener {
        public void onStatusChanged(JSONObject result);

        public void onCoordsChanged(JSONObject result);

        public void onMessage(JSONObject result);

    }

    private final IBinder mBinder = new LocalBinder();
    private io.socket.client.Socket socket;
    private Thread socketThread;


    private OnEmitListener onOrdersUpdateListener;

    private String TAG = BackgroundService.class.getName();

    public class LocalBinder extends Binder {
        public BackgroundService getService() {
            return BackgroundService.this;
        }
    }


    public void setOnOrdersUpdateListener(OnEmitListener onOrdersUpdateListener) {
        this.onOrdersUpdateListener = onOrdersUpdateListener;
    }


    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }


    public void initSocket(String orderId) {
        if (socketThread == null) {
            socketThread = new Thread(new ClientThread(orderId));
            socketThread.start();
        } else
            emitAuth(orderId);


    }


    @Override
    public void onDestroy() {

        Log.d(TAG, "service on destroy");
        if (socketThread != null) {
            socketThread.interrupt();
            socketThread = null;
        }
        if (socket != null) {
            socket.disconnect();
            socket.close();
        }
        stopForeground(true);
    }

    private void showNewOrderNotification(Order order) {

        //Define Notification Manager
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

//Define sound URI
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.order_dark)
                .setContentTitle("Zmiana statusu")

                .setContentText("Twoje zamówienie przeszło do statusu: " + order.status)
                .setSound(soundUri); //This sets the sound to play

//Display notification
        notificationManager.notify(0, mBuilder.build());
    }

    private void emitAuth(String roomId) {
        try {
            JSONObject o = new JSONObject();

            o.put("room", roomId);
            o.put("token", MyApplication.token);
            o.put("roomType", "order");

            Log.d(TAG, "emit auth " + o.toString());

            socket.emit("auth", o);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void emitMessage(String message, String orderId,String restaurantId) {
        if (socket == null || !socket.connected()) {
            return;
        }

        try {
            JSONObject msg = new JSONObject();
            msg.put("from", orderId);
            msg.put("to",restaurantId);
            msg.put("body", message);
            JSONObject senderDetails = new JSONObject();
            msg.put("senderDetails", senderDetails);


            socket.emit("msg", msg);
            Log.d(TAG,"emir message "+ msg.toString());


        } catch
                (Throwable t) {
            Log.d(TAG, t.toString());
        }
    }


    class ClientThread implements Runnable {

        private String roomId;

        public ClientThread(String id) {
            this.roomId = id;
        }

        public void run() {
            try {
                Log.d(TAG, "start service " + roomId);


                socket = IO.socket("http://188.166.101.11:7990");
                socket.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {

                        emitAuth(roomId);
                    }

                }).on("join", new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];

                        pl.gryko.smartpitlib.widget.Log.d(TAG, "join " + obj.toString());
                    }

                }).on("error", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                        pl.gryko.smartpitlib.widget.Log.d(TAG, "error " + args[0]);
                    }

                }).on("statusChange", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "statusChange " + obj.toString());

                        showNewOrderNotification(new Order(obj));
                        // Order newOrder = new Order(obj);
                        // Order oldOrder = AppHelper.getOrder(newOrder.id, orders);
                        //if (oldOrder != null)
                        //    oldOrder.status = newOrder.status;


                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onStatusChanged(obj);
                    }
                }).on("coordsUpdate", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "coordsUpdate " + obj.toString());

                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onStatusChanged(obj);
                    }
                }).on("newOrder", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "newOrder " + obj.toString());

                        //  Order order = new Order(obj);
                        //  orders.add(order);
                        // showNewOrderNotification(order);
                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onStatusChanged(obj);
                    }
                }).on("msg", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "msg" + obj.toString());

                        //  Order order = new Order(obj);
                        //  orders.add(order);
                        // showNewOrderNotification(order);
                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onMessage(obj);
                    }
                }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {

                        pl.gryko.smartpitlib.widget.Log.d(TAG, "event disconenct");
                    }

                }).on(io.socket.client.Socket.EVENT_ERROR, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "error " + args[0].toString());
                    }
                }).on(io.socket.client.Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "error connection " + args[0].toString());
                    }
                }).on(io.socket.client.Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "error timeout " + args[0].toString());
                    }
                });

                IO.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        //TODO: Make this more restrictive
                        Log.d(TAG, "verify " + hostname);
                        return true;
                    }
                });
                socket.connect();
                pl.gryko.smartpitlib.widget.Log.d(TAG, "socket connecting...");
                pl.gryko.smartpitlib.widget.Log.d(TAG, Boolean.toString(socket.connected()));


            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }

    }


}