package pl.slapps.client.motivrestaurantclient.page;

import android.view.LayoutInflater;
import android.view.View;

import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 15.07.15.
 */
public class PageSplashThree {

    private SmartPitSelectorView btn_enrol;

    public View createView(LayoutInflater inflater)
    {
        View v = inflater.inflate(R.layout.page_splash_three,null);

        btn_enrol = (SmartPitSelectorView)v.findViewById(R.id.btn_enrol);
        btn_enrol.configure(R.drawable.circle_sel,R.drawable.circle_bkg,R.drawable.enrol);
        btn_enrol.getImageView().setPadding(30,30,30,30);
        return v;
    }
}
