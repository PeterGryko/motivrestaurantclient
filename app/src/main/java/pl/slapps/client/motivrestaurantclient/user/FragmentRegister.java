package pl.slapps.client.motivrestaurantclient.user;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import pl.gryko.smartpitlib.adapter.SmartPitGoogleAddressesAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.model.SmartPitGoogleAddress;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 24.03.15.
 */
public class FragmentRegister extends SmartPitFragment {
    private String TAG = FragmentRegister.class.getName();
    private EditText etUsername;
    private EditText etEmail;
    private EditText etPassword;
    private AutoCompleteTextView etAddress;
    private SmartPitSelectorView btnRegister;
    private EditText etHouse;
    private EditText etFlat;
    private EditText etPhone;
    private SmartPitGoogleAddressesAdapter adapter;
    private double lat;
    private double lng;

    private boolean isFormValid() {


        boolean flag = true;


        if (etUsername.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etPassword.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etEmail.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etPhone.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etAddress.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etFlat.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etHouse.getText().toString().trim().equals("")) {
            flag = false;
        }

        if (!flag)
            Toast.makeText(this.getActivity(), "musisz wypelnic wszystkie pola", Toast.LENGTH_LONG).show();

        if (!SmartPitAppHelper.validateEmail(etEmail.getText().toString())) {
            Toast.makeText(this.getActivity(), "Email jest niepoprawny", Toast.LENGTH_LONG).show();
            flag = false;
        }

        if (lat == 0 || lng == 0) {
            flag = false;
            Toast.makeText(this.getActivity(), "musisz wybrać adres z listy", Toast.LENGTH_LONG).show();
        }

        return flag;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, parent, false);

        int ab_color = this.getActivity().getResources().getColor(R.color.profile_indicator_on);

        View actionbarLayout = v.findViewById(R.id.action_bar);
        actionbarLayout.setBackgroundColor(Color.argb(100, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));


        etUsername = (EditText) v.findViewById(R.id.et_username);
        etEmail = (EditText) v.findViewById(R.id.et_email);

        etPassword = (EditText) v.findViewById(R.id.et_password);

        etFlat = (EditText) v.findViewById(R.id.et_flat);
        etHouse = (EditText) v.findViewById(R.id.et_house);
        etPhone = (EditText) v.findViewById(R.id.et_phone);

        btnRegister = (SmartPitSelectorView) v.findViewById(R.id.btn_register);

        btnRegister.configure(R.drawable.circle_profile, R.drawable.circle_profile_sel, R.drawable.enrol);
        int pad = (int)this.getResources().getDimension(R.dimen.btn_padding_large);
        btnRegister.getImageView().setPadding(pad, pad,pad, pad);
        etAddress = (AutoCompleteTextView) v.findViewById(R.id.et_address);
        adapter = new SmartPitGoogleAddressesAdapter(this.getActivity(), android.R.layout.simple_list_item_1);
        etAddress.setAdapter(adapter);
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SmartPitGoogleAddress a = adapter.getPoint(position);
                etHouse.setText(a.getStreetNumber());
                lat = a.getLat();
                lng = a.getLon();
                //etFlat.setText(a.getStreetNumber());
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFormValid())
                    return;
                JSONObject data = new JSONObject();
                try {
                    data.put("username", etUsername.getText().toString());
                    data.put("password", etPassword.getText().toString());
                    data.put("email", etEmail.getText().toString());

                    JSONObject insideData = new JSONObject();

                    insideData.put("phone", etPhone.getText().toString());
                    insideData.put("address", etAddress.getText().toString());
                    insideData.put("house", etHouse.getText().toString());
                    insideData.put("flat", etFlat.getText().toString());
                    insideData.put("lat", lat);
                    insideData.put("lng", lng);
                    data.put("data", insideData);

                    Log.d(TAG, data.toString());


                    DAO.registerUser(data, new Response.Listener() {
                        @Override
                        public void onResponse(Object o) {
                            Log.d(TAG, o.toString());
                            Toast.makeText(FragmentRegister.this.getActivity(), "Zarejestrowano pomyslnie", Toast.LENGTH_LONG).show();
                            ((MainActivity) FragmentRegister.this.getActivity()).onBackPressed();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            Toast.makeText(FragmentRegister.this.getActivity(), AppHelper.getErrorMessage(volleyError), Toast.LENGTH_LONG).show();


                            String json = null;


                        }
                    });
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });

        return v;
    }

    @Override
    public String getLabel() {
        return "Rejestracja";
    }
}
