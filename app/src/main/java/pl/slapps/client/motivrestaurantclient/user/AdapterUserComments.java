package pl.slapps.client.motivrestaurantclient.user;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.menu.AdapterCategories;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 26.07.15.
 */
public class AdapterUserComments extends ArrayAdapter {
    private Context context;
    private ArrayList<JSONObject> list;
    private int lastPosition = 0;
    private Drawable error_image;
    private String TAG = AdapterCategories.class.getName();

    public AdapterUserComments(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.row_comment, list);
        error_image = context.getResources().getDrawable(R.drawable.icon_restaurant);
        this.context = context;
        this.list = list;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_content;
        SmartImageView user;
        SmartImageView attachment;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_user_comments, null);
            holder = new ViewHolder();
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
            holder.attachment = (SmartImageView) convertView.findViewById(R.id.iv_media);
            holder.attachment.setMode(SmartImageView.Mode.NORMAL.ordinal());
            holder.attachment.getImageView().setAdjustViewBounds(true);
            holder.user = (SmartImageView) convertView.findViewById(R.id.iv_logo);
            holder.user.setErrorImage(error_image);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        holder.attachment.getImageView().setImageBitmap(null);


        holder.attachment.setVisibility(View.GONE);

        JSONObject o = list.get(position);
        String image = null;
        try {
            JSONObject owner = o.has("owner") ? o.getJSONObject("owner") : new JSONObject();
            Place model = new Place(owner.toString());

            JSONObject data = o.has("data") ? o.getJSONObject("data") : new JSONObject();
            String username = data.has("username") ? data.getString("username") : "";
            String avatar = data.has("avatar") ? data.getString("avatar") : "";

            String content = o.has("content") ? o.getString("content") : "";


            holder.tv_name.setText(model.getGName());
            holder.tv_content.setText(content);
            AppHelper.setImage(context, holder.user, model.getRLogo(), 0, 0, position);

            JSONObject media = o.has("media") ? o.getJSONObject("media") : new JSONObject();
            String path = media.has("path") ? media.getString("path") : "";

            if (!path.trim().equals("")) {
                // holder.attachment.setTag(position);
                holder.attachment.setVisibility(View.VISIBLE);
                holder.attachment.getProgressBar().setVisibility(View.GONE);
                AppHelper.setImage(context, holder.attachment, DAO.API_V1_STATIC + path, 0, 0, position);
                // holder.attachment.setVisibility(View.VISIBLE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //convertView.startAnimation(animation);
        //lastPosition = position;

        return convertView;
    }
}