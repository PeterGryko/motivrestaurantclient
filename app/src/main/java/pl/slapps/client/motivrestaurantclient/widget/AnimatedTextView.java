package pl.slapps.client.motivrestaurantclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

/**
 * Created by piotr on 27.07.15.
 */
public class AnimatedTextView extends TextView {

    private WriteAnimation write_animation_object;

    class WriteAnimation {
        private String input;
        private String output;
        private Animation write_animation;

        public WriteAnimation(long duration) {
            this.write_animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);

                    AnimatedTextView.this.setAnimationProgress(interpolatedTime, WriteAnimation.this.input, WriteAnimation.this.output);
                }
            };
            this.write_animation.setDuration(duration);
        }

        public void startAnimation(String output) {
            this.input = AnimatedTextView.this.getText().toString();
            this.output = output;

            if(input.equals(output))
                return;

            Log.d(TAG,"Start animation "+input+"  "+output);

            AnimatedTextView.this.startAnimation(write_animation);


        }
    }

    private String TAG = AnimatedTextView.class.getName();

    public AnimatedTextView(Context context) {
        super(context);
        write_animation_object = new WriteAnimation(500);
    }

    public AnimatedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        write_animation_object = new WriteAnimation(500);

    }

    public void setTextWithAnimation(String text) {
        write_animation_object.startAnimation(text);
    }


    public void setAnimationProgress(float move, String input, String output) {
        int lenght = input.length();
        if (output.length() > input.length())
            lenght = output.length();

        double part_size = 1.0f / lenght;


        int parts_to_draw = Math.round((float) (move / part_size));

        Log.d(TAG, "lenght " + lenght + "movement " + move + " part size " + part_size + "parts to draw " + parts_to_draw);

        String result = "";
        for (int i = 0; i < (parts_to_draw < output.length() ? parts_to_draw : output.length()); i++) {
            result += output.charAt(i);
        }

        if (input.length() > parts_to_draw)
            result += input.substring(parts_to_draw);

        Log.d(TAG, result);

        if (!getText().equals(result))
            setText(result);
    }
}
