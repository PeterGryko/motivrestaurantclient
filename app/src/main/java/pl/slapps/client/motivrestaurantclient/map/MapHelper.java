package pl.slapps.client.motivrestaurantclient.map;

import android.location.Location;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;

/**
 * Created by piotr on 05.04.15.
 */
public class MapHelper {
    private String TAG = MapHelper.class.getName();
    private MainActivity context;
    private LatLng currentLocation;
    private MapPlacesHelper placesHelper;
    private MapMarkerManager markerManager;
    private MapRouteHelper routeHelper;
    private boolean data_loaded = false;

    public static final String SEPARATOR = "#";
    public static final String PLACE = "PLACE";
    public static final String ROUTE = "ROUTE";

    private ClusterManager<ClusterItem> mClusterManager;


    public MapHelper(MainActivity context) {
        this.context = context;
        //this.placesHelper = new MapPlacesHelper(context);
        //this.restaurantsHelper = new MapRestaurantsHelper(context);
        markerManager = new MapMarkerManager(context);

        placesHelper = new MapPlacesHelper(context, markerManager);
        routeHelper = new MapRouteHelper(context, this);


    }

    public void setRouteClickListener(GoogleMap map) {

        routeHelper.setOnClickListener(map);
    }


    public LatLng getCurrentLocation() {
        return currentLocation;
    }

    public MapMarkerManager getMarkerManager() {
        return markerManager;
    }

    public MapRouteHelper getRouteHelper() {
        return routeHelper;
    }


    public MapPlacesHelper getPlacesHelper() {
        return placesHelper;
    }


    public void relocateLocationButton(View v) {

        View locationButton = ((View) v.findViewById(1).getParent()).findViewById(2);

        // and next place it, for exemple, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
    }

    public void setInfoWindowAdapter(GoogleMap map) {


        map.setInfoWindowAdapter(new AdapterMapWindow(context, this));
    }


    public void setInfoWindowClickListener(GoogleMap map) {
        if (map == null)
            return;

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                String entry = marker.getTitle();
                String[] split = entry.split(SEPARATOR);
                if (split[0].equals(PLACE))
                    placesHelper.onMarkerClick(split[1]);

            }
        });


    }

    public void setLocationListener(final GoogleMap map) {
        if (map == null || data_loaded)
            return;

        map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                MyApplication.currentLocation = currentLocation;
/*
                if (!data_loaded) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));

                    //placesHelper.searchPlacesByRange(map, currentLocation);
                    data_loaded = true;
                }
  */
            }

        });
    }

}
