package pl.slapps.client.motivrestaurantclient.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.menu.AdapterCart;
import pl.slapps.client.motivrestaurantclient.menu.FragmentConfirmBuy;

/**
 * Created by piotr on 31.03.15.
 */
public class FragmentRestaurantDrawer extends SmartPitFragment {


    private SmartImageView ivLogo;
    private JSONObject restaurant;
    private ListView lv;
    private ArrayList<JSONObject> list;
    private TextView tv_label;
    private TextView tv_sum;

    private AdapterCart adapter;
    private SmartPitSelectorView btn_till;
    private int padding;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_restaurant_drawer_layout, parent, false);
        padding = (int) this.getResources().getDimension(R.dimen.btn_padding_large);

        ivLogo = (SmartImageView) v.findViewById(R.id.iv_logo);
        ivLogo.setMode(SmartImageView.Mode.NORMAL.ordinal());
        ivLogo.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);

        lv = (ListView) v.findViewById(R.id.list_view);
        tv_label = (TextView) v.findViewById(R.id.tv_label);
        tv_sum = (TextView) v.findViewById(R.id.tv_sum);
        btn_till = (SmartPitSelectorView) v.findViewById(R.id.btn_till);
        btn_till.configure(R.drawable.circle_bkg, R.drawable.circle_profile_sel, R.drawable.till);
        btn_till.getImageView().setPadding(padding, padding, padding, padding);


        btn_till.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MyApplication.getCurrentUser()==null)
                {
                    Toast.makeText(FragmentRestaurantDrawer.this.getActivity(),"Musisz się zalogować",Toast.LENGTH_LONG).show();
                    return;
                }
                ((MainActivity) FragmentRestaurantDrawer.this.getActivity()).showSlaveMenu();

                Bundle arg = new Bundle();
                arg.putString("price", tv_sum.getText().toString());
                arg.putInt("count", adapter.getCount());
                FragmentConfirmBuy fragmentConfirmBuy = new FragmentConfirmBuy();
                fragmentConfirmBuy.setArguments(arg);

                ((MainActivity) FragmentRestaurantDrawer.this.getActivity()).switchDrawerFragment(fragmentConfirmBuy);


            }
        });


        if (MyApplication.getCart().size() > 0)
            tv_label.setVisibility(View.GONE);


        refreashCart();


        parseInitialArgs();
        return v;
    }

    public void refreashSum(double price) {
        tv_sum.setText(price + "zł");
    }


    private void parseInitialArgs() {

        restaurant = MyApplication.getCurrentRestaurant();

        Place m = (Place) restaurant;
        //MapRestaurantModel m = MapRestaurantModel.valueOfLocal(restaurant);

        SmartPitImageLoader.setImage(this.getActivity(), ivLogo, m.getRLogo(), 0, 0);


    }

    public void refreashCart() {

        if (MyApplication.getCart().size() == 0) {
            tv_label.setVisibility(View.VISIBLE);
            tv_label.setText(this.getString(R.string.menu_cart_empy));
            tv_sum.setText(Integer.toString(0));


        } else {
            tv_label.setVisibility(View.GONE);
            if (lv.getAdapter() == null) {
                adapter = new AdapterCart(this.getActivity(), MyApplication.getCart(), this);
                lv.setAdapter(adapter);
            } else
                adapter.notifyDataSetChanged();
            //this.initMap();

            adapter.calculateSum();
            // tvCart.setText(this.getString(R.string.menu_cart, MyApplication.getCart().size()));
        }
    }

    public void onDrawerSlide(float fraction) {

        //ivLogo.setPadding(0, 0, (int) (fraction * 100),(int) (fraction * 100));
    }

    @Override
    public String getLabel() {
        return null;
    }
}
