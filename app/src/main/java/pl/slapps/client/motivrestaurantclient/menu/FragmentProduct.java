package pl.slapps.client.motivrestaurantclient.menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.model.Product;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 24.03.15.
 */
public class FragmentProduct extends SmartPitFragment implements RestaurantInterface {


    private String TAG = FragmentProduct.class.getName();


    private Product product;

    private ListView lv;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_product_detail, parent, false);


        lv = (ListView) v.findViewById(R.id.list);

        if (this.getArguments() != null) {
            try {
                product = new Product(new JSONObject(this.getArguments().getString("data")));
                initView(product);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        return v;
    }


    private void initView(final Product product) {

        ArrayList<JSONObject> listData = new ArrayList<>();
        try {


            JSONArray productIngredients = product.ingredients;

            JSONObject category = new JSONObject();
            category.put("view_type", AdapterProductDetail.VIEW_TYPE.CATEGORY.name());
            listData.add(category);

            JSONObject name = new JSONObject();
            name.put("view_type", AdapterProductDetail.VIEW_TYPE.NAME.name());
            name.put("name", product.name);
            listData.add(name);


            JSONArray photos = new JSONArray();
            for (int i = 0; i < product.urls.size(); i++) {
                photos.put(product.urls.get(i));
            }

            JSONObject images = new JSONObject();
            images.put("view_type", AdapterProductDetail.VIEW_TYPE.IMAGES.name());
            images.put("photos", photos);
            listData.add(images);
            JSONObject cart = new JSONObject();
            cart.put("view_type", AdapterProductDetail.VIEW_TYPE.CART.name());
            listData.add(cart);

            JSONObject rate = new JSONObject();
            rate.put("view_type", AdapterProductDetail.VIEW_TYPE.RATE.name());
            rate.put("price", product.price);

            listData.add(rate);

            JSONObject desc = new JSONObject();
            desc.put("view_type", AdapterProductDetail.VIEW_TYPE.DESC.name());
            desc.put("desc", product.desc);
            listData.add(desc);

            JSONObject ingredientLabel = new JSONObject();
            ingredientLabel.put("view_type", AdapterProductDetail.VIEW_TYPE.INGREDIENTS_LABEL.name());
            listData.add(ingredientLabel);

            for (int i = 0; i < productIngredients.length(); i++) {
                JSONObject ingredient = new JSONObject();
                ingredient.put("view_type", AdapterProductDetail.VIEW_TYPE.INGREDIENT.name());
                ingredient.put("name", productIngredients.getString(i));
                listData.add(ingredient);
            }
            JSONObject commentsLabel = new JSONObject();
            commentsLabel.put("view_type", AdapterProductDetail.VIEW_TYPE.COMMENTS_LABEL.name());
            listData.add(commentsLabel);


            for (int i = 0; i < 5; i++) {
                JSONObject comment = new JSONObject();
                comment.put("view_type", AdapterProductDetail.VIEW_TYPE.COMMENTS.name());
                listData.add(comment);
            }

            JSONObject commentsMore = new JSONObject();
            commentsMore.put("view_type", AdapterProductDetail.VIEW_TYPE.COMMENTS_MORE.name());
            listData.add(commentsMore);



            lv.setAdapter(new AdapterProductDetail((MainActivity) this.getActivity(), listData, product));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
