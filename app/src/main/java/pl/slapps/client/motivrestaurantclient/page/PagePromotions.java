package pl.slapps.client.motivrestaurantclient.page;

import android.view.LayoutInflater;
import android.view.View;

import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 27.05.15.
 */
public class PagePromotions {
    public View createView(LayoutInflater inflater)
    {
        return inflater.inflate(R.layout.page_promotions,null);
    }
}
