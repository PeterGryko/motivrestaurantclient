package pl.slapps.client.motivrestaurantclient.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 26.03.15.
 */
public class FragmentHistoryCities extends SmartPitFragment {
    private ListView lv;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_history_cities, parent, false);
        lv = (ListView) v.findViewById(R.id.list);

        final ArrayList<String> mocks = new ArrayList<String>();
        mocks.add("warszawa");
        mocks.add("białystok");
        lv.setAdapter(new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, mocks));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FragmentHistoryRestaurants fr = new FragmentHistoryRestaurants();
                Bundle arg = new Bundle();
                JSONObject object = new JSONObject();
                try {
                    object.put("title", mocks.get(position));

                    object.put("logo", "http://www.designknock.com/wp-content/uploads/2013/03/Restaurant-Logo-1.jpg");
                    arg.putString("city",object.toString());
                    fr.setArguments(arg);


                    FragmentHistoryCities.this.getFragmentsListener().switchFragment(fr, true);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
