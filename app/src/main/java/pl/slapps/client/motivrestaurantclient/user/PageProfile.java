package pl.slapps.client.motivrestaurantclient.user;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 19.05.15.
 */
public class PageProfile {


    private String TAG = PageProfile.class.getName();

    private MainActivity context;
    //private Button btnEdit;
    //private Button btnHistory;
    private SmartImageView ivLogo;

    private TextView etUsername;

    private Button btnLogout;

    private LinearLayout layout_addresses;

    private SmartPitSelectorView btn_add_address;

    private LinearLayout editBase;
    private LinearLayout editForm;

    public View createView(LayoutInflater inflater, final MainActivity context) {
        this.context = context;

        View v = inflater.inflate(R.layout.page_profile, null);

        etUsername = (TextView) v.findViewById(R.id.et_username);

        layout_addresses = (LinearLayout) v.findViewById(R.id.addresses);

        editBase = (LinearLayout) v.findViewById(R.id.layout_edit);
        editForm = (LinearLayout) inflater.inflate(R.layout.layout_add_address, null);

        LinearLayout btnConfirm = (LinearLayout) editForm.findViewById(R.id.btn_confirm_address);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DAO.updateProfil(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {
                        Log.d(TAG, o.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());
                    }
                }, MyApplication.getCurrentUser());
            }
        });
        editBase.addView(editForm);
        editBase.setVisibility(View.GONE);


        btnLogout = (Button) v.findViewById(R.id.btn_logout);

        final Animation show = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {

                editBase.getLayoutParams().height = (int) (editForm.getHeight() * (interpolatedTime));
            }
        };
        show.setDuration(500);
        final Animation hide = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {

                editBase.getLayoutParams().height = (int) (editForm.getHeight() - editForm.getHeight() * (interpolatedTime));
            }
        };
        hide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                editBase.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        hide.setDuration(500);
        btn_add_address = (SmartPitSelectorView) v.findViewById(R.id.btn_add_address);
        btn_add_address.configure(R.drawable.circle_profile, R.drawable.circle_sel, R.drawable.plus_white);
        btn_add_address.getImageView().setPadding(10, 10, 10, 10);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.setCurrentProfile(null);
                context.switchMasterDrawerTitleFragment(new FragmentLogin());
            }
        });
        btn_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editBase.getVisibility() == View.VISIBLE) {
                    //    editBase.startAnimation(hide);


                    //}
                    SmartPitAppHelper.hideViewWithAnimation(editBase, 300);

                } else {
                    //ViewCompat.set
                    //    editBase.setVisibility(View.VISIBLE);
                    //ViewCompat.setAlpha(editBase,0.0f);
                    //view.setAlpha(0.0f);

// Start the animation
                    //   editBase.startAnimation(show);
                    //ViewCompat.animate(editBase).y(0).setDuration(500).start();

                }
                SmartPitAppHelper.showViewWithAnimation(editBase, 300);
            }
        });


        //btnHistory = (Button) v.findViewById(R.id.btn_history);
        //btnHistory.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        context.switchFragment(new FragmentHistoryCities(), true);
        //    }
        //});


        ivLogo = (SmartImageView) v.findViewById(R.id.logo);
        ivLogo.setErrorImage(context.getResources().getDrawable(R.drawable.mock_profile));
        ivLogo.getProgressBar().setVisibility(View.GONE);
        ivLogo.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.mock_profile));

        //SmartPitAppHelper.getInstance(this.getActivity()).setImage(ivLogo, "http://www.american.edu/uploads/profiles/large/chris_palmer_profile_11.jpg", 0, 0);

        ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.pickImageFromGallery();
            }
        });

        parseProfileData(MyApplication.getCurrentUser().toString());
       /*
        DAO.getProfile(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());



                parseProfileData(o.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, AppHelper.getErrorMessage(error));
            }
        });
*/
        return v;

    }

    private View generateAddressView(JSONObject data) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_address, null);
        TextView etFlat = (TextView) v.findViewById(R.id.et_flat);
        TextView etHouse = (TextView) v.findViewById(R.id.et_house);
        TextView etPhone = (TextView) v.findViewById(R.id.et_phone);
        TextView etEmail = (TextView) v.findViewById(R.id.et_email);
        TextView etAddress = (TextView) v.findViewById(R.id.et_address);


        try {
            String email = data.has("email") ? data.getString("email") : "";


            String address = data.has("address") ? data.getString("address") : "";
            String house = data.has("house") ? data.getString("house") : "";
            String flat = data.has("flat") ? data.getString("flat") : "";

            etEmail.setText(email);
            etAddress.setText(address);
            etHouse.setText(house);
            etFlat.setText(flat);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return v;
    }


    private void parseProfileData(String s) {


        try {
            JSONObject p = new JSONObject(s);
            JSONObject data = p.has("data") ? p.getJSONObject("data") : new JSONObject();

            String username = data.has("username") ? data.getString("username") : "";


            JSONObject avatar = (p.has("avatar") && !p.isNull("avatar")) ? p.getJSONObject("avatar") : new JSONObject();
            String path = avatar.has("path") ? avatar.getString("path") : "";

            MyApplication.setCurrentProfile(p);

            SmartPitImageLoader.setImage(context,
                    ivLogo, DAO.API + path, 0, 0);


            etUsername.setText(username);

            for (int i = 0; i < 3; i++) {
                layout_addresses.addView(generateAddressView(data));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void onImageSelected(Bitmap bitmap) {
        if (bitmap != null && ivLogo != null)
            ivLogo.getImageView().setImageBitmap(bitmap);

    }

}
