package pl.slapps.client.motivrestaurantclient.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitRowedLayout;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.PlacesFecheter;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.menu.FragmentMenu;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;
import pl.slapps.client.motivrestaurantclient.widget.CustomScrollView;
import pl.slapps.client.motivrestaurantclient.widget.FadeViewFlipper;
import pl.slapps.client.motivrestaurantclient.widget.RatingWidget;

/**
 * Created by piotr on 05.04.15.
 */
public class FragmentPlaceDetails extends SmartPitFragment {

    private CustomScrollView parent_sv;
    private CustomScrollView child_sv;
    private View v;

    private LinearLayout parentLayout;
    private RelativeLayout relativeLayout;
    private LinearLayout layout_comment;
    private LinearLayout topLayout;
    private LinearLayout childBase;
    private SmartPitRowedLayout tagsLayout;
    private TextView tvName;
    private TextView tvAddress;
    private TextView tvPhone;
    private TextView tvHours;
    private TextView tvCheckings;
    private TextView tvTalkingAbout;
    private TextView tvLikes;
    private TextView tvPriceRange;
    private TextView tvDescripiton;
    private LinearLayout btnNav;
    private LinearLayout btnSv;
    private LinearLayout btnRoute;
    private LinearLayout btnPhone;
    private SmartPitSelectorView btn_comment;
    private ImageView btnHome;
    private ImageView btnGoogle;
    private ImageView btnFacebook;
    private ImageView btnMenu;
    private TextView tvVisits;
    private LinearLayout parkingLayout;
    private LinearLayout servicesLayout;
    private LinearLayout specialitiesLayout;


    private FadeViewFlipper flipper;
    private TextView tv_more_comments;

    private float scrollable_part;
    private float activities_height;
    private float actionbar_height;

    private int active_color;
    private float current_opacity = 0;

    private Drawable profile_error;
    private String TAG = FragmentPlaceDetails.class.getName();

    public float getCurrentOpacity() {
        return current_opacity;
    }

    private void setDispachListener(View v)
    {
        v.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                // Disallow the touch request for parent scroll on touch of  child view
                parent_sv.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        v = inflater.inflate(R.layout.fragment_place_details, parent, false);
        parentLayout = (LinearLayout) v.findViewById(R.id.parent_layout);

        ((MainActivity) this.getActivity()).getAbImpl().showRestaurantBase();

        //SmartPitAppHelper.showViewWithAnimation(((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().getSearchbarBase(),300);
        //SmartPitAppHelper.hideViewWithAnimation(((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().getRestaurantBase(), 300);

        profile_error = this.getResources().getDrawable(R.drawable.mock_profile);

        btnHome = (ImageView) v.findViewById(R.id.iv_www);
        btnGoogle = (ImageView) v.findViewById(R.id.iv_www_google);
        btnFacebook = (ImageView) v.findViewById(R.id.iv_www_facebook);
        btnMenu = (ImageView) v.findViewById(R.id.menu);
        tvVisits = (TextView) v.findViewById(R.id.tv_visits);

        childBase = (LinearLayout) v.findViewById(R.id.layout_top);

        btnMenu.setEnabled(false);
        //btnMenu.setVisibility(View.GONE);


        parkingLayout = (LinearLayout) v.findViewById(R.id.layout_parking);
        servicesLayout = (LinearLayout) v.findViewById(R.id.layout_services);
        specialitiesLayout = (LinearLayout) v.findViewById(R.id.layout_specialities);

        scrollable_part = FragmentPlaceDetails.this.getActivity().getResources().getDimension(R.dimen.detail_top);
        actionbar_height = FragmentPlaceDetails.this.getActivity().getResources().getDimension(R.dimen.abc_action_bar_default_height_material);
        activities_height = FragmentPlaceDetails.this.getActivity().getResources().getDimension(R.dimen.detail_activities_height);
        active_color = FragmentPlaceDetails.this.getActivity().getResources().getColor(R.color.profile_indicator_on);
        childBase.setMinimumHeight((int) (SmartPitAppHelper.getScreenHeight(this.getActivity()) + scrollable_part - actionbar_height - activities_height));

        child_sv = (CustomScrollView) v.findViewById(R.id.sv_child);

        relativeLayout = (RelativeLayout) v.findViewById(R.id.detail_top);
        layout_comment = (LinearLayout) v.findViewById(R.id.layout_comments);
        tv_more_comments = (TextView) v.findViewById(R.id.btn_more_comment);

        topLayout = (LinearLayout) v.findViewById(R.id.top_layout);
        tagsLayout = (SmartPitRowedLayout) v.findViewById(R.id.layout_tags);
        tvName = (TextView) v.findViewById(R.id.tv_name);
        tvAddress = (TextView) v.findViewById(R.id.tv_address);
        tvPhone = (TextView) v.findViewById(R.id.tv_phone);
        tvHours = (TextView) v.findViewById(R.id.tv_hours);

        tvCheckings = (TextView) v.findViewById(R.id.tv_checkings);
        tvTalkingAbout = (TextView) v.findViewById(R.id.tv_talking_about);
        tvLikes = (TextView) v.findViewById(R.id.tv_likes);
        tvPriceRange = (TextView) v.findViewById(R.id.tv_price_range);

        tvDescripiton = (TextView) v.findViewById(R.id.tv_description);


        btnNav = (LinearLayout) v.findViewById(R.id.btn_nav);
        btnSv = (LinearLayout) v.findViewById(R.id.btn_sv);
        btnRoute = (LinearLayout) v.findViewById(R.id.btn_route);
        btnPhone = (LinearLayout) v.findViewById(R.id.btn_phone);

        setDispachListener(btnNav);
        setDispachListener(btnSv);
        setDispachListener(btnRoute);
        setDispachListener(btnPhone);
        setDispachListener(topLayout);
        setDispachListener(tagsLayout);
        setDispachListener(child_sv);




        btn_comment = (SmartPitSelectorView) v.findViewById(R.id.btn_comment);


        btn_comment.getImageView().setPadding(10, 10, 10, 10);

        parent_sv = (CustomScrollView) v.findViewById(R.id.parent_sv);
        parent_sv.setInterceptFlag(true);
        child_sv.setInterceptFlag(true);

        final RelativeLayout permanent_layout = (RelativeLayout) v.findViewById(R.id.permanent_layout);

        final int[] in = new int[]{0, 0, 0, 0};
        final int[] out = new int[]{255, Color.red(active_color), Color.green(active_color), Color.blue(active_color)};

        child_sv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                //parent_sv.getHeight()
                AppHelper.getScreenHeight(this.getActivity()) - (int) scrollable_part + ((int) scrollable_part - (int) activities_height - (int) actionbar_height)
                //  - (int) activities_height - (int) actionbar_height
        ));

        parent_sv.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                parent_sv
                        .requestDisallowInterceptTouchEvent(false);


                return false;
            }
        });



        child_sv.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int i, boolean b, boolean b2) {


                if (i < 0)
                    return;

                float h = scrollable_part - i / 2;


                current_opacity = (i / 2) / (scrollable_part - actionbar_height - activities_height);


                // setTopAnimationProgress(-i/2);
                Log.d(TAG, "movement " + current_opacity + " " + i / 2 + " " + (scrollable_part - actionbar_height - activities_height));
                //int[] ab_result = AppHelper.generateRainbowTour(out, actionbar_bkg, movement);


                if (current_opacity < 1.00f && current_opacity > 0.00f) {
                    int[] result = AppHelper.generateRainbowTour(in, out, current_opacity);

                    permanent_layout.setBackgroundColor(Color.argb(result[0], result[1], result[2], result[3]));
                    // ((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().getSearchbarBase().setAlpha(1 - current_opacity);
                    // ((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().setBackgroundAlphaFromDefaultRange(1 - current_opacity);
                }
                /*
                else if (((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().getSearchbarBase().getAlpha() > 0 && current_opacity > 0.0f) {
                    int[] result = AppHelper.generateRainbowTour(in, out, 1);

                    permanent_layout.setBackgroundColor(Color.argb(result[0], result[1], result[2], result[3]));
                    ((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().getSearchbarBase().setAlpha(0);
                    ((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().setBackgroundAlphaFromDefaultRange(0);
                }
                */
                //relativeLayout.getLayoutParams().height=(int)h;
                //relativeLayout.requestLayout();
                parent_sv.smoothScrollTo(0, i / 2);
                //relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, (int) h));

                //  relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, (int) h));

            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {

            }
        });

        refreashContextInfo();
        parseInitialArgs();
        return v;
    }

    private void refreashContextInfo() {
        if (((MainActivity) this.getActivity()).getCurrentSlaveDrawerFragment().getClass() != FragmentContextInfo.class) {
            FragmentContextInfo fi = new FragmentContextInfo();
            Bundle arg = new Bundle();
            arg.putString("query", ((MainActivity) this.getActivity()).getCurrentGeocoder().locatity);
            fi.setArguments(arg);
            ((MainActivity) this.getActivity()).switchSlaveDrawerTitleFragment(fi);
        }
    }


    private void parseInitialArgs() {
        if (this.getArguments() != null) {

            String data = this.getArguments().getString("data");
            if (data != null) {
                try {
                    parseJsonResult(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                String id = this.getArguments().getString("place_id");
                refreashData(id);
            }
        }
    }

    public void onDestroyView() {
        if (flipper != null)
            flipper.stop();

        //((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().startAlphaAnimation(ViewCompat.getAlpha(((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().getSearchbarBase()), 1);
        super.onDestroyView();
    }

    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "on detach");
    }

    public void refreashData(String id) {
        Log.d(TAG, "refreash data " + id);
        parentLayout.setVisibility(View.INVISIBLE);

        PlacesFecheter.fechPlaces(new PlacesFecheter.FechPlacesListener() {
            @Override
            public void onSuccess(ArrayList<Place> places, Place place, JSONObject geocoder) {
                parseJsonResult(place);

            }

            @Override
            public void onError(VolleyError error) {
                Log.d(TAG, error.toString());

            }
        }, null, null, id);


        /*
        DAO.getPlaces(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                try {
                    Log.d(TAG, o.toString());
                    JSONObject json = new JSONObject(o.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());

            }
        }, null, null, id);
*/
    }


    public void parseJsonResult(JSONObject data) {


        initTopView((MainActivity) this.getActivity(), v, data);


    }


    @Override
    public String getLabel() {
        return null;
    }

    private View generateTagView(final String tag) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.tag_layout, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_label);

        tv.setText(tag);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentPlaceDetails.this.getActivity()).showMenu();
                ((MainActivity) FragmentPlaceDetails.this.getActivity()).fechPlacesByTag(tag);

            }
        });
        setDispachListener(v);

        return v;
    }

    private void loadRestaurantComments(final Place model) {


        DAO.getPlaceComments(model.getRId(), "1", "2", new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                Log.d(TAG, "comments " + o.toString());
                parseCommentData(o.toString());
                parseGoogleReviews(model);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
                Log.d(TAG, AppHelper.getErrorMessage(volleyError));
            }
        });
    }

    private void parseCommentData(String d) {
        try {
            JSONObject object = new JSONObject(d.toString());
            object = object.has("api") ? object.getJSONObject("api") : object;
            JSONArray data = object.has("results") ? object.getJSONArray("results") : new JSONArray();
            for (int i = 0; i < data.length(); i++) {
                layout_comment.addView(getCommentView(this.getActivity(), data.getJSONObject(i)));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public View getCommentView(Context context, JSONObject o) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.row_comment, null);

        final TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        final TextView tv_content = (TextView) convertView.findViewById(R.id.tv_content);
        final SmartImageView user = (SmartImageView) convertView.findViewById(R.id.iv_logo);
        final SmartImageView attachment = (SmartImageView) convertView.findViewById(R.id.iv_media);
        final RatingWidget ratingWidget = (RatingWidget) convertView.findViewById(R.id.rating);
        attachment.setVisibility(View.GONE);
        user.setErrorImage(profile_error);
        attachment.setMode(SmartImageView.Mode.NORMAL.ordinal());
        attachment.getImageView().setAdjustViewBounds(true);

        String image = null;
        try {
            JSONObject data = o.has("data") ? o.getJSONObject("data") : new JSONObject();
            String username = data.has("username") ? data.getString("username") : "";
            String avatar = data.has("avatar") ? data.getString("avatar") : "";
            int score = data.has("rate") ? data.getInt("rate") : 0;
            ratingWidget.setScore(score);

            String content = o.has("content") ? o.getString("content") : "";

            tv_name.setText(username);
            tv_content.setText(content);
            SmartPitImageLoader.setImage(context, user, DAO.API + avatar, 0, 0);

            JSONObject media = o.has("media") ? o.getJSONObject("media") : new JSONObject();
            String path = media.has("path") ? media.getString("path") : "";
            if (!path.trim().equals("")) {
                attachment.setVisibility(View.VISIBLE);
                SmartPitImageLoader.setImage(context, attachment, DAO.API_V1_STATIC + path, 0, 0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return convertView;
    }

    private void parseGoogleReviews(Place model) {
        try {


            for (int i = 0; i < (model.getGReviews().length() > 2 ? 2 : model.getGReviews().length()); i++) {
                layout_comment.addView(getGoogleReviewView(this.getActivity(), model.getGReviews().getJSONObject(i)));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public View getGoogleReviewView(final Context context, JSONObject o) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.row_comment, null);

        final TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        final TextView tv_content = (TextView) convertView.findViewById(R.id.tv_content);
        final SmartImageView user = (SmartImageView) convertView.findViewById(R.id.iv_logo);
        final RatingWidget rating = (RatingWidget) convertView.findViewById(R.id.rating);
        final SmartImageView attachment = (SmartImageView) convertView.findViewById(R.id.iv_media);

        attachment.setVisibility(View.GONE);
        user.setErrorImage(profile_error);
        //user.getImageView().setAdjustViewBounds(true);
        convertView.findViewById(R.id.iv_media).setVisibility(View.GONE);


        String image = null;
        try {
            String username = o.has("author_name") ? o.getString("author_name") : "";
            final String avatar = o.has("author_url") ? o.getString("author_url") : "";
            int score = o.has("rating") ? o.getInt("rating") : 0;
            rating.setScore(score);
            rating.setEnabled(false);
            String content = o.has("text") ? o.getString("text") : "";

            tv_name.setText(username);
            tv_content.setText(content);
            //SmartPitAppHelper.getInstance(context).setImage(user, avatar, 0, 0);
            user.getProgressBar().setVisibility(View.GONE);
            user.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.google_white));
            user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse(avatar);
                    i.setData(data);
                    context.startActivity(i);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return convertView;
    }


    private void initTopView(final MainActivity context, View v, final JSONObject object) {
        tagsLayout.removeAllViews();
        if (flipper != null)
            flipper = null;
        flipper = new FadeViewFlipper(context);

        /// context.getAbImpl().hideBkg();

        try {
            Log.d(TAG, "init top view " + object.toString());
            final Place model = new Place(object.toString());
            ((MainActivity) this.getActivity()).getAbImpl().setRestaurantLabel(model.getGName());


            tv_more_comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentPlaceComments fragmentPlaceComments = new FragmentPlaceComments();
                    Bundle arg = new Bundle();
                    arg.putString("data", model.toString());
                    fragmentPlaceComments.setArguments(arg);
                    ((MainActivity) FragmentPlaceDetails.this.getActivity()).switchDrawerFragment(fragmentPlaceComments);
                }
            });


            btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (MyApplication.getCurrentUser() == null) {
                        Toast.makeText(FragmentPlaceDetails.this.getActivity(), "Musisz się zalogować", Toast.LENGTH_LONG).show();


                        return;
                    }

                    FragmentNewComment fc = new FragmentNewComment();
                    Bundle arg = new Bundle();
                    arg.putString("data", object.toString());
                    fc.setArguments(arg);
                    FragmentPlaceDetails.this.getFragmentsListener().switchFragment(fc, true);
                }
            });


            loadRestaurantComments(model);

            tvVisits.setText(Integer.toString(model.getRVisits()));

            MyApplication.setCurrentRestaurant(model);
            flipper.getUrlsList().removeAll(flipper.getUrlsList());
            if (model.getPhotos().size() == 0)
                topLayout.setVisibility(View.GONE);
            else {
                for (int i = 0; i < model.getPhotos().size(); i++) {
                    flipper.getUrlsList().add(model.getPhotos().get(i));
                }
            }


            if (model.isRestaurant() != null) {
                btnMenu.setEnabled(true);
                btnMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        FragmentMenu fm = new FragmentMenu();
                        Bundle arg = new Bundle();
                        arg.putString("data", model.toString());
                        fm.setArguments(arg);

                        ((MainActivity) FragmentPlaceDetails.this.getActivity()).switchDrawerFragment(fm);
                        ((MainActivity) FragmentPlaceDetails.this.getActivity()).switchSlaveDrawerTitleFragment(new FragmentRestaurantDrawer());
                    }
                });
            }


            topLayout.removeAllViews();
            topLayout.addView(flipper.getView());
            flipper.start();
            Log.d(TAG, "adapter model g name " + model.getGName());
            Log.d(TAG, "adapter model g phone " + model.getGPhone());

            tvName.setText(model.getGName());

            //((MainActivity) FragmentPlaceDetails.this.getActivity()).getAbImpl().setSearchtext(model.getGName());

            tvPhone.setText(model.getGPhone());
            tvAddress.setText(model.getGAddress());
            //tvRate.setText(Double.toString(model.getGRate()));

            tvCheckings.setText(Integer.toString(model.getFCheckings()) + " osób odwiedziło to miejsce.");
            tvTalkingAbout.setText(Integer.toString(model.getFTalkingsAbout()) + " osób o tym rozmawia.");
            tvLikes.setText(Integer.toString(model.getFLikes()) + " osób lubi to miejsce.");
            tvPriceRange.setText(model.getFPriceRange());

            if (!model.getFDescription().trim().equals(""))
                tvDescripiton.setText(model.getFDescription());
            else
                v.findViewById(R.id.description_base).setVisibility(View.GONE);

            for (int i = 0; i < model.getTags().size(); i++) {
                tagsLayout.addView(generateTagView(model.getTags().get(i)));
            }

            if (model.getGWeekdayText().size() > 0) {
                tvHours.setText("");
                for (int i = 0; i < model.getGWeekdayText().size(); i++) {
                    tvHours.append(model.getGWeekdayText().get(i) + "\n");
                }
            } else
                v.findViewById(R.id.hours_base).setVisibility(View.GONE);


            btnNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String uri = buildNavRequest(model);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    context.startActivity(intent);
                }
            });

            btnSv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String uri = buildStreetViewRequest(model);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    context.startActivity(intent);
                }
            });

            btnRoute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) FragmentPlaceDetails.this.getActivity()).drawRoute(model);
                }
            });

            if (model.getGWeb().trim().equals("")) {
                btnHome.setEnabled(false);
            } else {
                btnHome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getGWeb()));
                        context.startActivity(intent);
                    }
                });
            }
            if (model.getGGoogleWeb().equals(""))
                btnGoogle.setEnabled(false);
            btnGoogle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getGGoogleWeb()));
                    context.startActivity(intent);
                }
            });

            if (model.getFPage().equals(""))
                btnFacebook.setEnabled(false);
            btnFacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getFPage()));
                    context.startActivity(intent);
                }
            });
            topLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    JSONArray array = new JSONArray();
                    for (int i = 0; i < model.getPhotos().size(); i++) {
                        array.put(model.getPhotos().get(i));
                    }
                    Bundle arg = new Bundle();
                    arg.putString("data", array.toString());
                    FragmentGallery fg = new FragmentGallery();
                    fg.setArguments(arg);

                    //context.showMasterMenu();
                    context.switchDrawerFragment(fg);


                }
            });

            HashMap<String, Integer> parkingInfo = model.getFParkingInfo();
            if (parkingInfo.keySet().size() == 0)
                parkingLayout.setVisibility(View.GONE);
            for (String key : parkingInfo.keySet()) {

                parkingLayout.addView(getPlaceFeatureRow(context, key, parkingInfo.get(key)));
            }

            HashMap<String, Integer> servicesInfo = model.getFServicesInfo();
            if (servicesInfo.keySet().size() == 0)
                servicesLayout.setVisibility(View.GONE);
            for (String key : servicesInfo.keySet()) {

                servicesLayout.addView(getPlaceFeatureRow(context, key, servicesInfo.get(key)));
            }

            HashMap<String, Integer> specialitiesInfo = model.getFSpecialitiesInfo();
            if (specialitiesInfo.keySet().size() == 0)
                specialitiesLayout.setVisibility(View.GONE);
            for (String key : specialitiesInfo.keySet()) {

                specialitiesLayout.addView(getPlaceFeatureRow(context, key, specialitiesInfo.get(key)));
            }


            SmartPitAppHelper.showViewWithAnimation(parentLayout, 300);
            context.centerMap(model.getGCoords().latitude, model.getGCoords().longitude);

        } catch (Throwable t) {
            Log.d(TAG, t.toString());
            t.printStackTrace();
        }


    }


    private String buildNavRequest(Place m) {
        String request = "https://www.google.pl/maps/dir/" + MyApplication.currentLocation.latitude + "," + MyApplication.currentLocation.longitude + "/" + m.getGCoords().latitude + "," + m.getGCoords().longitude;

        Log.d(TAG, request);
        return request;
    }

    private String buildStreetViewRequest(Place m) {
        String request = "google.streetview:cbll=" + m.getGCoords().latitude + "," + m.getGCoords().longitude + "&cbp=1,99.56,,1,-5.27&mz=21";

        Log.d(TAG, request);
        return request;
    }


    private View getPlaceFeatureRow(MainActivity context, String text, int flag) {

        Log.d(TAG, "get place feature row " + text);
        View v = LayoutInflater.from(context).inflate(R.layout.row_place_feature, null);

        TextView label = (TextView) v.findViewById(R.id.tv_label);
        label.setText(text);
        ImageView iv = (ImageView) v.findViewById(R.id.iv);

        if (flag > 0)
            iv.setImageDrawable(context.getResources().getDrawable(R.drawable.checkin));
        else
            iv.setImageDrawable(context.getResources().getDrawable(R.drawable.error));


        return v;
    }


}
