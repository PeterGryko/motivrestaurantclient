package pl.slapps.client.motivrestaurantclient;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.plus.Plus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitBaseFragment;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppDialog;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.service.BackgroundService;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentContextInfo;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.user.FragmentLogin;
import pl.slapps.client.motivrestaurantclient.map.FragmentMap;
import pl.slapps.client.motivrestaurantclient.menu.FragmentMenu;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentNewComment;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentPlaceDetails;
import pl.slapps.client.motivrestaurantclient.user.FragmentProfileBase;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentRestaurantDrawer;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentRestaurantsBase;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentSplash;
import pl.slapps.client.motivrestaurantclient.widget.ActionbarImplementation;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;


public class MainActivity extends BaseActivity {


    public enum PICK_TYPE {
        PROFILE_IMAGE, COMMENT_IMAGE;
    }


    public class GeoCoder {
        public String locatity;
        public String formatted_address;
        public LatLng coords;
        public boolean is_current_location;


        public GeoCoder() {
        }

        public GeoCoder(JSONObject data, boolean is_current_location) {
            refreashData(data, is_current_location);
        }

        public void refreashData(JSONObject data, boolean is_current_location) {
            if (data != null) {
                this.is_current_location = is_current_location;
                try {
                    this.formatted_address = data.has("formatted_address") ? data.getString("formatted_address") : "";

                    JSONObject geometry = data.has("geometry") ? data.getJSONObject("geometry") : new JSONObject();
                    this.locatity = geometry.has("locality") ? geometry.getString("locality") : null;

                    JSONObject location = geometry.has("location") ? geometry.getJSONObject("location") : new JSONObject();
                    double lat = location.has("lat") ? location.getDouble("lat") : 0;
                    double lon = location.has("lng") ? location.getDouble("lng") : 0;

                    this.coords = new LatLng(lat, lon);

                    Log.d(TAG, "geocoder data " + lat + "  " + lon);

                    if (MyApplication.currentLocation != null) {
                        Log.d(TAG, "current lcoation lat " + MyApplication.currentLocation.latitude + " lon  " + MyApplication.currentLocation.longitude);
                    }
                    if (this.locatity != null) {
                        updateVisitedCities(this.locatity, this.coords);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, e.toString());
                }
            }


        }


    }

    private int ENABLE_GPS_CODE = 1234;

    private JSONObject visited_cities;

    private PICK_TYPE current_type;

    private GeoCoder CURRENT_GEOCODER;

    private String TAG = MainActivity.class.getName();
    private SmartPitBaseFragment masterDrawerBaseFragment;
    private SmartPitBaseFragment drawerBaseFragment;
    private FragmentRestaurantsBase fragmentSearchRestaurant;


    private ActionbarImplementation abImpl;
    public GoogleApiClient mGoogleApiClient;
    private BackgroundService mService;
    private boolean mBound;
    private Handler handler = new Handler();

    public BackgroundService getService() {
        return mService;
    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) service;
            mService = binder.getService();
            mService.setOnOrdersUpdateListener(new BackgroundService.OnEmitListener() {
                @Override
                public void onStatusChanged(final JSONObject result) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            AppHelper.showStatusChangeDialog(result, MainActivity.this);

                        }
                    });

                }

                @Override
                public void onCoordsChanged(JSONObject result) {

                }

                @Override
                public void onMessage(final JSONObject result) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            AppHelper.showMessageDialog(result, MainActivity.this, mService);

                        }
                    });
                }
            });
            mBound = true;
            Toast.makeText(MainActivity.this, "service connected", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mService = null;
            Toast.makeText(MainActivity.this, "service disconected", Toast.LENGTH_SHORT).show();

        }
    };


    private void updateVisitedCities(String city, LatLng coords) {
        if (visited_cities == null)
            loadSavedCities();

        JSONObject cityToSave = new JSONObject();
        try {
            cityToSave.put("lat", coords.latitude);

            cityToSave.put("lon", coords.longitude);
            visited_cities.put(city, cityToSave);

            Log.d(TAG, "visited cities updated " + city);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, e.toString());
        }
    }

    private void loadSavedCities() {
        String data = SmartPitAppHelper.getPreferences(this).getString("visited_cities", "{}");
        try {
            visited_cities = new JSONObject(data);
            Log.d(TAG, "visited cities loaded " + data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void saveVisitedCities() {
        if (visited_cities != null) {
            SmartPitAppHelper.getPreferences(this).edit().putString("visited_cities", visited_cities.toString()).commit();
            Log.d(TAG, "visited cities saved " + visited_cities.toString());
        }
    }

    public GeoCoder getCurrentGeocoder() {
        return CURRENT_GEOCODER;
    }


    /*
    ################################################################################################
    Main methods , onStart, onStrop, onCreate
    */


    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
        Intent intent = new Intent(this, BackgroundService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        saveVisitedCities();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onStop();
    }

    public void onNewIntent(Intent i) {
        super.onNewIntent(i);
        Log.d(TAG, "on new intent");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.flag = true;

        Log.d(TAG, "on create");

        mGoogleApiClient = new GoogleApiClient
                .Builder(this).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                Log.d(TAG, "connected ");
                //    Person currentUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                ///    Log.d(TAG, "connected user " + currentUser.getUrl() + " " + currentUser.getDisplayName() + " " + currentUser.getPlusOneCount());
            }

            @Override
            public void onConnectionSuspended(int i) {

                Log.d(TAG, "connection suspended");
            }
        }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult result) {
                Log.d(TAG, "connection failed" + result.toString());
                if (result.hasResolution()) {
                    try {
                        // !!!
                        result.startResolutionForResult(MainActivity.this, 123);
                    } catch (IntentSender.SendIntentException e) {
                        mGoogleApiClient.connect();
                    }
                }
            }
        })
                .addApi(Places.GEO_DATA_API).addApi(Plus.API)
                        //.addScope(Plus.SCOPE_PLUS_LOGIN)
                        //.addScope(Plus.SCOPE_PLUS_PROFILE)

                .build();


        abImpl = new ActionbarImplementation(this);
        abImpl.getAb().hide();

        fragmentSearchRestaurant = new FragmentRestaurantsBase();

        this.setDrawerGravity(Gravity.RIGHT);

        this.setFirstFragment(new FragmentSplash());


        try {
            Field f = DrawerLayout.class.getDeclaredField("mMinDrawerMargin");
            f.setAccessible(true);
            f.set(getMasterDrawerLayout(), 0);
            f.set(getDrawerLayout(), 0);

            getMasterDrawerLayout().requestLayout();
            getDrawerLayout().requestLayout();

        } catch (Exception e) {
            e.printStackTrace();
        }


        this.getDrawerLayout().setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (MainActivity.this.getCurrentMasterDrawerFragment() instanceof FragmentRestaurantDrawer) {
                    ((FragmentRestaurantDrawer) MainActivity.this.getCurrentMasterDrawerFragment()).onDrawerSlide(slideOffset);
                }
                animateActionbar(slideOffset, true);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                if (MainActivity.this.getCurrentDrawerFragment() instanceof SmartPitBaseFragment) {
                    SmartPitBaseFragment base = (SmartPitBaseFragment) MainActivity.this.getCurrentDrawerFragment();
                    if (!(base.getCurrentFragment() instanceof FragmentRestaurantsBase)) {
                        //FragmentPlaceDetails fragment = (FragmentPlaceDetails) base.getCurrentFragment();
                        //ViewCompat.setAlpha(MainActivity.this.getAbImpl().getActionbarLayout(),1-fragment.getCurrentOpacity());

                        getAbImpl().showRestaurantBase();
                        //MainActivity.this.getAbImpl().startAlphaAnimation(1, 1 - fragment.getCurrentOpacity());

                    }
                }


            }

            @Override
            public void onDrawerClosed(View drawerView) {

                //SmartPitAppHelper.showViewWithAnimation(MainActivity.this.getAbImpl().getSearchbarBase(),200);

                //SmartPitAppHelper.hideViewWithAnimation(MainActivity.this.getAbImpl().getRestaurantBase(),200);
                //getAbImpl().showSearchbarBase();
                //MainActivity.this.getAbImpl().startAlphaAnimation(ViewCompat.getAlpha(MainActivity.this.getAbImpl().getSearchbarBase()), 1);
                getAbImpl().showSearchbarBase();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                Log.d(TAG, "drawer state changed " + newState);

            }
        });

        this.getMasterDrawerLayout().setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                animateActionbar(slideOffset, false);

            }

            @Override
            public void onDrawerOpened(View drawerView) {

                getAbImpl().showProfileBase();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                getAbImpl().showSearchbarBase();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });


    }

    /*
    ################################################################################################

    Back pressed methods, onBack, onExit

     */

    public void onBackPressed() {

        if (this.getMasterDrawerLayout().isDrawerOpen(this.getMasterDrawerContent())) {
            //    hideMasterShowChild();
            if (!masterDrawerBaseFragment.onBackPressed())
                showMasterMenu();
        } else if (this.getSlaveDrawerLayout().isDrawerOpen(Gravity.RIGHT)) {
            showSlaveMenu();

        } else if (this.getDrawerLayout().isDrawerOpen(Gravity.RIGHT)) {


            if (!drawerBaseFragment.onBackPressed()) {
                showMenu();
            }


        } else {

            if (!abImpl.getSearchtext().equals("")) {
                abImpl.resetSerachBar();
                return;
            }

            FragmentMap fragmentMap = null;
            if (this.getCurrentFragment() instanceof FragmentMap)
                fragmentMap = (FragmentMap) this.getCurrentFragment();
            if (fragmentMap != null && fragmentMap.isAdded()) {
                if (fragmentMap.isRouteActive()) {
                    fragmentMap.resetMapRoute();
                    return;
                }
            }


            super.onBackPressed();
        }
    }

    public void onExit() {

        if (this.getCurrentFragment() instanceof FragmentSplash) {
            SmartPitAppDialog.showExitDialog(AlertDialog.THEME_HOLO_LIGHT, this, "Czy chcesz wyjsc z aplikacji?", "Tak", "Nie");
        } else if (masterDrawerBaseFragment != null && masterDrawerBaseFragment.getCurrentFragment() instanceof FragmentRestaurantDrawer && !(this.getCurrentFragment() instanceof FragmentMenu)) {
            this.switchTitleFragment(new FragmentMenu(), true);
        } else if (this.getCurrentFragment().getClass() != FragmentMap.class) {
            this.switchTitleFragment(new FragmentMap(), true);
        } else
            SmartPitAppDialog.showExitDialog(AlertDialog.THEME_HOLO_LIGHT, this, "Czy chcesz wyjsc z aplikacji?", "Tak", "Nie");
    }

    /*
    ################################################################################################
    Fragments managments methods.


     */

    public void switchFragment(SmartPitFragment fragment, boolean flag) {
        if (this.getCurrentFragment().getClass() == fragment.getClass())
            return;

        if (this.getMasterDrawerLayout().isDrawerOpen(Gravity.LEFT))
            showMenu();

        super.switchFragment(fragment, flag);


    }

    public void switchTitleFragment(SmartPitFragment fragment, boolean flag) {
        if (this.getCurrentFragment().getClass() == fragment.getClass())
            return;

        if (this.getMasterDrawerLayout().isDrawerOpen(Gravity.LEFT))
            showMenu();

        super.switchTitleFragment(fragment, flag);


    }

    public void switchMasterDrawerFragment(SmartPitFragment fragment) {
        if (masterDrawerBaseFragment != null && masterDrawerBaseFragment.getCurrentFragment().getClass() != fragment.getClass())
            masterDrawerBaseFragment.switchFragment(fragment, true);
    }

    public void switchMasterDrawerTitleFragment(SmartPitFragment fragment) {
        try {
            if (masterDrawerBaseFragment != null && masterDrawerBaseFragment.getCurrentFragment().getClass() != fragment.getClass()) {
                masterDrawerBaseFragment.clearBackstack();
                masterDrawerBaseFragment.switchTitleFragment(fragment, true);
            }
        } catch (Throwable t) {
            Log.d(TAG, "cached!");
        }
    }


    public void switchDrawerFragment(SmartPitFragment fragment) {
        if (drawerBaseFragment != null && drawerBaseFragment.getCurrentFragment().getClass() != fragment.getClass()) {
            //if (fragment instanceof RestaurantInterface)
            //    getAbImpl().showRestaurantBase();
            //else
            //    getAbImpl().showSearchbarBase();
            drawerBaseFragment.switchFragment(fragment, true);

        }
    }


    public void switchDrawerTitleFragment(SmartPitFragment fragment) {
        try {
            if (drawerBaseFragment != null && drawerBaseFragment.getCurrentFragment().getClass() != fragment.getClass()) {
                drawerBaseFragment.clearBackstack();
                drawerBaseFragment.switchTitleFragment(fragment, true);
            }
        } catch (Throwable t) {
            Log.d(TAG, "cached!");
        }
    }



    /*
    ###############################################################################################
    Places and map managments methods

    */


    public void initMap(JSONObject geocoder) {

        FragmentMap fragmentMap = new FragmentMap();

        CURRENT_GEOCODER = new GeoCoder(geocoder, true);


        if (geocoder != null) {
            Log.d(TAG, "initial argument given");
            Bundle arg = new Bundle();
            arg.putString("geocoder", geocoder.toString());

            fragmentMap.setArguments(arg);

        } else
            Log.d(TAG, "initial argument null");

        this.switchTitleFragment(fragmentMap, true);


    }

    public void centerMap(double lat, double lng) {
        Log.d(TAG, "center map invoked");

        FragmentMap fragmentMap = null;
        if (this.getCurrentFragment() instanceof FragmentMap)
            fragmentMap = (FragmentMap) this.getCurrentFragment();

        if (fragmentMap != null && fragmentMap.isAdded()) {
            Log.d(TAG, "center map executed");

            fragmentMap.centerOnMarker(lat, lng);
        }
    }

    public void drawRoute(Place end) {
        FragmentMap fragmentMap = null;
        if (this.getCurrentFragment() instanceof FragmentMap)
            fragmentMap = (FragmentMap) this.getCurrentFragment();
        if (fragmentMap != null && fragmentMap.isAdded())
            fragmentMap.toggleRoute(end);


    }


    public void refreashMap(ArrayList<Place> places, JSONObject geocoder) {


        if (places.size() == 0) {
            Toast.makeText(MainActivity.this, "Brak wyników", Toast.LENGTH_LONG).show();
            return;
        }


        FragmentMap fragmentMap = null;
        if (this.getCurrentFragment() instanceof FragmentMap)
            fragmentMap = (FragmentMap) this.getCurrentFragment();
        if (fragmentMap != null && fragmentMap.isAdded())
            fragmentMap.refreashMap(places);

        if (this.getCurrentDrawerFragment() instanceof SmartPitBaseFragment) {
            SmartPitBaseFragment base = (SmartPitBaseFragment) this.getCurrentDrawerFragment();
            if (base.getCurrentFragment() instanceof FragmentRestaurantsBase)
                ((FragmentRestaurantsBase) base.getCurrentFragment()).refreashData(places, CURRENT_GEOCODER, abImpl.getSearchtext());
        }

        if (this.getCurrentSlaveDrawerFragment() instanceof FragmentContextInfo) {
            ((FragmentContextInfo) this.getCurrentSlaveDrawerFragment()).refreashData(CURRENT_GEOCODER.locatity);
        }


    }

    public void resetMap() {
        FragmentMap fragmentMap = null;
        if (this.getCurrentFragment() instanceof FragmentMap)
            fragmentMap = (FragmentMap) this.getCurrentFragment();
        if (fragmentMap != null && fragmentMap.isAdded())
            fragmentMap.resetMap();

        if (this.getCurrentDrawerFragment() instanceof SmartPitBaseFragment) {
            SmartPitBaseFragment base = (SmartPitBaseFragment) this.getCurrentDrawerFragment();
            if (base.getCurrentFragment() instanceof FragmentRestaurantsBase)
                ((FragmentRestaurantsBase) base.getCurrentFragment()).resetData();
        }

        MyApplication.setCurrentPlaces(MyApplication.getPlaces());


    }

    public void setCurrentLocationContext() {
        if (CURRENT_GEOCODER.is_current_location) {
            Toast.makeText(this, "Twoja lokalizacje jest juz ustlona", Toast.LENGTH_LONG).show();
            return;
        }
        CURRENT_GEOCODER.coords = MyApplication.currentLocation;
        fetchPlaces(null, null);
    }

    public void fetchPlaces(final String q, final String placeId) {
        Log.d(TAG, " fech places " + q);

        LatLng pos = MyApplication.currentLocation;
        if (CURRENT_GEOCODER != null)
            pos = CURRENT_GEOCODER.coords;

        abImpl.getProgressBar().setVisibility(View.VISIBLE);
        abImpl.getProgressBar().progressiveStart();
        PlacesFecheter.fechPlaces(new PlacesFecheter.FechPlacesListener() {
            @Override
            public void onSuccess(ArrayList<Place> places, Place place, JSONObject geocoder) {
                abImpl.getProgressBar().progressiveStop();


                if (place == null) {


                    MyApplication.setCurrentPlaces(places);

                    if (q == null && placeId == null)
                        CURRENT_GEOCODER = new GeoCoder(geocoder, true);
                    else
                        CURRENT_GEOCODER = new GeoCoder(geocoder, false);
                    refreashMap(places, geocoder);
                } else {
                    SmartPitBaseFragment currentFragment = (SmartPitBaseFragment) MainActivity.this.getCurrentDrawerFragment();

                    if (currentFragment.getCurrentFragment() instanceof FragmentPlaceDetails) {
                        ((FragmentPlaceDetails) currentFragment.getCurrentFragment()).parseJsonResult(place);
                        Log.d(TAG, "place screen added");

                    } else {
                        Log.d(TAG, "place screen clear");

                        Bundle arg = new Bundle();

                        arg.putString("place_id", place.getGPlaceId());
                        FragmentPlaceDetails fd = new FragmentPlaceDetails();

                        fd.setArguments(arg);
                        MainActivity.this.switchDrawerFragment(fd);

                    }
                    if (!getMasterDrawerLayout().isDrawerOpen(getMasterDrawerContent()))
                        showMenu();
                }

            }

            @Override
            public void onError(VolleyError error) {
                abImpl.getProgressBar().progressiveStop();
                Toast.makeText(MainActivity.this, "Wystąpił błąd", Toast.LENGTH_LONG).show();

            }
        }, q, pos, placeId);


    }

    public void fechPlacesByTag(final String tag) {
        abImpl.getProgressBar().setVisibility(View.VISIBLE);
        abImpl.getProgressBar().progressiveStart();
        PlacesFecheter.fechPlacesByTag(new PlacesFecheter.FechPlacesListener() {
            @Override
            public void onSuccess(ArrayList<Place> places, Place placeModel, JSONObject geocoder) {
                getAbImpl().setSearchtext(tag);
                refreashMap(places, geocoder);
                abImpl.getProgressBar().progressiveStop();
            }

            @Override
            public void onError(VolleyError error) {

            }
        }, tag);
    }


    /*
    ################################################################################################
    Actionbar methods
     */
    private void animateActionbar(float offset, boolean right) {


        abImpl.animateActionbar(offset, right);
    }


    public ActionbarImplementation getAbImpl() {
        return abImpl;
    }

    public void onActionbarItemSelected(View item) {


        if (item.getId() == R.id.menu_search) {

            if (!abImpl.getSearchtext().trim().equals(""))
                fetchPlaces(abImpl.getSearchtext(), null);

        }

        if (item.getId() == R.id.menu_cart) {
            showSlaveMenu();

        }

        if (item.getId() == R.id.menu_profile || item.getId() == R.id.restaurant_base) {
            if (MyApplication.getCurrentUser() != null) {
                if (!(this.getCurrentMasterDrawerFragment() instanceof FragmentProfileBase))
                    switchMasterDrawerTitleFragment(new FragmentProfileBase());
            } else {
                if (!(this.getCurrentMasterDrawerFragment() instanceof FragmentLogin))
                    switchMasterDrawerTitleFragment(new FragmentLogin());
            }
            showMasterMenu();

        }

        if (item.getId() == R.id.menu_settings) {
            showMenu();


        }


    }


    public void setActionBarLabel(String text) {
        Log.d(TAG, "set actionbar label " + text);
        // if (text != null)
        //     abImpl.getAnimatedLabel().setTextWithAnimation(text);
    }

/*
####################################################################################################
Drawers methods
 */

    public void showMenu() {
        Log.d(TAG, "show menu");
        if (getSlaveDrawerLayout().isDrawerOpen(getSlaveDrawerContent())) {
            getSlaveDrawerLayout().closeDrawers();
        } else if (getMasterDrawerLayout().isDrawerOpen(getMasterDrawerContent())) {
            getMasterDrawerLayout().closeDrawers();
        } else {
            super.showMenu();
        }
    }

    public void showMasterMenu() {
        Log.d(TAG, "show master menu");
        if (getSlaveDrawerLayout().isDrawerOpen(getSlaveDrawerContent())) {
            getSlaveDrawerLayout().closeDrawers();
        } else if (getDrawerLayout().isDrawerOpen(getDrawerContent())) {
            if (!drawerBaseFragment.onBackPressed())
                getDrawerLayout().closeDrawers();
        } else
            super.showMasterMenu();
    }

    public void initDrawer() {

        if (masterDrawerBaseFragment == null) {
            masterDrawerBaseFragment = new SmartPitBaseFragment();

            masterDrawerBaseFragment.setInitialFragment(new FragmentLogin());


            drawerBaseFragment = new SmartPitBaseFragment();
            drawerBaseFragment.setInitialFragment(fragmentSearchRestaurant);

            this.setMasterDrawerFragment(masterDrawerBaseFragment);
            this.setDrawerFragment(drawerBaseFragment);
            Bundle arg = new Bundle();
            arg.putString("query", CURRENT_GEOCODER.locatity);
            FragmentContextInfo fci = new FragmentContextInfo();
            fci.setArguments(arg);
            this.setSlaveDrawerFragment(fci);

            //    this.setMasterDrawerFragment(drawerBaseFragment);

            this.getMasterDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            this.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            this.getSlaveDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);


            // this.getDrawerLayout().openDrawer(this.getDrawerContent());

        }
        abImpl.getAb().show();

        /*
        new Handler() {
            public void handleMessage(Message msg) {
                getDrawerLayout().openDrawer(Gravity.RIGHT);
                Log.d(TAG, "delayed action");
            }
        }.sendMessageDelayed(Message.obtain(), 1000);
    */
    }


    public void refreashCart() {

        if (this.getCurrentSlaveDrawerFragment() instanceof FragmentRestaurantDrawer) {
            ((FragmentRestaurantDrawer) this.getCurrentSlaveDrawerFragment()).refreashCart();
        }

    }


    public void pickImageFromGallery(PICK_TYPE type) {
        this.current_type = type;
        this.pickImageFromGallery();
    }

    public void pickImageFromCamera(PICK_TYPE type) {
        this.current_type = type;
        this.pickImageFromCamera();
    }

    private void onImagePicked(File file, Bitmap bitmap) {


        Log.d(TAG, "image picked " + file.getPath());

        if (current_type == PICK_TYPE.PROFILE_IMAGE) {

            if (this.getCurrentMasterDrawerFragment() instanceof FragmentProfileBase) {
                Log.d(TAG, "pass");
                ((FragmentProfileBase) this.getCurrentMasterDrawerFragment()).onImageSelected(bitmap);
            }

            DAO.updateUserAvatar(file, new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    Log.d(TAG, o.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                    Log.d(TAG, AppHelper.getErrorMessage(error));
                }
            });
        } else if (current_type == PICK_TYPE.COMMENT_IMAGE) {
            SmartPitBaseFragment currentFragment = (SmartPitBaseFragment) this.getCurrentDrawerFragment();

            if (currentFragment.getCurrentFragment() instanceof FragmentNewComment)
                ((FragmentNewComment) currentFragment.getCurrentFragment()).onImagePick(bitmap, file);
        }

    }

    public void onCameraImagePicked(Uri uri, File file, Bitmap bitmap) {
        onImagePicked(file, bitmap);
    }

    public void onGalleryImagePicked(Uri uri, File file, Bitmap bitmap) {

        onImagePicked(file, bitmap);
    }

    public void enableGPS() {
        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), ENABLE_GPS_CODE);

    }

    public void onActivityResult(int code, int result, Intent data) {
        super.onActivityResult(code, result, data);
        Log.d(TAG, "on activity result " + code + " " + result);
        if (code == ENABLE_GPS_CODE) {
            if (getCurrentFragment() instanceof FragmentSplash) {
                ((FragmentSplash) this.getCurrentFragment()).onGPSEnabled();
            }
        }
    }


}
