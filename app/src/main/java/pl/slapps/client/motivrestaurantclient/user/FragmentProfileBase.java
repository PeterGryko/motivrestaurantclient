package pl.slapps.client.motivrestaurantclient.user;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitTabHostIndicator;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.page.PageNews;
import pl.slapps.client.motivrestaurantclient.page.PageOrders;
import pl.slapps.client.motivrestaurantclient.widget.AnimatedTextView;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 19.05.15.
 */
public class FragmentProfileBase extends SmartPitPagerFragment {

    class RainbowImage {

        private View view;
        private float current_progress;
        private FinishAnimation finishAnimation;
        int[] color_one = new int[3];
        int[] color_two = new int[3];

        public RainbowImage(View view, int[] color_one, int[] color_two) {
            this.view = view;
            this.color_one = color_one;
            this.color_two = color_two;


        }

        class FinishAnimation {
            private Animation animation;


            float output_progress;

            public FinishAnimation(final float output) {
                final float input = current_progress;

                output_progress = output;

                animation = new Animation() {
                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {

                        //
                        // Log.d(TAG,"apply trans "+interpolatedTime);

                        float progress = (input - (input * interpolatedTime));
                        Log.d(TAG, "set finish animation progress " + input + " output " + output_progress + " result " + progress);

                        setFinishAnimationProgress(progress);
                    }
                };
                animation.setDuration(500);

            }

            public void startAnimation(View view) {
                Log.d(TAG, "start animation");

                view.startAnimation(animation);
            }

            public Animation getAnimation() {
                return animation;
            }


        }


        public void startFinishAnimation() {
            String format = SmartPitAppHelper.getDecimalFormat('.', 2).format(current_progress);

            current_progress = Float.parseFloat(format);


            if (current_progress > 0.00f) {
                finishAnimation = new FinishAnimation(1);
                finishAnimation.startAnimation(view);
            }

        }

        private void setFinishAnimationProgress(float move) {
            this.current_progress = move;
            int[] result = AppHelper.generateRainbowTour(color_one, color_two, (float) move);

            view.setBackgroundColor(Color.rgb(result[0], result[1], result[2]));

        }

        public void setAnimationProgress(float move) {


            if (finishAnimation != null && finishAnimation.getAnimation() != null && finishAnimation.getAnimation().hasStarted() && !finishAnimation.getAnimation().hasEnded()) {
                finishAnimation.getAnimation().cancel();
                view.clearAnimation();

            }

            this.current_progress = move;
            Log.d(TAG, "current progress " + this.current_progress);
            int[] result = AppHelper.generateRainbowTour(color_one, color_two, (float) move);

            view.setBackgroundColor(Color.rgb(result[0], result[1], result[2]));

        }


    }

    public String TAG = FragmentProfileBase.class.getName();
    private SmartPitTabHostIndicator movingIndicator;
    //private HorizontalScrollView scrollViewWidget;
    private PageProfile profile;
    private ArrayList<RainbowImage> tabs = new ArrayList<>();
    private String[] tabs_names = new String[5];
    private int color_off;
    private int color_on;

    private int[] bkg_color_one = new int[3];
    private int[] bkg_color_two = new int[3];

    private AnimatedTextView tv_label;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        tabs = new ArrayList<>();
        color_off = this.getResources().getColor(R.color.profile_indicator_off);
        color_on = this.getResources().getColor(R.color.profile_indicator_on);

        bkg_color_one[0] = Color.red(color_off);
        bkg_color_one[1] = Color.green(color_off);
        bkg_color_one[2] = Color.blue(color_off);

        bkg_color_two[0] = Color.red(color_on);
        bkg_color_two[1] = Color.green(color_on);
        bkg_color_two[2] = Color.blue(color_on);



        /*
        if (MyApplication.getCurrentUser() == null) {
            View v = null;

            if (this.getArguments() != null) {
                v = new PageLogin().onCreateView(inflater, parent, savedInstanceState, (MainActivity) this.getActivity(), true);
            } else {
                v = new PageLogin().onCreateView(inflater, parent, savedInstanceState, (MainActivity) this.getActivity(), false);

            }
            return v;
        }
*/
        View v = inflater.inflate(R.layout.fragment_profile_base, parent, false);

        v.setLayoutParams(new LinearLayout.LayoutParams(SmartPitAppHelper.getScreenWidth(this.getActivity()), ViewGroup.LayoutParams.FILL_PARENT));

        tv_label = (AnimatedTextView)v.findViewById(R.id.tv_label);

        int ab_color =this.getActivity().getResources().getColor(R.color.profile_indicator_on);

        View actionbarLayout = v.findViewById(R.id.action_bar);
        actionbarLayout.setBackgroundColor(Color.argb(100, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));


        profile = new PageProfile();



        ArrayList<View> views = new ArrayList<>();
        views.add(new PageNews().createView(inflater));
        views.add(profile.createView(inflater, (MainActivity) this.getActivity()));
        views.add(new PageUserComments().createView(inflater, this.getActivity()));
        views.add(new PageFavs().createView(inflater));
        views.add(new PageOrders().createView(inflater));


        this.setViewsPager(v, R.id.pager, views);
        this.getPager().setOffscreenPageLimit(4);


        movingIndicator = this.initMovingIndicator(v, R.id.smart_indicator, inflater.inflate(R.layout.profile_indicator_view, null), false);


        movingIndicator.setSwipeListener(new OnSwipeListener() {
            @Override
            public void onSwipeRight(double movement, int position) {


                if (position < tabs.size()) {


                    tabs.get(position + 1).setAnimationProgress((float) movement);
                    tabs.get(position).setAnimationProgress(1 - (float) movement);
                    tv_label.setAnimationProgress((float) movement, tabs_names[position], tabs_names[position + 1]);

                }
                ///topView.setBackgroundColor(Color.argb(result[0], result[1], result[2], result[3]));

            }

            @Override
            public void onSwipeLeft(double movement, int position) {


                if (position < tabs.size()) {


                    tabs.get(position + 1).setAnimationProgress((float) movement);
                    tabs.get(position).setAnimationProgress(1 - (float) movement);

                    tv_label.setAnimationProgress((float) movement, tabs_names[position], tabs_names[position + 1]);
                }
                // int[] result = AppHelper.generateAlphaRainbowTour(colorsList.get(position), colorsList.get(position + 1), (float) movement);
                //topView.setBackgroundColor(Color.argb(result[0], result[1], result[2], result[3]));

            }
        });

        return v;
    }

    public void onAdapterSetted() {
        setActionbarLabel();
        Log.d(TAG, "adapter setted ");
    }


    private View getTab(String label) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.pager_tab, null);

        TextView tv = (TextView) v.findViewById(R.id.tv_label);
        tv.setText(label);
        return v;
    }

    @Override
    public String getLabel() {
        if (this.getPager() == null)
            return null;
        else
            return tabs_names[this.getPager().getCurrentItem()];
    }

    public void onPageScrollStateChanged(int state) {
        super.onPageScrollStateChanged(state);


        if (state == 0) {
            for (int i = 0; i < tabs.size(); i++) {
                if (i != getPager().getCurrentItem()) {
                    tabs.get(i).startFinishAnimation();
                }
            }

        }

    }

    @Override
    public View createTabIndicator(Context context, int index) {


        View v = LayoutInflater.from(context).inflate(R.layout.tab_profile, null);
        ImageView imageView = (ImageView) v.findViewById(R.id.iv_image);
        tabs.add(new RainbowImage(v.findViewById(R.id.background), bkg_color_one, bkg_color_two));

        switch (index) {

            case 0:
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.info_cut));
                tabs_names[0] = "Aktualności";
                break;
            case 1:
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.profile_cut));
                tabs_names[1] = "Profil";
                break;
            case 2:
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.speach_cut));
                tabs_names[2] = "Komentarze";
                break;
            case 3:
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_cut));
                tabs_names[3] = "Ulubione";
                break;
            case 4:
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.order_cut));
                tabs_names[4] = "Zamówienia";
                break;

            default:
                v = new TextView(this.getActivity());


        }
        return v;


    }

    public void onImageSelected(Bitmap bitmap) {
        profile.onImageSelected(bitmap);
        Log.d(TAG, "profile base on image selected");

    }
}
