package pl.slapps.client.motivrestaurantclient.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 28.03.15.
 */
public class RatingWidget extends LinearLayout {

    private ArrayList<ImageView> stars;
    private Drawable selected;
    private Drawable empty;
    private int score;

    public RatingWidget(Context context) {
        super(context);
        initView(context);
    }

    public RatingWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public void setScore(final int index) {
        score = index;
        for (int j = 0; j < stars.size(); j++) {
            if (j <= index) {
                stars.get(j).setImageDrawable(selected);
            } else
                stars.get(j).setImageDrawable(empty);

        }
    }
    public int getScore()
    {
        return score;
    }

    private void initView(Context context) {
        View v = LayoutInflater.from(context).inflate(R.layout.rating_widget, null);
        stars = new ArrayList<ImageView>();
        stars.add((ImageView) v.findViewById(R.id.star_one));
        stars.add((ImageView) v.findViewById(R.id.star_two));
        stars.add((ImageView) v.findViewById(R.id.star_three));
        stars.add((ImageView) v.findViewById(R.id.star_four));
        stars.add((ImageView) v.findViewById(R.id.star_five));

        selected = context.getResources().getDrawable(R.drawable.star_dark);
        empty = context.getResources().getDrawable(R.drawable.star_empty);


        this.addView(v);


        for (int i = 0; i < stars.size(); i++) {
            final int index = i;
            ImageView element = stars.get(index);

            element.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setScore(index);
                }
            });

        }
    }
}
