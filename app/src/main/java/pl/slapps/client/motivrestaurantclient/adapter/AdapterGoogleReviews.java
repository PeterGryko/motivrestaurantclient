package pl.slapps.client.motivrestaurantclient.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.menu.AdapterCategories;

/**
 * Created by piotr on 19.07.15.
 */
public class AdapterGoogleReviews extends ArrayAdapter {
    private Context context;
    private ArrayList<JSONObject> list;
    private int lastPosition = 0;
    private String TAG = AdapterCategories.class.getName();

    public AdapterGoogleReviews(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.row_comment, list);

        this.context = context;
        this.list = list;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_comment, null);

        final TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        final TextView tv_content = (TextView) convertView.findViewById(R.id.tv_content);
        final SmartImageView user = (SmartImageView) convertView.findViewById(R.id.iv_logo);
        convertView.findViewById(R.id.iv_media).setVisibility(View.GONE);

        String image = null;
        try {
            JSONObject o = list.get(position);
            String username = o.has("author_name") ? o.getString("author_name") : "";
            final String avatar = o.has("author_url") ? o.getString("author_url") : "";

            String content = o.has("text") ? o.getString("text") : "";

            tv_name.setText(username);
            tv_content.setText(content);
            //SmartPitAppHelper.getInstance(context).setImage(user, avatar, 0, 0);
            user.getProgressBar().setVisibility(View.GONE);
            user.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.google_white));
            user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse(avatar);
                    i.setData(data);
                    context.startActivity(i);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //convertView.startAnimation(animation);
        //lastPosition = position;

        return convertView;
    }
}