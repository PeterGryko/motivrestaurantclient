package pl.slapps.client.motivrestaurantclient.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 15.05.15.
 */
public class CustomClusterRenderer extends DefaultClusterRenderer<Place> {
    private String TAG = CustomClusterRenderer.class.getName();
    public static Bitmap iconBitmap;
    public static Bitmap unknownBitmap;



    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<Place> clusterManager) {
        super(context, map, clusterManager);

        int size = (int)context.getResources().getDimension(R.dimen.map_marker_size);

        iconBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_restaurant), size, size, false);
        unknownBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown), size, size, false);


    }


    @Override
    protected void onClusterItemRendered(Place model, final Marker marker) {
        // Draw a single person.
        // Set the info window to show their name.

       // if (!model.getRLogo().equals("")) {
       //     new RemoteIconMapMarker().initMarker(marker, model.getRLogo(), true);

            //else
            //   marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.icon_restaurant));

       // }
       /*
        else
        {
            DAO.getPlaceDetails(new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    try {
                        Log.d(TAG, o.toString());
                        JSONObject json = new JSONObject(o.toString());
                        JSONObject result = json.has("result") ? json.getJSONObject("result") : new JSONObject();
                        PlaceModel m = new PlaceModel(result.toString());
                        new RemoteIconMapMarker().initMarker(marker, m.getGIcon());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d(TAG, volleyError.toString());

                }
            }, model.getGPlaceId());
        }
        */
    }

    @Override
    protected void onBeforeClusterItemRendered(Place model, final MarkerOptions markerOptions) {
        // Draw a single person.
        // Set the info window to show their name.


        markerOptions.title(MapHelper.PLACE+MapHelper.SEPARATOR +model.getGPlaceId());

        //if (model.getPhotos().size()>0|| model.getGIcon().trim().equals("")) {
        if (model.getGName().equals(""))
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(unknownBitmap));

        else

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconBitmap));
        //}

    }


}

