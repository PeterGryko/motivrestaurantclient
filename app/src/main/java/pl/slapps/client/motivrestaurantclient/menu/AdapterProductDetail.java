package pl.slapps.client.motivrestaurantclient.menu;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentGallery;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentNewComment;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentPlaceComments;
import pl.slapps.client.motivrestaurantclient.model.Product;
import pl.slapps.client.motivrestaurantclient.widget.FadeViewFlipper;
import pl.slapps.client.motivrestaurantclient.widget.RatingWidget;

/**
 * Created by piotr on 27.08.15.
 */
public class AdapterProductDetail extends BaseAdapter {

    public enum VIEW_TYPE {
        CATEGORY, NAME, IMAGES, RATE, DESC, INGREDIENTS_LABEL, INGREDIENT, COMMENTS_LABEL, COMMENTS, COMMENTS_MORE, CART;
    }

    private String TAG = AdapterProductDetail.class.getName();
    private ArrayList<JSONObject> list;
    private MainActivity context;
    private LayoutInflater inflater;
    private int lastPosition = 0;
    private Product product;

    public AdapterProductDetail(MainActivity context, ArrayList<JSONObject> list, Product product) {
        this.context = context;
        this.list = list;
        this.inflater = LayoutInflater.from(context);
        this.product = product;
    }

    @Override
    public int getItemViewType(int position) {

        JSONObject element = list.get(position);
        int result = 0;
        try {
            Log.d(TAG, element.toString());
            String viewType = element.has("view_type") ? element.getString("view_type") : VIEW_TYPE.CATEGORY.name();
            Log.d(TAG, "position " + position + " json view type " + viewType);
            result = VIEW_TYPE.valueOf(viewType).ordinal();
            Log.d(TAG, "position " + position + " json view type " + viewType + "result " + result);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE.values().length;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {
        TextView tvName;
        TextView tvCategory;

        TextView tvPrice;
        TextView tvDesc;
        TextView tvMore;
        TextView tvLabel;
        TextView tvIngredient;

        LinearLayout flipperBase;
        Button btnCart;
        ImageButton btnComment;
        ImageButton btnGallery;


        FadeViewFlipper flipper;
        TextView tvCommentName;
        TextView tvCommentContent;
        SmartImageView user;
        SmartImageView attachment;
        RatingWidget commentRatingWidget;

        public void initView(View v, VIEW_TYPE type) {
            switch (type) {
                case CATEGORY:
                    tvCategory = (TextView) v.findViewById(R.id.tv_category);
                    break;
                case NAME:
                    tvName = (TextView) v.findViewById(R.id.tv_name);
                    break;
                case IMAGES:
                    flipperBase = (LinearLayout) v.findViewById(R.id.flipper_base);
                    btnGallery = (ImageButton) v.findViewById(R.id.btn_gallery);
                    btnComment = (ImageButton) v.findViewById(R.id.btn_comment);
                    break;
                case RATE:
                    tvPrice = (TextView) v.findViewById(R.id.tv_price);
                    break;
                case DESC:
                    tvDesc = (TextView) v.findViewById(R.id.tv_desc);
                    break;

                case INGREDIENTS_LABEL:
                    tvLabel = (TextView) v.findViewById(R.id.tv_label);
                    break;

                case INGREDIENT:
                    tvIngredient = (TextView) v.findViewById(R.id.tv_ing);
                    break;
                case COMMENTS_LABEL:
                    tvLabel = (TextView) v.findViewById(R.id.tv_label);

                    break;

                case COMMENTS:
                    tvCommentName = (TextView) v.findViewById(R.id.tv_name);
                    tvCommentContent = (TextView) v.findViewById(R.id.tv_content);
                    user = (SmartImageView) v.findViewById(R.id.iv_logo);
                    attachment = (SmartImageView) v.findViewById(R.id.iv_media);
                    commentRatingWidget = (RatingWidget) v.findViewById(R.id.rating);
                    break;
                case COMMENTS_MORE:
                    tvMore = (TextView) v.findViewById(R.id.tv_more);
                    break;
                case CART:
                    btnCart = (Button) v.findViewById(R.id.btn_cart);
                    break;


            }
        }

        public void fillData(JSONObject data, VIEW_TYPE type) {
            switch (type) {
                case CATEGORY:

                    tvCategory.setText("kategoria 12");
                    break;
                case NAME:
                    Log.d(TAG, "fill data name " + data.toString());

                    try {
                        String name = data.has("name") ? data.getString("name") : "";
                        tvName.setText(name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case IMAGES:

                    try {
                        JSONArray array = data.has("photos") ? data.getJSONArray("photos") : new JSONArray();


                        flipper = new FadeViewFlipper(context);

                        if (array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                flipper.getUrlsList().add(array.getString(i));
                            }
                            flipperBase.addView(flipper.getView());
                            flipper.start();
                        } else {
                            flipper.getView().getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.mock_product));
                            flipper.getView().getProgressBar().setVisibility(View.GONE);
                            flipperBase.addView(flipper.getView());

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    btnGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            JSONArray array = new JSONArray();
                            for (int i = 0; i < flipper.getUrlsList().size(); i++) {
                                array.put(flipper.getUrlsList().get(i));
                            }
                            Bundle arg = new Bundle();
                            arg.putString("data", array.toString());
                            FragmentGallery fg = new FragmentGallery();
                            fg.setArguments(arg);

                            //context.showMasterMenu();
                            context.switchDrawerFragment(fg);


                        }
                    });

                    btnComment.setOnClickListener(new View.OnClickListener()

                                                  {
                                                      @Override
                                                      public void onClick(View v) {
                                                          context.switchDrawerFragment(new FragmentNewComment());
                                                          //context.showMasterMenu();
                                                      }
                                                  }

                    );
                    break;
                case RATE:
                    try {
                        String price = data.has("price") ? data.getString("price") : "";
                        tvPrice.setText(price);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case DESC:

                    try {
                        String desc = data.has("desc") ? data.getString("desc") : "";

                        tvDesc.setText(desc);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case INGREDIENTS_LABEL:
                    tvLabel.setText("Składniki:");
                    break;

                case INGREDIENT:
                    try {
                        String name = data.has("name") ? data.getString("name") : "";

                        tvIngredient.setText(name);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //tvPrice =(TextView)v.findViewById(R.id.tv_price);
                    break;
                case COMMENTS_LABEL:
                    tvLabel.setText("Komentarze:");

                    break;

                case COMMENTS:
                    tvCommentName.setText("Mark Antoni");
                    tvCommentContent.setText("Jak wyżej, kiedy będą zdjęcia z filmu dostępne na stronie? Bo zaraz umrę z ciekawości. Już je chcę i chcę film zobaczyć... Aaaaa... Dlaczego nie mieszkam w Japonii i nie znam japońskiego? To niesprawiedliwe. Chociaż gdyby to było po angielsku... ale nie miałoby swojego klimatu, więc nie, niech ...");
                    tvCommentName.setTextColor(Color.BLACK);
                    tvCommentContent.setTextColor(Color.BLACK);
                    user.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.mock_profile));
                    user.getProgressBar().setVisibility(View.GONE);
                    attachment.setVisibility(View.GONE);

                    break;
                case COMMENTS_MORE:
                    tvMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            context.switchDrawerFragment(new FragmentPlaceComments());
                        }
                    });
                    break;
                case CART:
                    btnCart.setOnClickListener(new View.OnClickListener()

                                               {
                                                   @Override
                                                   public void onClick(View v) {

                                                       JSONObject cart = new JSONObject();
                                                       try {
                                                           cart.put("count", 1);

                                                           cart.put("product", product.data);

                                                           MyApplication.getCart().add(cart);
                                                           ((MainActivity) context).refreashCart();
                                                       } catch (JSONException e) {
                                                           e.printStackTrace();
                                                       }
                                                   }
                                               }

                    );
                    break;


            }
        }


    }

    public View getView(int position, View convertView, ViewGroup parent) {

        VIEW_TYPE type = VIEW_TYPE.values()[getItemViewType(position)];
        Log.d(TAG, "position " + position + " view type " + type.name());
        ViewHolder vh = null;
        if (convertView == null) {
            switch (type) {
                case CATEGORY:
                    convertView = inflater.inflate(R.layout.product_row_category, null);
                    break;
                case NAME:
                    convertView = inflater.inflate(R.layout.product_row_name, null);
                    break;
                case IMAGES:
                    convertView = inflater.inflate(R.layout.product_row_images, null);
                    break;
                case RATE:
                    convertView = inflater.inflate(R.layout.product_row_rate, null);
                    break;
                case DESC:
                    convertView = inflater.inflate(R.layout.product_row_desc, null);
                    break;
                case INGREDIENTS_LABEL:
                    convertView = inflater.inflate(R.layout.product_row_label, null);
                    break;
                case INGREDIENT:
                    convertView = inflater.inflate(R.layout.product_row_ingredient, null);
                    break;
                case COMMENTS_LABEL:
                    convertView = inflater.inflate(R.layout.product_row_label, null);
                    break;
                case COMMENTS:
                    convertView = inflater.inflate(R.layout.row_comment, null);
                    break;
                case COMMENTS_MORE:
                    convertView = inflater.inflate(R.layout.product_row_comments, null);
                    break;
                case CART:
                    convertView = inflater.inflate(R.layout.product_row_cart, null);
                    break;
                default:
                    convertView = inflater.inflate(R.layout.product_row_category, null);
                    break;


            }
            vh = new ViewHolder();
            vh.initView(convertView, type);

            convertView.setTag(vh);

        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        vh.fillData(list.get(position), type);

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        animation.setDuration(500);
        convertView.startAnimation(animation);
        lastPosition = position;

        return convertView;
    }


}
