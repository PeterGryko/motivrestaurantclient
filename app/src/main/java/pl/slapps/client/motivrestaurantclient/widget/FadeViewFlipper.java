package pl.slapps.client.motivrestaurantclient.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;


import java.util.ArrayList;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;

/**
 * Created by piotr on 31.03.15.
 */
public class FadeViewFlipper {

    private ArrayList<String> urls;
    private Context context;
    private int currentItem = 0;
    private SmartImageView imageView;
    private TransitionDrawable currentTransition;
    private boolean isRunning = false;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (currentItem < urls.size() - 1)
                currentItem++;
            else
                currentItem = 0;

            if (isRunning)
                start();
        }
    };

    public FadeViewFlipper(Context context) {
        init(context);

    }

    public ArrayList<String> getUrlsList() {
        return urls;
    }

    private void init(Context context) {
        this.context = context;
        urls = new ArrayList<String>();
        imageView = new SmartImageView(context);
        imageView.setMode(SmartImageView.Mode.NORMAL.ordinal());
        imageView.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        //imageView.getImageView().setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


    }

    public void start() {

        if (urls.size() == 0)
            return;
        isRunning = true;
        String url = urls.get(currentItem);

        SmartPitActivity.getImageLoader()
                .get(url, new SmartPitImageLoader.SmartImagesListener() {
                    @Override
                    public void onResponse(SmartPitImageLoader.SmartImageContainer container, boolean flag) {
/*)
                        Drawable firstLayer = imageView.getImageView().getDrawable();
                        if (firstLayer == null)
                            firstLayer = new ColorDrawable(android.R.color.transparent);

                        final TransitionDrawable td =


                                new TransitionDrawable(new Drawable[]{
                                        firstLayer,
                                        new BitmapDrawable(context.getResources(), container.getBitmap())
                                });

                        //currentBitmap = container.getBitmap();
                        //imageView.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
*/
                        //imageView.getImageView().setImageBitmap(container.getBitmap());
                        imageView.setVisibility(View.GONE);
                        imageView.getImageView().setImageBitmap(container.getBitmap());
                        SmartPitAppHelper.showViewWithAnimation(imageView,300);
  //                      td.startTransition(1000 * 2);

                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, 0, 0);

        //SmartPitAppHelper.getInstance(context).setImage(imageView,url,0,0);
        if (urls.size() > 1)
            handler.sendMessageDelayed(handler.obtainMessage(), 1000 * 5);
    }

    public SmartImageView getView() {
        return imageView;
    }

    public void stop() {
        isRunning = false;
    }


}
