package pl.slapps.client.motivrestaurantclient.model;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 05.04.15.
 */
public class Place extends JSONObject implements ClusterItem {

    public Place() {
        super();
    }

    public Place(String data) throws JSONException {
        super(data);
    }


/*
*
*
* Google part
*
*
* */

    public int getRVisits() {
        int count = 0;

        try {
            JSONObject data = this.has("doc") ? this.getJSONObject("doc") : this;
          //  data = data.has("doc") ? data.getJSONObject("doc") : data;

            count = data.has("visits") ? data.getInt("visits") : 0;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return count;
    }

    public String getGPlaceId() {
        String place_id = "";
        try {
            //JSONObject data = this.has("api") ? this.getJSONObject("api") : this;
            JSONObject data = this.has("doc") ? this.getJSONObject("doc") : this;

            place_id = data.has("place_id") ? data.getString("place_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return place_id;
    }

    public String getGIcon() {
        String icon = "";
        try {
            JSONObject data = this.has("google") ? this.getJSONObject("google") : this;

            icon = data.has("icon") ? data.getString("icon") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return icon;
    }

    public String getGName() {
        String name = "";
        try {
            JSONObject data = this;
            //  data = data.has("google") ? data.getJSONObject("google") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;
            //  data = data.has("data") ? data.getJSONObject("data") : data;


            name = data.has("name") ? data.getString("name") : "";


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return name;
    }

    public String getGAddress() {
        String address = "";
        try {
            JSONObject data = this;
           // data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;

            address = data.has("formatted_address") ? data.getString("formatted_address") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return address;
    }

    public String getGWeb() {
        String web = "";
        try {
            JSONObject data = this;
            data = data.has("google") ? data.getJSONObject("google") : data;
            web = data.has("website") ? data.getString("website") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return web;
    }

    public String getGGoogleWeb() {
        String google_web = "";
        try {
            JSONObject data = this;
            data = data.has("google") ? data.getJSONObject("google") : data;
            google_web = data.has("url") ? data.getString("url") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return google_web;
    }

    public JSONArray getGReviews() {
        JSONArray revs = new JSONArray();
        try {
            JSONObject data = this;
            data = data.has("google") ? data.getJSONObject("google") : data;
            revs = data.has("reviews") ? data.getJSONArray("reviews") : new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return revs;

    }

    public double getGRate() {
        double rate = 1;
        try {
            JSONObject data = this;
            data = data.has("google")?data.getJSONObject("google"):data;


            rate = data.has("rating") ? data.getDouble("rating") : 1;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rate;
    }

    public ArrayList<String> getTags() {
        ArrayList<String> output = new ArrayList<>();

        try {
            JSONObject data = this;
           // data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;

            JSONArray tags = data.has("tags") ? data.getJSONArray("tags") : new JSONArray();

            for (int i = 0; i < tags.length(); i++) {
                output.add(tags.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return output;
    }

    public String getGPhone() {
        String phone = "";
        try {
            JSONObject data = this;
           // data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("google") ? data.getJSONObject("google") : data;
           // data = data.has("data") ? data.getJSONObject("data") : data;


            phone = data.has("international_phone_number") ? data.getString("international_phone_number") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return phone;
    }

    public ArrayList<String> getGWeekdayText() {
        ArrayList<String> data = new ArrayList<>();
        JSONObject hours = null;
        try {

            JSONObject place = this;
         //   place = place.has("api") ? place.getJSONObject("api") : place;
            place = place.has("doc") ? place.getJSONObject("doc") : place;
           // place = place.has("google") ? place.getJSONObject("google") : place;

            hours = place.has("opening_hours") ? place.getJSONObject("opening_hours") : new JSONObject();

            JSONArray weekday_text = hours.has("weekday_text") ? hours.getJSONArray("weekday_text") : new JSONArray();

            ArrayList<String> weekdayText = new ArrayList<String>();
            for (int i = 0; i < weekday_text.length(); i++) {
                data.add(weekday_text.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    /*

    returns string or integer place types
     */

    public ArrayList<Object> getGTypes() {
        ArrayList<Object> array = new ArrayList<>();
        JSONObject data = this;
        try {
            data = this.has("google") ? this.getJSONObject("google") : this;


            JSONArray types = data.has("types") ? data.getJSONArray("types") : new JSONArray();
            for (int i = 0; i < types.length(); i++) {
                array.add(types.getString(i));


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray types = data.has("types") ? data.getJSONArray("types") : new JSONArray();
            for (int i = 0; i < types.length(); i++) {
                array.add(types.getInt(i));


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }

    public ArrayList<String> getPhotos() {
        ArrayList<String> data = new ArrayList<>();
        try {
            JSONObject g = this;
            g = g.has("google") ? g.getJSONObject("google") : g;
            JSONArray photos = g.has("photos") ? g.getJSONArray("photos") : new JSONArray();
            for (int i = 0; i < photos.length(); i++) {
                //JSONObject photo = photos.getJSONObject(i);

                //String reference = photo.has("photo_reference") ? photo.getString("photo_reference") : "";
                data.add(photos.getString(i));
            }


            JSONObject cover = this.has("facebook") ? this.getJSONObject("facebook") : this;
            cover = cover.has("cover") ? cover.getJSONObject("cover") : cover;
            String source = cover.has("source") ? cover.getString("source") : "";
            if (!source.trim().equals(""))
                data.add(source);
            if (data.size() == 0)
                data.add("https://c4.staticflickr.com/4/3695/8981110642_958366cef2_b.jpg");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public double getDistance() {
        double distance = 0;

        Log.d("XXX","get distance "+this);
        try {

            distance = this.has("distance") ? this.getDouble("distance") : 0;
            Log.d("XXX","get distance "+distance);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return distance;
    }

    public LatLng getGCoords() {
        LatLng pos = null;
        try {
            JSONObject data = this;
            //data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;

            JSONObject geometry = data.has("geometry") ? data.getJSONObject("geometry") : new JSONObject();

            JSONObject location = geometry.has("location") ? geometry.getJSONObject("location") : new JSONObject();
            double lat = location.has("lat") ? location.getDouble("lat") : 0;
            double lon = location.has("lng") ? location.getDouble("lng") : 0;
            if (lat != 0 && lon != 0)

                pos = new LatLng(lat, lon);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pos;
    }


    public String generateGooglePhotoUrl(String reference) {
        String template = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + reference +
                "&key=" + DAO.API_PLACE_KEY;
        return template;
    }





    /*
*
*
* Motiv part
*
* */


    public String getRId() {
        String id = "";
        try {
            JSONObject data = this;
           // data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;
            id = data.has("_id") ? data.getString("_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return id;

    }


    public String getRAddress() {
        String value = "";
        try {
            value = this.has("name") ? this.getString("name") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return value;

    }

    public LatLng getRCoords() {
        LatLng position = null;
        try {

            JSONObject data = this;
         //   data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;
            data = data.has("geometry") ? data.getJSONObject("geometry") : data;
            data = data.has("location") ? data.getJSONObject("location") : data;


            double lat = data.has("lat") ? data.getDouble("lat") : 0;
            double lon = data.has("lon") ? data.getDouble("lon") : 0;
            if (lat != 0 && lon != 0)
                position = new LatLng(lat, lon);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return position;
    }


    public String getRDesc() {
        String value = "";
        try {
            JSONObject d = this.has("data") ? this.getJSONObject("data") : new JSONObject();
            String desc = d.has("desc") ? d.getString("desc") : "";
            value = desc;
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return value;

    }


    public String getRLogo() {
        String value = "";
        try {
            JSONObject data = this;
         //   data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;
            data = data.has("google") ? data.getJSONObject("google") : data;

            //JSONObject logo = data.has("logo") ? data.getJSONObject("logo") : data;
            String path = data.has("avatar") ? data.getString("avatar") : "";
            value = path;

        } catch (JSONException e) {
        }


        return value;

    }


    public String isRestaurant() {
        String result = null;


        try {
            JSONObject data = this;
           // data = data.has("api") ? data.getJSONObject("api") : data;
            data = data.has("doc") ? data.getJSONObject("doc") : data;

            result = data.has("owner") ? data.getString("owner") : null;
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result;
    }

    public ArrayList<String> getRMedia() {
        ArrayList<String> paths = new ArrayList<>();

        try {
            JSONArray media = this.has("media") ? this.getJSONArray("media") : new JSONArray();
            for (int i = 0; i < media.length(); i++) {
                JSONObject m = media.getJSONObject(i);
                String path = m.has("path") ? m.getString("path") : "";
                paths.add(DAO.API + path);

            }
        } catch (Throwable t) {
        }

        return paths;
    }


    @Override
    public LatLng getPosition() {


        if (getRCoords() != null)
            return getRCoords();
        else {
            return getGCoords();
        }

    }


    /*

    facebook
     */

    public Integer getFTalkingsAbout() {
        int c = 0;
        try {
            JSONObject data = this;
            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            c = data.has("talking_about_count") ? data.getInt("talking_about_count") : 0;


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return c;
    }


    public Integer getFCheckings() {
        int c = 0;
        try {
            JSONObject data = this;
            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            c = data.has("checkins") ? data.getInt("checkins") : 0;


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return c;
    }

    public Integer getFLikes() {
        int likes = 0;
        try {
            JSONObject data = this;
            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            likes = data.has("likes") ? data.getInt("likes") : 0;


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return likes;
    }


    public String getFPage() {
        String url = "";
        try {
            JSONObject data = this;
            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            url = data.has("link") ? data.getString("link") : "";


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return url;
    }

    public HashMap<String, Integer> getFParkingInfo() {
        HashMap<String, Integer> output = new HashMap<>();

        try {
            JSONObject data = this;

            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            data = data.has("parking") ? data.getJSONObject("parking") : new JSONObject();

            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {

                    Object value = data.get(key);

                    if (key.trim().equals("lot"))
                        key = "na posesji";
                    else if (key.trim().equals("valet"))
                        key = "miejsce parkingowe";
                    else if (key.trim().equals("street"))
                        key = "przy ulicy";
                    output.put(key, (int) value);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return output;
    }

    public HashMap<String, Integer> getFServicesInfo() {
        HashMap<String, Integer> output = new HashMap<>();

        try {
            JSONObject data = this;

            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            data = data.has("restaurant_services") ? data.getJSONObject("restaurant_services") : new JSONObject();

            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String key = iter.next();


                try {
                    Object value = data.get(key);

                    if (key.trim().equals("walkins"))
                        key = "Na miejscu";
                    else if (key.trim().equals("catering"))
                        key = "Katering";
                    else if (key.trim().equals("outdoor"))
                        key = "Miejsce na zewnątrz";
                    else if (key.trim().equals("reserve"))
                        key = "Rezerwacje";
                    else if (key.trim().equals("kids"))
                        key = "Strefa dziecięca";
                    else if (key.trim().equals("delivery"))
                        key = "Dostawy";
                    else if (key.trim().equals("waiter"))
                        key = "Kelner";
                    else if (key.trim().equals("takeout"))
                        key = "Na wynos";
                    else if (key.trim().equals("groups"))
                        key = "Zamówienia grupowe";
                    output.put(key, (int) value);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return output;
    }

    public HashMap<String, Integer> getFSpecialitiesInfo() {
        HashMap<String, Integer> output = new HashMap<>();

        try {
            JSONObject data = this;

            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            data = data.has("restaurant_specialties") ? data.getJSONObject("restaurant_specialties") : new JSONObject();

            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String key = iter.next();


                try {

                    Object value = data.get(key);

                    if (key.trim().equals("dinner"))
                        key = "Obiady";
                    else if (key.trim().equals("lunch"))
                        key = "Lunche";
                    else if (key.trim().equals("drinks"))
                        key = "Napoje";
                    else if (key.trim().equals("breakfast"))
                        key = "Śniadania";
                    else if (key.trim().equals("coffee"))
                        key = "Kawy";

                    output.put(key, (int) value);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return output;
    }

    public String getFDescription() {
        String c = "";
        try {
            JSONObject data = this;
            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            c = data.has("description") ? data.getString("description") : "";


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return c;
    }

    public String getFPriceRange() {
        String c = "";
        try {
            JSONObject data = this;
            data = data.has("facebook") ? data.getJSONObject("facebook") : data;
            c = data.has("price_range") ? data.getString("price_range") : "";


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return c;
    }


}
