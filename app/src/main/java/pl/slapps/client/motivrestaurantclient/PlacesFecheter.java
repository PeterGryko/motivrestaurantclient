package pl.slapps.client.motivrestaurantclient;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 16.05.15.
 */
public class PlacesFecheter {

    public static String TAG = PlacesFecheter.class.getName();

    public interface FechPlacesListener {

        public void onSuccess(ArrayList<Place> places, Place place, JSONObject geocoder);

        public void onError(VolleyError error);


    }


    public static void fechPlaces(final FechPlacesListener listener, String query, LatLng position, String placeId) {
        Log.d(TAG, "fech places " + placeId);
        if (query != null)
            query = query.replaceAll(" ", "%20");

        DAO.getPlaces(new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                Log.d(TAG, "places fached " + o.toString());

                try {
                    JSONObject data = new JSONObject(o.toString());
                    JSONObject geocoder = data.has("geocoder") ? data.getJSONObject("geocoder") : new JSONObject();

                    JSONObject google = data.has("google") ? data.getJSONObject("google") : data;

                    JSONObject api= data.has("api") ? data.getJSONObject("api") : data;

                    ArrayList<Place> places = new ArrayList<Place>();

                    JSONObject doc = api.has("doc") ? api.getJSONObject("doc") : null;

                    if (doc != null) {
                        listener.onSuccess(null, new Place(api.toString()), geocoder);
                    } else {
                        JSONArray motiv = api.has("results") ?api.getJSONArray("results") : new JSONArray();
                        JSONArray nearby = google.has("nearby") ? google.getJSONArray("nearby") : new JSONArray();
                        JSONArray radar = google.has("radar") ? google.getJSONArray("radar") : new JSONArray();
                        JSONArray textsearch = google.has("textSearch") ? google.getJSONArray("textSearch") : new JSONArray();


                        for (int i = 0; i < motiv.length(); i++) {
                            places.add(new Place(motiv.getJSONObject(i).toString()));
                        }
                        for (int i = 0; i < nearby.length(); i++) {
                            places.add(new Place(nearby.getJSONObject(i).toString()));
                        }
                        for (int i = 0; i < radar.length(); i++) {
                            places.add(new Place(radar.getJSONObject(i).toString()));
                        }

                        for (int i = 0; i < textsearch.length(); i++) {
                            places.add(new Place(textsearch.getJSONObject(i).toString()));
                        }


                        listener.onSuccess(places, null, geocoder);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, "error " + volleyError.toString());
                Log.d(TAG, "msg " + AppHelper.getErrorMessage(volleyError));

                listener.onError(volleyError);
            }
        }, query, position, placeId);
    }


    public static void fechPlacesByTag(final FechPlacesListener listener, String tag) {


        DAO.getPlacesByTag(tag, new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                Log.d(TAG, "places fached " + o.toString());

                try {
                    JSONObject data = new JSONObject(o.toString());
                    JSONObject geocoder = data.has("geocoder") ? data.getJSONObject("geocoder") : new JSONObject();
                    data = data.has("api") ? data.getJSONObject("api") : new JSONObject();

                    ArrayList<Place> places = new ArrayList<Place>();


                    JSONArray motiv = data.has("results") ? data.getJSONArray("results") : new JSONArray();


                    for (int i = 0; i < motiv.length(); i++) {
                        places.add(new Place(motiv.getJSONObject(i).toString()));
                    }


                    listener.onSuccess(places, null, geocoder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, "error " + volleyError.toString());
                Log.d(TAG, "msg " + AppHelper.getErrorMessage(volleyError));

                listener.onError(volleyError);
            }
        });
    }


}
