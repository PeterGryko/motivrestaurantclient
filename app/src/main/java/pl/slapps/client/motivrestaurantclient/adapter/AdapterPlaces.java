package pl.slapps.client.motivrestaurantclient.adapter;

import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 05.04.15.
 */
public class AdapterPlaces extends ArrayAdapter<String> implements
        Filterable {


    private String TAG = "AdapterDeliveryAddress";
    private RequestQueue jr;
    private MainActivity context;
    private ArrayList<Place> list = new ArrayList<Place>();
    ArrayList<Place> res;


    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            list.removeAll(list);
            list.addAll(res);

            AdapterPlaces.this.notifyDataSetChanged();
        }
    };

    public AdapterPlaces(MainActivity context) {
        super(context, R.layout.row_search);
        // TODO Auto-generated constructor stub
        jr = Volley.newRequestQueue(context);
        this.context = context;

    }



    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_search, null);

        boolean google = false;
        String name = "";
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        if (list.get(position).getGName().trim().equals("")) {
            google = true;
            name = list.get(position).getGName();
        } else
            name = list.get(position).getGName();

        tvName.setText(Html.fromHtml(name));
       // SmartImageView image = (SmartImageView) convertView.findViewById(R.id.logo);


      //  image.setMode(SmartImageView.Mode.NORMAL.ordinal());
      //  image.setVisibility(View.GONE);
        //String logo = list.get(position).getRLogo();
       // if (logo != null && !logo.trim().equals(""))
       //     SmartPitImageLoader.setImageForListView(context, image, logo, 0, 0,position);

        return convertView;
    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int index) {
        return list.get(index).getGName();
    }

    public Place getPoint(int index) {
        return list.get(index);
    }


    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        return new Filter() {

            @Override
            protected FilterResults performFiltering(final CharSequence arg0) {
                // TODO Auto-generated method stub

                Log.d(TAG, "perform filtering " + arg0);
                if (arg0 == null) {
                    res = new ArrayList<>();
                    handler.sendMessage(handler.obtainMessage());
                } else {
                    new FilterAddresses(arg0.toString(), handler).start();
                }
                FilterResults result = new FilterResults();
                result.values = list;
                result.count = list.size();

                return result;
            }

            @Override
            protected void publishResults(CharSequence arg0, FilterResults arg1) {

            }
        };
    }


    class FilterAddresses extends Thread {


        class MixedResults {

            ArrayList<Place> restaurants;
            ArrayList<Place> places;
            ArrayList<Place> dataList;

            public MixedResults(ArrayList<Place> data) {
                this.dataList = data;
            }


            public void populateRestaurants(JSONArray motiv) {

                ArrayList<Place> tmp = new ArrayList<>();
                for (int i = 0; i < motiv.length(); i++) {
                    try {

                        tmp.add(new Place(motiv.getJSONObject(i).toString()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                restaurants = tmp;
                Log.d(TAG, "restaurants populated");
                if (places != null)
                    sendResult();
            }

            public void populatePlaces(AutocompletePredictionBuffer autocompletePredictions) {

                Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
                ArrayList<Place> tmp = new ArrayList<>();
                while (iterator.hasNext()) {
                    AutocompletePrediction prediction = iterator.next();

                    try {
                        Place model = new Place();
                        model.put("place_id", prediction.getPlaceId());
                        model.put("name", prediction.getDescription());

                        JSONArray types = new JSONArray();

                        for (int j = 0; j < prediction.getPlaceTypes().size(); j++) {
                            int type = prediction.getPlaceTypes().get(j);

                            types.put(type);

                        }
                        model.put("types", types);
                        //populateResult(model, dataList);

                        tmp.add(model);

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                }
                autocompletePredictions.release();
                places = tmp;
                if (restaurants != null)
                    sendResult();

            }


            private void sendResult() {

                for (int i = 0; i < restaurants.size(); i++) {
                    populateResult(restaurants.get(i), dataList);
                }
                for (int i = 0; i < places.size(); i++) {
                    populateResult(places.get(i), dataList);
                }


                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);
            }

        }

        private String TAG = FilterAddresses.class.getName();

        private URL url;
        private HttpURLConnection httpConn;
        private BufferedReader br;
        ;
        private String arg0;
        private Handler handler;

        public FilterAddresses(String arg0, Handler handler) {
            this.arg0 = arg0;
            this.handler = handler;
        }


        @Override
        public void run() {


            try {

                if (res != null)
                    res.removeAll(res);
                else
                    res = new ArrayList<Place>();
                final MixedResults mr = new MixedResults(res);

                /*
                Log.d(TAG, "start motiv query " + arg0);

                DAO.getAutocomplete(jr, new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {

                        Log.d(TAG, "request completetd " + arg0);


                        // Log.d(TAG, o.toString());
                        try {
                            JSONObject json = new JSONObject(o.toString());
                            JSONArray motiv = json.has("motiv_autocomplete") ? json.getJSONArray("motiv_autocomplete") : new JSONArray();

                            mr.populateRestaurants(motiv);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        mr.populateRestaurants(new JSONArray());

                    }
                }, arg0.replaceAll(" ", "%20"));

*/
                mr.populateRestaurants(new JSONArray());
                PendingResult<AutocompletePredictionBuffer> results =
                        Places.GeoDataApi
                                .getAutocompletePredictions(context.mGoogleApiClient, arg0,
                                        new LatLngBounds(new LatLng(51.057159, 14.986014), new LatLng(54.177114, 22.808280)), null);

                // This method should have been called off the main UI thread. Block and wait for at most 60s
                // for a result from the API.
                final AutocompletePredictionBuffer autocompletePredictions = results
                        .await(5, TimeUnit.SECONDS);
                final Status status = autocompletePredictions.getStatus();
                if (!status.isSuccess()) {

                    Log.e(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                    autocompletePredictions.release();

                }

                mr.populatePlaces(autocompletePredictions);


            } catch (Throwable e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public void populateResult(Place element, ArrayList<Place> list) {
        list.add(element);

    }
}