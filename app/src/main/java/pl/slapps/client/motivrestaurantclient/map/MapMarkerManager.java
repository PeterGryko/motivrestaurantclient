package pl.slapps.client.motivrestaurantclient.map;

import android.app.Activity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 14.05.15.
 */
public class MapMarkerManager {

    private String TAG = MapMarkerManager.class.getName();

    private ClusterManager<Place> pointsClusterManager;
    private Activity context;
    private ArrayList<Place> places;
    private ArrayList<Marker> markers;
    private int map_camera_padding;
    private int map_bounds_width;
    private int map_bounds_height;

    public ArrayList<Place> getPlaces() {
        return places;
    }


    public MapMarkerManager(Activity context) {
        this.context = context;
        map_camera_padding = (int) context.getResources().getDimension(R.dimen.map_camra_padding);
        map_bounds_width = (int) ((float) SmartPitAppHelper.getScreenWidth(context) / 1.25f);
        map_bounds_height = (int) ((float) SmartPitAppHelper.getScreenHeight(context) / 1.5f);

    }


    private void setUpClusters(final GoogleMap map) {
        // Declare a variable for the cluster manager.


        // Position the map.
        //getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        pointsClusterManager = new ClusterManager<Place>(context, map);
        pointsClusterManager.setRenderer(new CustomClusterRenderer(context, map, pointsClusterManager));

        pointsClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<Place>() {
            @Override
            public boolean onClusterClick(Cluster<Place> placeModelCluster) {
                float zoom = map.getCameraPosition().zoom;

                map.animateCamera(CameraUpdateFactory.newLatLngZoom(placeModelCluster.getPosition(), zoom + 1));
                return true;
            }
        });


        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        map.setOnCameraChangeListener(pointsClusterManager);
        map.setOnMarkerClickListener(pointsClusterManager);
        markers = new ArrayList<>();

        // Add cluster items (markers) to the cluster manager.

    }

    public void resetPoints(GoogleMap map) {
        refreashPoints(MyApplication.getPlaces(),  map);
    }

    public void clearMap(GoogleMap map)
    {
        if(pointsClusterManager!=null)
            pointsClusterManager.clearItems();
        map.clear();
    }

    public void refreashPoints(ArrayList<Place> places,  GoogleMap map) {

        if (pointsClusterManager == null)
            setUpClusters(map);

        clearMap(map);
        for (int i = 0; i < markers.size(); i++) {
            markers.get(i).remove();
        }
        markers.removeAll(markers);


        this.places = places;
        ArrayList<LatLng> coords = new ArrayList<>();
        for (int i = 0; i < places.size(); i++) {


            pointsClusterManager.addItem(places.get(i));
            coords.add(places.get(i).getPosition());
        }

        pointsClusterManager.cluster();




        animateMap(coords, map);


    }



    public void animateMap(ArrayList<LatLng> places, GoogleMap map) {
        if (places.size() > 0) {
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            for (LatLng p : places) {
                b.include(p);
            }
            LatLngBounds bounds = b.build();

//Change the padding as per needed
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, map_bounds_width,
                    map_bounds_height,
                    map_camera_padding);
            map.animateCamera(cu);
        }
    }


}
