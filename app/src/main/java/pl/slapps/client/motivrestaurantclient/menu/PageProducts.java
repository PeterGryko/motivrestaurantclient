package pl.slapps.client.motivrestaurantclient.menu;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.model.Product;

/**
 * Created by piotr on 09.07.15.
 */
public class PageProducts {

    public interface RevindListener {
        public void onFinished();
    }

    private String TAG = PageProducts.class.getName();

    private ObservableListView listView;
    private Context context;
    // private SmartPitFragmentsInterface listener;
    private ArrayList<Product> data;

    private boolean reviningList = false;
    private RevindListener revindListener;

    private ProgressBar bar;

    private Bundle outState;

    public Bundle onSaveInstanceState() {

        Log.d("XXX", "products on save " + outState.getString("data"));
        return outState;
    }

    public View init(final Context context, final FragmentMenu menu, final Place restaurant, JSONObject parent_category, Bundle savedInstanceState) {
        this.context = context;
        outState = new Bundle();
        //   this.listener = menu.getFragmentsListener();
        data = new ArrayList<>();

        View v = LayoutInflater.from(context).inflate(R.layout.fragment_list, null);
        listView = (ObservableListView) v.findViewById(R.id.lv);
        bar = (ProgressBar) v.findViewById(R.id.bar);

        if (data.size() > 0)
            bar.setVisibility(View.GONE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle arg = new Bundle();
                arg.putString("data", data.get(position).data.toString());
                FragmentProduct fp = new FragmentProduct();
                fp.setArguments(arg);
                ((MainActivity) context).switchDrawerFragment(fp);
            }
        });
        listView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int i, boolean b, boolean b2) {
                Log.d(TAG, "scroll chagned " + i);

                if (!reviningList) {
                    // menu.setListPosition(i);
                } else {
                    if (i == 0) {
                        revindListener.onFinished();
                        reviningList = false;
                    }

                }

            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                // Log.d(TAG, "scroll state  " + scrollState.name());

            }
        });

        if (savedInstanceState != null && savedInstanceState.getString("data") != null) {
            try {
                parseHttpResponse(savedInstanceState.getString("data"));
                bar.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            String id = null;
            String parent_id = null;
            try {
                id = restaurant.isRestaurant();

                parent_id = parent_category.has("_id") ? parent_category.getString("_id") : null;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            DAO.getProducts(new Response.Listener() {
                @Override
                public void onResponse(Object o) {

                    Log.d(TAG, o.toString());
                    try {
                        outState.putString("data", o.toString());
                        parseHttpResponse(o.toString());
                        bar.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    bar.setVisibility(View.GONE);
                    try {
                        outState.putString("data", "");

                        parseHttpResponse(volleyError.toString());
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }

                }
            }, id, parent_id);
        }

        // SmartPitAppHelper.getInstance(context).setListViewHeightBasedOnChildren(listView);

        return v;
    }

    private void parseHttpResponse(String o) throws JSONException {


        Log.d(TAG, "parse http results " + o.toString());
        JSONObject result = new JSONObject(o.toString());
        result = result.has("api") ? result.getJSONObject("api") : result;

        JSONArray array = result.has("results") ? result.getJSONArray("results") : new JSONArray();
        for (int i = 0; i < array.length(); i++) {
            data.add(new Product(array.getJSONObject(i)));
        }

        //for (int i = 0; i < 15; i++) {
        //    JSONObject object = new JSONObject();
        //    object.put("name", Integer.toString(i));
        //    data.add(object);
        // }
        listView.setAdapter(new AdapterProducts(context, data));


    }

    public ListView getListView() {
        return listView;
    }

    public void revindList(RevindListener listener) {

        if (listView.getCurrentScrollY() == 0) {

            listener.onFinished();
        } else {
            revindListener = listener;
            reviningList = true;
            listView.setSelection(0);
        }
    }


}
