package pl.slapps.client.motivrestaurantclient.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.page.PageAvailable;
import pl.slapps.client.motivrestaurantclient.page.PageNearest;
import pl.slapps.client.motivrestaurantclient.page.PageNews;
import pl.slapps.client.motivrestaurantclient.page.PagePromotions;
import pl.slapps.client.motivrestaurantclient.widget.SelectorView;

/**
 * Created by piotr on 28.03.15.
 */


public class FragmentRestaurantsBase extends SmartPitPagerFragment {

    public String TAG = FragmentRestaurantsBase.class.getName();
    private View v;
    private LayoutInflater inflater;
    private ArrayList<SelectorView> tabs;
    private PageNearest page_nearest;
    private PageAvailable page_available;

    private TextView tv_location;
    private TextView tv_search;
    private ImageView btn_current_location;
    private LinearLayout btn_info;
    private int current_tab = 0;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_restaurants_base, parent, false);

        ((MainActivity)this.getActivity()).getAbImpl().showSearchbarBase();

        this.inflater = inflater;
        tabs = new ArrayList<>();
        tv_location = (TextView) v.findViewById(R.id.tv_location);
        tv_search = (TextView) v.findViewById(R.id.tv_query);
        btn_info = (LinearLayout) v.findViewById(R.id.btn_info);
        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentRestaurantsBase.this.getActivity()).showSlaveMenu();
            }
        });

        MainActivity.GeoCoder geoCoder = ((MainActivity) this.getActivity()).getCurrentGeocoder();
        if (geoCoder != null) {
            tv_location.setText(geoCoder.locatity);
            if (geoCoder.is_current_location)
                tv_search.setText("Twoja lokalizacja");
            else
                tv_search.setText(geoCoder.formatted_address);
        }
        btn_current_location = (ImageView) v.findViewById(R.id.btn_current_location);

        btn_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentRestaurantsBase.this.getActivity()).setCurrentLocationContext();

                // ((MainActivity) FragmentRestaurantsBase.this.getActivity()).showCurrentLocationContext();
            }
        });


        v.setLayoutParams(new LinearLayout.LayoutParams(SmartPitAppHelper.getScreenWidth(this.getActivity()), ViewGroup.LayoutParams.MATCH_PARENT));

        page_nearest = new PageNearest();
        page_available = new PageAvailable();

        ArrayList<View> views = new ArrayList<>();
        views.add(page_nearest.createView((MainActivity) this.getActivity(), inflater));

        views.add(new PageNews().createView(inflater));

        views.add(page_available.createView((MainActivity) this.getActivity(), inflater));

        views.add(new PagePromotions().createView(inflater));
        // views.add(new PageOrders().createView(inflater));


        this.setViewsPager(v, R.id.pager, views);  //;
        this.getPager().setOffscreenPageLimit(3);

        setOnSwipeListener(new OnSwipeListener() {
            @Override
            public void onSwipeRight(double v, int i) {
                Log.d(TAG, "right " + i);
                if (i < tabs.size()) {
                    tabs.get(i).setAnimationProgress((float) v);
                    tabs.get(i + 1).setAnimationProgress(1 - (float) v);
                }
            }

            @Override
            public void onSwipeLeft(double v, int i) {
                Log.d(TAG, "left " + i);
                if (i < tabs.size()) {
                    tabs.get(i).setAnimationProgress((float) v);
                    tabs.get(i + 1).setAnimationProgress(1 - (float) v);
                }
            }
        });

        return v;
    }

    public void onAdapterSetted() {
        Log.d(TAG, "adapter setted ");
        tabs.get(this.getHost().getCurrentTab()).setAnimationProgress(0);

    }


    @Override
    public String getLabel() {
        return null;
    }

    @Override
    public View createTabIndicator(Context context, int index) {

        View view = LayoutInflater.from(context).inflate(R.layout.restaurant_tab, null);
        SelectorView selector = (SelectorView) view.findViewById(R.id.selector);

        selector.getImageView().setPadding(10, 10, 10, 10);

        tabs.add(selector);


        switch (index) {
            case 0:
                selector.configure(R.drawable.circle_profile_sel, R.drawable.circle_profile, R.drawable.earth);
                break;
            case 1:
                selector.configure(R.drawable.circle_profile_sel, R.drawable.circle_profile, R.drawable.info);
                break;
            case 2:
                selector.configure(R.drawable.circle_profile_sel, R.drawable.circle_profile, R.drawable.delivery);
                break;
            case 3:
                selector.configure(R.drawable.circle_profile_sel, R.drawable.circle_profile, R.drawable.proms);
                break;

        }
        return view;


    }

    public void onPageScrollStateChanged(int state) {
        super.onPageScrollStateChanged(state);

        if (state == 0) {
            for (int i = 0; i < tabs.size(); i++) {
                if (i != this.getPager().getCurrentItem()) {
                    tabs.get(i).finishAnimation(1);
                }
            }
        }
    }


    public void refreashData(ArrayList<Place> places, MainActivity.GeoCoder geoCoder, String query) {
        String address = null;

        address = geoCoder.locatity;

        tv_location.setText(address);


        if (geoCoder.is_current_location)
            tv_search.setText("Twoja lokalizacja");
        else if (query != null)
            tv_search.setText(query);
        else
            tv_search.setText(geoCoder.formatted_address);


        page_available.refreashData(places);
        page_nearest.refreashData(places);
    }

    public void resetData() {


        page_available.refreashData(MyApplication.getPlaces());
        page_nearest.refreashData(MyApplication.getPlaces());
    }


}
