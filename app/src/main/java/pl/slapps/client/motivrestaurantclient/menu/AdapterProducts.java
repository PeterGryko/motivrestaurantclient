package pl.slapps.client.motivrestaurantclient.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.menu.AdapterCategories;
import pl.slapps.client.motivrestaurantclient.model.Product;

/**
 * Created by piotr on 09.07.15.
 */
public class AdapterProducts extends ArrayAdapter {
    private Context context;
    private ArrayList<Product> list;
    private int lastPosition = 0;
    private String TAG = AdapterCategories.class.getName();

    public AdapterProducts(Context context, ArrayList<Product> list) {
        super(context, R.layout.row_product, list);

        this.context = context;
        this.list = list;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_product, null);

        final TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);
        final TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        SmartImageView image = (SmartImageView) convertView.findViewById(R.id.image);
        image.setMode(SmartImageView.Mode.NORMAL.ordinal());
        image.setErrorImage(context.getResources().getDrawable(R.drawable.mock_product));

        Product p = list.get(position);

        if (p.urls.size() > 0) {
            String path = p.urls.get(0);
            SmartPitImageLoader.setImage(context, image, path, 0, 0);
            image.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            image.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.mock_product));
            image.getProgressBar().setVisibility(View.GONE);

        }


        tvPrice.setText(p.price + "zł");
        tvName.setText(p.name);


        //Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //convertView.startAnimation(animation);
        //lastPosition = position;

        return convertView;
    }
}