package pl.slapps.client.motivrestaurantclient.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 03.08.15.
 */
public class FragmentContextInfo extends SmartPitFragment {

    private TextView tv_title;
    private TextView tv_desc;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_context_info, parent, false);

        tv_desc = (TextView) v.findViewById(R.id.tv_desc);
        tv_title = (TextView) v.findViewById(R.id.tv_title);

        if(this.getArguments()!=null)
        {
            String query = this.getArguments().getString("query");
            refreashData(query);
        }
        return v;
    }

    public void refreashData(final String query) {
        DAO.getWikipediaDescription(query, new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                if (FragmentContextInfo.this.isAdded()) {
                    tv_title.setText(query);
                    parseData(o.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    private void parseData(String data) {
        try {
            JSONObject object = new JSONObject(data.toString());
            JSONObject query = object.has("query") ? object.getJSONObject("query") : new JSONObject();
            JSONObject pages = query.has("pages") ? query.getJSONObject("pages") : new JSONObject();

            while (pages.keys().hasNext()) {
                String key = pages.keys().next();
                if (pages.get(key) instanceof JSONObject) {
                    JSONObject page = pages.getJSONObject(key);
                    String text = page.has("extract") ? page.getString("extract") : "" ;
                    tv_desc.setText(text);
                    return;
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
