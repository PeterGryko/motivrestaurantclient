package pl.slapps.client.motivrestaurantclient.menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.RestaurantInterface;

/**
 * Created by piotr on 26.03.15.
 */
public class FragmentCart extends SmartPitFragment implements RestaurantInterface{
    private ListView lv;
    private ArrayList<JSONObject> list;
    private LinearLayout btnBuy;
    private TextView tv_label;
    private TextView tv_sum;

    private AdapterCart adapter;
    private SmartPitSelectorView btn_till;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_cart, parent, false);
        lv = (ListView) v.findViewById(R.id.list_view);
        tv_label = (TextView) v.findViewById(R.id.tv_label);
        tv_sum = (TextView) v.findViewById(R.id.tv_sum);
        btn_till = (SmartPitSelectorView) v.findViewById(R.id.btn_till);
        btn_till.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.till);


        btn_till.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentCart.this.getFragmentsListener().switchFragment(new FragmentConfirmBuy(), true);


            }
        });


        if (MyApplication.getCart().size() > 0)
            tv_label.setVisibility(View.GONE);
       // adapter = new AdapterCart(this.getActivity(), MyApplication.getCart(), this);
       // lv.setAdapter(adapter);
        //this.initMap();

       // adapter.calculateSum();
        return v;
    }

    public void refreashSum(double price) {
        tv_sum.setText(price + "zł");
    }

    @Override
    public String getLabel() {
        return null;
    }
}


