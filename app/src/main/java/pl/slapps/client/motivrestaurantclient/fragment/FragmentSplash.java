package pl.slapps.client.motivrestaurantclient.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppDialog;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitRowedLayout;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.PlacesFecheter;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.adapter.AdapterCities;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.page.PageSplashThree;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;
import pl.slapps.client.motivrestaurantclient.widget.CustomHorizontalScrollView;

/**
 * Created by piotr on 29.03.15.
 */
public class FragmentSplash extends SmartPitPagerFragment {

    private String TAG = FragmentSplash.class.getName();

    private LocationManager locationManager;
    private AutoCompleteTextView tvCity;
    private AdapterCities adapter;
    private InputMethodManager imm;
    private boolean isChecking = false;
    private SmartPitSelectorView btn_enter;
    private ArrayList<View> views;
    //private TextView tvLastCity;
    private RelativeLayout layout_pager;
    private LinearLayout layout_login;
    private SmartPitRowedLayout placesBase;
    private SurfaceView surfaceViewFrame;
    private View background;
    private int[] bkg_color_one = new int[]{50, 0, 0, 0};
    private int[] bkg_color_two = new int[]{50, 255, 255, 0};
    private int[] bkg_color_three = new int[]{50, 255, 0, 255};
    private ArrayList<int[]> colors;
    private CustomHorizontalScrollView hsv;
    private float video_width;
    private float video_height;
    private ProgressBar bar;
    private int btnPadding;


    private View getMyPlaceButton() {

        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.splash_place_buble, null);

        TextView tvCity = (TextView) v.findViewById(R.id.tv_city);
        tvCity.setText("Moja lokalizacja");
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLocation();

            }
        });

        return v;
    }

    private View getInputPlaceButton() {

        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.splash_place_buble, null);

        TextView tvCity = (TextView) v.findViewById(R.id.tv_city);
        tvCity.setText("Inna");
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FragmentSplash.this.tvCity.setVisibility(View.VISIBLE);
                FragmentSplash.this.placesBase.setVisibility(View.GONE);

            }
        });

        return v;
    }

    private View getCityButton(final String city, final double lat, final double lon, final String id) {

        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.splash_place_buble, null);

        TextView tvCity = (TextView) v.findViewById(R.id.tv_city);
        tvCity.setText(city);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeRequest(city, new LatLng(lat, lon), id);

            }
        });

        return v;
    }

    private void loadLastCities() {

        String cities = SmartPitAppHelper.getPreferences(this.getActivity()).getString("visited_cities", null);

        if (cities != null) {
            try {
                JSONObject data = new JSONObject(cities);

                Iterator<String> iterator = data.keys();

                while (iterator.hasNext()) {
                    String city = iterator.next();
                    JSONObject details = data.getJSONObject(city);
                    String id = details.has("id") ? details.getString("id") : null;
                    double lat = details.has("lat") ? details.getDouble("lat") : 0;
                    double lon = details.has("lon") ? details.getDouble("lon") : 0;


                    placesBase.addView(getCityButton(city, lat, lon, id));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "visited cities laoded " + cities);


    }

    private void geocodeIp() {
        DAO.geocodeIp(new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                Log.d(TAG, o.toString());

                try {
                    JSONObject data = new JSONObject(o.toString());
                    JSONObject api = data.has("api") ? data.getJSONObject("api") : new JSONObject();
                    JSONObject ip = api.has("ip") ? api.getJSONObject("ip") : new JSONObject();
                    String city = ip.has("city") ? ip.getString("city") : "";
                    double lat = ip.has("latitude") ? ip.getDouble("latitude") : 0;
                    double lon = ip.has("longitude") ? ip.getDouble("longitude") : 0;

                    placesBase.addView(getCityButton(city, lat, lon, null));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());

            }
        });
    }


    private void checkLocation() {


        if (locationManager == null || isChecking)
            return;

        if (!SmartPitAppHelper.isConnected(FragmentSplash.this.getActivity())) {
            SmartPitAppDialog.getInfoDialog(AlertDialog.THEME_HOLO_LIGHT, FragmentSplash.this.getActivity(), "Sprawdź połączenie z internetem").show();
            return;
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            //All location services are disabled
            Log.d(TAG, "location is enabled!");


            SmartPitAppHelper.hideViewWithAnimation(layout_pager, 300);
            SmartPitAppHelper.hideViewWithAnimation(layout_login, 300);

            SmartPitAppHelper.showViewWithAnimation(bar, 300);

            isChecking = true;
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d(TAG, "location changed! " + location.getLatitude() + " " + location.getLongitude());
                    locationManager.removeUpdates(this);

                    final LatLng place = new LatLng(location.getLatitude(), location.getLongitude());
                    makeRequest(null, place, null);

                    isChecking = false;

                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            }, null);

        } else {

            final AlertDialog alert = SmartPitAppDialog.getInfoDialog(AlertDialog.THEME_HOLO_LIGHT, FragmentSplash.this.getActivity(), "Czy chcesz włączyć GPS?");
            alert.setButton("tak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ((MainActivity) FragmentSplash.this.getActivity()).enableGPS();
                    alert.dismiss();
                }
            });

            alert.setButton2("nie", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    alert.dismiss();
                }
            });
            alert.show();

            //    bar.setVisibility(View.GONE);

        }
    }


    private void saveLastCity(String name, LatLng pos, String id) {
        SmartPitAppHelper.getPreferences(this.getActivity()).edit().putString("city", name).putFloat("lat", (float) pos.latitude).putFloat("lon", (float) pos.longitude).putString("id", id).commit();
    }

    private void makeRequest(final String query, final LatLng position, final String id) {

        if (!SmartPitAppHelper.isConnected(FragmentSplash.this.getActivity())) {
            SmartPitAppDialog.getInfoDialog(AlertDialog.THEME_HOLO_LIGHT, FragmentSplash.this.getActivity(), "Sprawdź połączenie z internetem").show();
            return;
        }

        Log.d(TAG, "make request " + query);


        bar.setVisibility(View.VISIBLE);
        layout_login.setVisibility(View.GONE);

        PlacesFecheter.fechPlaces(new PlacesFecheter.FechPlacesListener() {
            @Override
            public void onSuccess(ArrayList<Place> places, Place place, JSONObject geocoder) {


                MyApplication.setPlaces(places);
                MyApplication.setCurrentPlaces(places);

                saveLastCity(query, position, id);

                if (FragmentSplash.this.isAdded())
                    ((MainActivity) FragmentSplash.this.getActivity()).initMap(geocoder);
                else {
                    layout_login.setVisibility(View.VISIBLE);

                    bar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(VolleyError error) {
                Log.d(TAG, error.toString());
                bar.setVisibility(View.GONE);
                layout_login.setVisibility(View.VISIBLE);
                loadLastCities();
                Toast.makeText(FragmentSplash.this.getActivity(), "Spróbuj ponownie", Toast.LENGTH_LONG).show();


                //    tvCity.setVisibility(View.VISIBLE);
                //   tvLabel.setVisibility(View.VISIBLE);
                //   bar.setVisibility(View.GONE);

            }
        }, query, position, id);


    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_splash, parent, false);
        surfaceViewFrame = (SurfaceView) v.findViewById(R.id.sv);
        btnPadding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_padding_large);
        bar = (ProgressBar) v.findViewById(R.id.bar);
        bar.setVisibility(View.GONE);


        bar.getIndeterminateDrawable().setColorFilter(this.getResources().getColor(R.color.profile_indicator_on), PorterDuff.Mode.MULTIPLY);


        //tvLastCity = (TextView) v.findViewById(R.id.tv_last_city);

        //loadLastCities();
        colors = new ArrayList<>();
        colors.add(bkg_color_one);
        colors.add(bkg_color_two);
        colors.add(bkg_color_three);

        video_width = SmartPitAppHelper.getScreenWidth(this.getActivity()) * 1.5f;
        video_height = SmartPitAppHelper.getScreenHeight(this.getActivity());


        background = (View) v.findViewById(R.id.background);
        background.setBackgroundColor(Color.argb(bkg_color_one[0], bkg_color_one[1], bkg_color_one[2], bkg_color_one[3]));
        tvCity = (AutoCompleteTextView) v.findViewById(R.id.et_city);
        adapter = new AdapterCities((MainActivity) this.getActivity(), android.R.layout.simple_list_item_1);
        tvCity.setAdapter(adapter);
        tvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                makeRequest(adapter.getPoint(i).getCity(), new LatLng(adapter.getPoint(i).getLat(), adapter.getPoint(i).getLon()), adapter.getPoint(i).getId());
            }
        });
        btn_enter = (SmartPitSelectorView) v.findViewById(R.id.btn_enter);
        btn_enter.configure(R.drawable.circle_bkg, R.drawable.circle_sel, R.drawable.enter);
        btn_enter.getImageView().setPadding(btnPadding, btnPadding, btnPadding, btnPadding);

        views = new ArrayList<>();

        views.add(inflater.inflate(R.layout.page_splash_one, null));
        views.add(inflater.inflate(R.layout.page_splash_two, null));
        views.add(new PageSplashThree().createView(inflater));

        layout_pager = (RelativeLayout) v.findViewById(R.id.layout_pager);
        layout_login = (LinearLayout) v.findViewById(R.id.layout_login);

        placesBase = (SmartPitRowedLayout) v.findViewById(R.id.base_places);
        placesBase.addView(getMyPlaceButton());
        placesBase.addView(getInputPlaceButton());

        layout_login.setVisibility(View.GONE);

        hsv = (CustomHorizontalScrollView) v.findViewById(R.id.hsv);


        this.setViewsPager(v, R.id.pager, views);


        this.setOnSwipeListener(new OnSwipeListener() {
            @Override
            public void onSwipeRight(double v, int position) {
                Log.d(TAG, "swipe rigt " + position + " " + v);


                int[] result = AppHelper.generateAlphaRainbowTour(colors.get(position), colors.get(position + 1), (float) v);
                background.setBackgroundColor(Color.argb(result[0], result[1], result[2], result[3]));

                int move_width = (int) ((hsv.getMaxScrollAmount() / 2) * position + ((hsv.getMaxScrollAmount() / 2) * v));

                hsv.scrollTo(move_width, 0);

            }

            @Override
            public void onSwipeLeft(double v, int position) {
                Log.d(TAG, "swipe left " + position + " " + v);

                int[] result = AppHelper.generateAlphaRainbowTour(colors.get(position + 1), colors.get(position), (float) (1 - v));
                background.setBackgroundColor(Color.argb(result[0], result[1], result[2], result[3]));
                int move_width = (int) ((hsv.getMaxScrollAmount() / 2) * position + ((hsv.getMaxScrollAmount() / 2) * v));

                hsv.scrollTo(move_width, 0);


            }
        });


        initSurface();


        imm = (InputMethodManager) this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        //  bar = (ProgressBar) v.findViewById(R.id.bar);
        Log.d(TAG, "on create view");
        locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);

        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SmartPitAppHelper.hideViewWithAnimation(layout_pager, 300);
                SmartPitAppHelper.showViewWithAnimation(layout_login, 300);

                loadLastCities();
                // geocodeIp();
                // checkLocation();


            }
        });

        tvCity.setVisibility(View.GONE);
        //btnEnter.setVisibility(View.GONE);
        return v;
    }


    public boolean onBackPressed() {

        if (tvCity.getVisibility() == View.VISIBLE) {
            tvCity.setVisibility(View.GONE);
            placesBase.setVisibility(View.VISIBLE);
            return true;
        }

        if (layout_pager.getVisibility() == View.GONE && bar.getVisibility() == View.GONE) {
            SmartPitAppHelper.hideViewWithAnimation(layout_login, 300);
            SmartPitAppHelper.showViewWithAnimation(layout_pager, 300);
            return true;
        }

        return super.onBackPressed();
    }

    private void initSurface() {


        final MediaPlayer player = new MediaPlayer();

        player.setLooping(true);
        player.setVolume(0, 0);


        surfaceViewFrame.setClickable(false);


        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) surfaceViewFrame.getLayoutParams();

        lp.width = (int) video_width;
        lp.height = (int) video_height;
        surfaceViewFrame.setLayoutParams(lp);


        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                if (Build.VERSION.SDK_INT >= 16)
                    player.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

                if (!player.isPlaying()) {
                    player.start();
                }

            }
        });


        final SurfaceHolder holder = surfaceViewFrame.getHolder();
        //holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                final Surface surface = holder.getSurface();

                if (surface == null) return;

                final boolean invalidSurfaceAccepted = false;
                final boolean invalidSurface = !surface.isValid();

                if (invalidSurface && (!invalidSurfaceAccepted)) return;

                player.setDisplay(holder);

                String path = "android.resource://" + FragmentSplash.this.getActivity().getPackageName() + "/" + R.raw.sample;
                try {
                    player.setDataSource(FragmentSplash.this.getActivity(), Uri.parse(path));
                    //player.setDataSource(path);
                    player.prepare();
                } catch (IOException e) {
                    Log.d(TAG, e.toString());
                    e.printStackTrace();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });
    }


    @Override
    public String getLabel() {
        return null;
    }


    public void onGPSEnabled() {

        checkLocation();

    }

    @Override
    public View createTabIndicator(Context context, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        return v;
    }
}
