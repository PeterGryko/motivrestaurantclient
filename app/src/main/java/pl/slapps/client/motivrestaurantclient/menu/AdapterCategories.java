package pl.slapps.client.motivrestaurantclient.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 26.03.15.
 */
public class AdapterCategories extends ArrayAdapter {
    private Context context;
    private ArrayList<JSONObject> list;
    private int lastPosition = 0;
    private String TAG = AdapterCategories.class.getName();

    public AdapterCategories(Context context, ArrayList<JSONObject> list) {
        super(context, R.layout.row_product, list);

        this.context = context;
        this.list = list;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_category, null);

        final TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);


        JSONObject o = list.get(position);
        String image = null;
        try {

            String name = o.has("label") ? o.getString("label") : "";


            tvName.setText(name.toUpperCase());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        return convertView;
    }
}
