package pl.slapps.client.motivrestaurantclient.user;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 19.05.15.
 */
public class PageUserComments {

    private String TAG = PageUserComments.class.getName();

    private LinearLayout layout_info;
    private ListView lv;
    private SwipeRefreshLayout refresh_layout;

    private void refreashData(final Context context) {
        DAO.getUserComments(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
                try {
                    JSONObject object = new JSONObject(o.toString());
                    object = object.has("api") ? object.getJSONObject("api") : object;
                    JSONArray array = object.has("results") ? object.getJSONArray("results") : new JSONArray();
                    ArrayList<JSONObject> data = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        data.add(array.getJSONObject(i));
                    }
                    if (array.length() > 0) {
                        layout_info.setVisibility(View.GONE);
                        //      refresh_layout.setEnabled(true);
                    }
                    lv.setAdapter(new AdapterUserComments(context, data));
                    refresh_layout.setRefreshing(false);
                    //   SmartPitAppHelper.getInstance(context).setListViewHeightBasedOnChildren(lv);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
                refresh_layout.setRefreshing(false);

            }
        }, MyApplication.getCurrentProfileId());

    }

    public View createView(LayoutInflater inflater, final Context context) {
        View v = inflater.inflate(R.layout.page_user_comments, null);

        layout_info = (LinearLayout) v.findViewById(R.id.info_layout);
        lv = (ListView) v.findViewById(R.id.lv);
        refresh_layout = (SwipeRefreshLayout) v.findViewById(R.id.refreash_layout);
        refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreashData(context);

            }
        });
        //refresh_layout.setEnabled(false);


        refreashData(context);
        return v;
    }
}
