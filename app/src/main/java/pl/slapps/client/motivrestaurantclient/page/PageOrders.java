package pl.slapps.client.motivrestaurantclient.page;

import android.view.LayoutInflater;
import android.view.View;

import pl.slapps.client.motivrestaurantclient.R;

/**
 * Created by piotr on 19.05.15.
 */
public class PageOrders {

    public View createView(LayoutInflater inflater)
    {
        return inflater.inflate(R.layout.page_orders,null);
    }
}
