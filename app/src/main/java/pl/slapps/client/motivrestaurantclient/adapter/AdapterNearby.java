package pl.slapps.client.motivrestaurantclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.widget.RatingWidget;

/**
 * Created by piotr on 28.05.15.
 */
public class AdapterNearby extends ArrayAdapter {
    private ArrayList<Place> list;
    private Context context;
    private int background;
    private int transparent;

    public AdapterNearby(Context context, ArrayList<Place> list) {
        super(context, R.layout.row_nearby, list);
        this.context = context;
        this.list = list;
        background = context.getResources().getColor(R.color.transparent);
        transparent = context.getResources().getColor(android.R.color.transparent);
    }

    private class ViewHolder {
        public SmartImageView logo;
        public TextView label;
        public RatingWidget rate;
        public TextView tv_distance;

        private ImageView iv_facebook;
        private ImageView iv_motiv;


    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (list.get(position).has("divider_label")) {
            View v = LayoutInflater.from(context).inflate(R.layout.row_places_divider, null);
            TextView tv_label = (TextView) v.findViewById(R.id.tv_divider);
            String divider = null;
            try {
                divider = list.get(position).getString("divider_label");

                tv_label.setText(divider);
                v.setTag(null);
                return v;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ViewHolder holder;
        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.row_nearby, null);

            holder.tv_distance = (TextView) convertView.findViewById(R.id.tv_distance);
            holder.label = (TextView) convertView.findViewById(R.id.tv_label);
            holder.rate = (RatingWidget) convertView.findViewById(R.id.rating);
            holder.logo = (SmartImageView) convertView.findViewById(R.id.logo);
            holder.iv_facebook = (ImageView) convertView.findViewById(R.id.iv_facebook);
            holder.iv_motiv = (ImageView) convertView.findViewById(R.id.iv_motiv);
            holder.logo.setMode(SmartImageView.Mode.NORMAL.ordinal());
            holder.logo.setErrorImage(context.getResources().getDrawable(R.drawable.icon_restaurant));
            holder.logo.getProgressBar().setVisibility(View.GONE);

            convertView.setTag(holder);

        } else
            holder = (ViewHolder) convertView.getTag();


        Place model = list.get(position);
        if (model.isRestaurant()==null)
            holder.iv_motiv.setVisibility(View.GONE);
        if (!model.has("facebook_node_id"))
            holder.iv_facebook.setVisibility(View.GONE);

        holder.label.setText(model.getGName());
        holder.tv_distance.setText(SmartPitAppHelper.getDecimalFormat(',', 2).format(model.getDistance()) + "m");
        holder.rate.setScore((int) model.getGRate());

        if (!model.getRLogo().equals("")) {
            holder.logo.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.logo.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.place_holder));
            holder.logo.getErrorView().setVisibility(View.INVISIBLE);
            SmartPitImageLoader.setImageForListView(context, holder.logo, model.getRLogo(), 100, 100, position);
        } else if (!model.getGName().equals("")) {
            holder.logo.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.logo.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.icon_restaurant));
        } else {
            holder.logo.getImageView().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            holder.logo.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.unknown_place));
        }

        if (model.getGName().equals("")) {
            convertView.setBackgroundColor(background);
            holder.rate.setVisibility(View.GONE);
            holder.label.setVisibility(View.GONE);



        } else {
            convertView.setBackgroundColor(transparent);
            holder.rate.setVisibility(View.VISIBLE);
            holder.label.setVisibility(View.VISIBLE);


        }

        return convertView;
    }
}
