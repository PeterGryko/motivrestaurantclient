package pl.slapps.client.motivrestaurantclient.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitBaseFragment;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.model.Place;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.adapter.AdapterNearby;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentPlaceDetails;

/**
 * Created by piotr on 27.05.15.
 */
public class PageAvailable {

    private ListView lv;
    private ArrayList<Place> list;
    //private Context context;
    private MainActivity context;
    //private Context context;

    public View createView(MainActivity context, LayoutInflater inflater) {
        this.context = context;
        View v = inflater.inflate(R.layout.page_nearest, null);

        lv = (ListView) v.findViewById(R.id.lv);


        refreashData(MyApplication.getCurrentPlaces());


        return v;
    }

    public void refreashData(ArrayList<Place> places) {
        this.list = new ArrayList<>();

        for (int i = 0; i < places.size(); i++) {
            if (places.get(i).isRestaurant()!=null)
                list.add(places.get(i));
        }

        lv.setAdapter(new AdapterNearby(context, list));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                String id = list.get(i).getGPlaceId();
                if (((SmartPitBaseFragment) context.getCurrentDrawerFragment()).getCurrentFragment() instanceof FragmentPlaceDetails) {
                    ((FragmentPlaceDetails) ((SmartPitBaseFragment) context.getCurrentDrawerFragment()).getCurrentFragment()).refreashData(id);
                } else {

                    Bundle arg = new Bundle();
                    arg.putString("place_id", id);
                    FragmentPlaceDetails fd = new FragmentPlaceDetails();
                    fd.setArguments(arg);
                    context.switchDrawerFragment(fd);
                }

            }
        });
    }
}
