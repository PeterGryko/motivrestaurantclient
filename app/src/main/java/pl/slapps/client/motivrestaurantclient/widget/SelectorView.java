package pl.slapps.client.motivrestaurantclient.widget;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import pl.gryko.smartpitlib.widget.SmartPitSelectorView;

/**
 * Created by piotr on 05.08.15.
 */
public class SelectorView extends SmartPitSelectorView {

    private String TAG = SelectorView.class.getName();

    public boolean isOpen;
    float current_progress=1;
    private FinishAnimation finishAnimation;

    public void finishAnimation(float output) {
        finishAnimation = new FinishAnimation(output);
        finishAnimation.startAnimation();
    }

    public SelectorView(Context context) {
        super(context);
    }

    public SelectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setFinishAnimationProgress(float progress) {
        current_progress = progress;
        ViewCompat.setScaleX(getBackgroundView(), progress);
        ViewCompat.setScaleY(getBackgroundView(), progress);

    }

    public void setAnimationProgress(float progress) {
        current_progress = progress;
        if (finishAnimation != null && finishAnimation.getAnimation().hasStarted() && !finishAnimation.getAnimation().hasEnded()) {
            finishAnimation.getAnimation().cancel();
            getBackgroundView().clearAnimation();
            finishAnimation = null;
        }
        super.setAnimationProgress(progress);
    }

    public void open() {
        isOpen = true;
        Log.d(TAG, "open");
        super.open();
    }

    public void close() {
        Log.d(TAG, "close");
        isOpen = false;
        super.close();
    }


    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    private class FinishAnimation {
        float input;
        float output;

        private Animation finish_animation;

        public Animation getAnimation() {
            return finish_animation;
        }

        public FinishAnimation(final float output) {
            this.input = current_progress;
            this.output = output;

            finish_animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {

                    float result = input + ((output - input) * interpolatedTime);

                    //  Log.d(TAG,"result" +result);
                    if (result <= 1 && input <= 1 && input >= 0 && output <= 1 && output >= 0) {
                        //    Log.d(TAG,"finish animation "+interpolatedTime +" "+input+" "+output+" "+result);
                        setFinishAnimationProgress(result);


                    }

                }
            };
            finish_animation.setDuration(500);
        }

        public void startAnimation() {
            if (input < output) {


                SelectorView.this.getBackgroundView().startAnimation(finish_animation);
            }
        }
    }


}
