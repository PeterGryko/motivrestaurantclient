package pl.slapps.client.motivrestaurantclient.user;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.client.motivrestaurantclient.DAO;
import pl.slapps.client.motivrestaurantclient.MainActivity;
import pl.slapps.client.motivrestaurantclient.MyApplication;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.menu.FragmentConfirmBuy;
import pl.slapps.client.motivrestaurantclient.widget.AppHelper;

/**
 * Created by piotr on 25.07.15.
 */
public class FragmentLogin extends SmartPitFragment {


    private String TAG = FragmentLogin.class.getName();
    private SmartPitSelectorView btnLogin;
    private SmartPitSelectorView btnRegister;
    private EditText etLogin;
    private EditText etPass;

    private SmartPitSelectorView signInButton;
    private SmartPitSelectorView facebookBtn;
    private ProgressBar bar;
    private View actionbarLayout;


    private boolean isFormValid() {
        boolean flag = true;

        if (etLogin.getText().toString().trim().equals("")) {
            flag = false;
        }
        if (etPass.getText().toString().trim().equals("")) {
            flag = false;
        }

        if (!flag)
            Toast.makeText(this.getActivity(), "musisz podac login i haslo", Toast.LENGTH_LONG).show();

        return flag;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_login, parent, false);
        int ab_color = this.getActivity().getResources().getColor(R.color.profile_indicator_on);

        actionbarLayout = v.findViewById(R.id.action_bar);
        actionbarLayout.setBackgroundColor(Color.argb(100, Color.red(ab_color), Color.green(ab_color), Color.blue(ab_color)));


        bar = (ProgressBar) v.findViewById(R.id.bar);
        bar.setVisibility(View.INVISIBLE);

        btnLogin = (SmartPitSelectorView) v.findViewById(R.id.btn_login);
        btnRegister = (SmartPitSelectorView) v.findViewById(R.id.btn_register);
        etLogin = (EditText) v.findViewById(R.id.et_login);
        etPass = (EditText) v.findViewById(R.id.et_password);

        int pad = (int) this.getActivity().getResources().getDimension(R.dimen.btn_padding_large);
        int padSmall = (int) this.getActivity().getResources().getDimension(R.dimen.splash_btn_padding);

        btnLogin.configure(R.drawable.circle_profile, R.drawable.circle_profile_sel, R.drawable.enter);
        btnLogin.getImageView().setPadding(pad, pad, pad, pad);

        signInButton = (SmartPitSelectorView) v.findViewById(R.id.sign_in_button);
        signInButton.configure(R.drawable.circle_sel, R.drawable.circle_bkg, R.drawable.gplus);
        signInButton.getImageView().setPadding(padSmall, padSmall, padSmall, padSmall);

        facebookBtn = (SmartPitSelectorView) v.findViewById(R.id.sign_in_facebook);
        facebookBtn.configure(R.drawable.circle_profile_sel, R.drawable.circle_profile, R.drawable.facebook);
        facebookBtn.getImageView().setPadding(padSmall, padSmall, padSmall, padSmall);

        btnRegister.configure(R.drawable.circle_profile, R.drawable.circle_profile_sel, R.drawable.enrol);
        btnRegister.getImageView().setPadding(padSmall, padSmall, padSmall, padSmall);

        String last_login = SmartPitAppHelper.getPreferences(this.getActivity()).getString("login", "");
        String last_pass = SmartPitAppHelper.getPreferences(this.getActivity()).getString("pass", "");

        etLogin.setText(last_login);
        etPass.setText(last_pass);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) FragmentLogin.this.getActivity()).mGoogleApiClient.connect();
                //Person currentUser = Plus.PeopleApi.getCurrentPerson(activity.mGoogleApiClient);
                //Log.d(TAG,currentUser.getNickname()+" % "+currentUser.getCurrentLocation() +" % "+currentUser.getUrl());

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isFormValid())
                    return;

                JSONObject data = new JSONObject();
                try {
                    data.put("username", etLogin.getText().toString());

                    data.put("password", etPass.getText().toString());

                    bar.setVisibility(View.VISIBLE);
                    DAO.loginUser(data, new Response.Listener() {
                        @Override
                        public void onResponse(Object o) {
                            Log.d(TAG, o.toString());

                            JSONObject object = null;
                            try {
                                object = new JSONObject(o.toString());

                                String token = object.has("token") ? object.getString("token") : "";
                                MyApplication.token = token;

                                loadProfile();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, AppHelper.getErrorMessage(error));
                            bar.setVisibility(View.INVISIBLE);
                            Toast.makeText(FragmentLogin.this.getActivity(), AppHelper.getErrorMessage(error), Toast.LENGTH_LONG).show();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) FragmentLogin.this.getActivity()).switchMasterDrawerFragment(new FragmentRegister());

            }
        });
        return v;
    }

    private void loadProfile() {
        DAO.getProfile(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());

                try {
                    MyApplication.setCurrentProfile(new JSONObject(o.toString()));


                    SmartPitAppHelper.getPreferences(FragmentLogin.this.getActivity()).edit().putString("login", etLogin.getText().toString()).commit();
                    SmartPitAppHelper.getPreferences(FragmentLogin.this.getActivity()).edit().putString("pass", etPass.getText().toString()).commit();


                    ((MainActivity) FragmentLogin.this.getActivity()).switchMasterDrawerTitleFragment(new FragmentProfileBase());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, AppHelper.getErrorMessage(error));
            }
        });
    }


    public String getLabel() {
        return "Zaloguj się";
    }
}
