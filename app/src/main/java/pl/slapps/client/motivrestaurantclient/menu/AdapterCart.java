package pl.slapps.client.motivrestaurantclient.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.client.motivrestaurantclient.R;
import pl.slapps.client.motivrestaurantclient.fragment.FragmentRestaurantDrawer;

/**
 * Created by piotr on 26.03.15.
 */
public class AdapterCart extends ArrayAdapter {
    private Context context;
    private ArrayList<JSONObject> list;
    private String TAG = AdapterCart.class.getName();
    private FragmentRestaurantDrawer fragment;

    public AdapterCart(Context context, ArrayList<JSONObject> list, FragmentRestaurantDrawer fragment) {
        super(context, R.layout.row_cart, list);

        this.context = context;
        this.list = list;
        this.fragment = fragment;
    }

    public void calculateSum() {
        double sum = 0;

        for (int i = 0; i < list.size(); i++) {
            try {
                int count = list.get(i).has("count") ? list.get(i).getInt("count") : 0;

                JSONObject product = list.get(i).has("product") ? list.get(i).getJSONObject("product") : new JSONObject();
                JSONObject data = product.has("data") ? product.getJSONObject("data") : new JSONObject();
                double price = data.has("price") ? data.getDouble("price") : 0;
                sum = sum+ (price*count);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        fragment.refreashSum(sum);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_cart, null);

        TextView btnMinus = (TextView) convertView.findViewById(R.id.btn_minus);
        TextView btnPlus = (TextView) convertView.findViewById(R.id.btn_plus);
        final TextView tvCount = (TextView) convertView.findViewById(R.id.tv_count);
        final TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        final TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);


        int count = 0;
        String name = "";
        double price = 0;
        try {
            count = list.get(position).has("count") ? list.get(position).getInt("count") : 0;
            JSONObject product = list.get(position).has("product") ? list.get(position).getJSONObject("product") : new JSONObject();
            name = product.has("name") ? product.getString("name") : "";
            JSONObject data = product.has("data") ? product.getJSONObject("data") : new JSONObject();
            price = data.has("price") ? data.getDouble("price") : 0;
            tvPrice.setText(Double.toString(price * count) + "zł");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        tvCount.setText(Integer.toString(count) + "x");
        tvName.setText(name);
        final double f_price = price;
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                try {
                    count = (list.get(position).has("count") ? list.get(position).getInt("count") : 0) + 1;
                    list.get(position).put("count", count);
                    tvCount.setText(Integer.toString(count) + "x");
                    tvPrice.setText(Double.toString(f_price * count) + "zł");
                    calculateSum();
                    Log.d(TAG, Integer.toString(count));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                try {
                    count = (list.get(position).has("count") ? list.get(position).getInt("count") : 0);
                    if (count > 0) {
                        list.get(position).put("count", count - 1);

                        tvCount.setText(Integer.toString(count - 1) + "x");
                        tvPrice.setText(Double.toString(f_price * count) + "zł");
                        calculateSum();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        return convertView;
    }
}
